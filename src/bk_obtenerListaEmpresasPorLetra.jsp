<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.support.MiddlewareValue"%>


<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bk_obtenerListaEmpresasPorLetra.jsp");
	//INPUT
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	String vLetraInicial = additionalParams.get("letraInicial");
	String vListaEmpresas = additionalParams.get("listaEmpresas");
	String vListaCodNegocio = additionalParams.get("listaCodNegocio");
	
	
	//OUTPUT
	String vListaAudiosEmpresasString="";
	String vListaCodNegocioString="";
	String vNumEmpresas="";
	
	JSONObject result = new JSONObject();
		
	List<String> listaCodNegocioIn = MiddlewareValue.getUrlsFromJsonString(vListaCodNegocio);
	List<String> listaEmpresas= MiddlewareValue.getUrlsFromJsonString(vListaEmpresas);
	
	List<Character> listaLetras=new ArrayList();
	List<String> listaCodNegocioOut=new ArrayList();
	List<String> listaAudiosEmpresas=new ArrayList();
	
	setLog(strCallUUID + " IBCC-CSF :::: =============== BACKEND OBTENER LISTA EMPRESAS POR LETRA ================================");
	
	switch(Integer.parseInt(vLetraInicial)){
	
		case 2: listaLetras.add('A');
				listaLetras.add('B');
				listaLetras.add('C');
				break;
		case 3: listaLetras.add('D');
				listaLetras.add('E');
				listaLetras.add('F');
				break;
		case 4: listaLetras.add('G');
				listaLetras.add('H');
				listaLetras.add('I');
				break;
		case 5: listaLetras.add('J');
				listaLetras.add('K');
				listaLetras.add('L');
				break;
		case 6: listaLetras.add('M');
				listaLetras.add('N');
				listaLetras.add('O');
				break;
		
		case 7: listaLetras.add('P');
				listaLetras.add('Q');
				listaLetras.add('R');
				listaLetras.add('S');
				break;
		case 8: listaLetras.add('T');
				listaLetras.add('U');
				listaLetras.add('V');
				break;
		case 9: listaLetras.add('W');
				listaLetras.add('X');
				listaLetras.add('Y');
				listaLetras.add('Z');
				break;

	}
	
	for(char letra: listaLetras){
		
		int i=0;
		for(String empresa: listaEmpresas){
			
			if(empresa.toUpperCase().charAt(0)==letra){
				String codNegocio=listaCodNegocioIn.get(i);
				listaAudiosEmpresas.add(url_audio+"/"+empresa+extension);
				listaCodNegocioOut.add(codNegocio);
				
			}
			i++;
	    
		}
	}
	
	
    vNumEmpresas=Integer.toString(listaAudiosEmpresas.size());
    vListaAudiosEmpresasString=MiddlewareValue.getListaString(listaAudiosEmpresas);
    vListaCodNegocioString=MiddlewareValue.getListaString(listaCodNegocioOut);
    
	
    setLog(strCallUUID + " IBCC-CSF :::: Num Empresas = "+vNumEmpresas);
    setLog(strCallUUID + " IBCC-CSF :::: Lista Audios Empresas = "+vListaAudiosEmpresasString);
    setLog(strCallUUID + " IBCC-CSF :::: Lista Cod Negocio = "+vListaCodNegocioString);
	    
    
	
	result.put("numEmpresas", vNumEmpresas);
	result.put("listaAudiosEmpresas", vListaAudiosEmpresasString);
	result.put("listaCodNegocio", vListaCodNegocioString);
	return result;	
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>