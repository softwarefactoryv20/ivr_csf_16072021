<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.classes.ServicioAfiliado"%>
<%@page import="middleware_support.tramas.Bean_IB76"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	
	//Input
	//String listaServiciosString =  vListaTarjetasString;//additionalParams.get("listaTarjetas");
	//String vNumIngreso = ultimos4Digitos;//additionalParams.get("Ult4Digitos");
	
	String listaServiciosString =  additionalParams.get("listaServicios");
	String vNumIngreso = additionalParams.get("inp_Bill");
	
	//Output
	String numServicios3UltDigitos = "";
	String listaServicios3UltDigitosString = "";
	
    JSONObject result = new JSONObject();
    Logger log = Logger.getLogger("Bck_ObtenerListaServiciosUltimos3Digitos.jsp");
    
    setLog(strCallUUID + " IBCC-CSF :::: ================ BACKEND OBTENER LISTA SERVICIOS ULTIMOS 3 DIGITOS ================================");
    setLog(strCallUUID + " IBCC-CSF :::: === Lista: "+listaServiciosString);
    
    List<ServicioAfiliado> listaServicios=Bean_IB76.getListaServicios(listaServiciosString);
    List<ServicioAfiliado> listaServicios3UltDigitos=new ArrayList();
	
    setLog(strCallUUID + " IBCC-CSF :::: ----Lista Recuperada----");	
	
	if(vNumIngreso.length() == 3) {
		//--- INICIO - Recorriendo el Arreglo (Ultimos 3 digitos de la tarjeta)
		int j=0;
		setLog(strCallUUID + " IBCC-CSF :::: Servicios cuyos 3 ultimos digitos son : "+ vNumIngreso);	
		
		for(ServicioAfiliado refServicioAfiliado: listaServicios){	
			String cadena = refServicioAfiliado.getNumServicio().trim();
			int longitud = cadena.length();
			if(cadena.substring(longitud-3,longitud).equals(vNumIngreso)){
				setLog(strCallUUID + " IBCC-CSF :::: Cod. Producto: " +j+" : " + cadena);
				listaServicios3UltDigitos.add(refServicioAfiliado);
				j++;
			}
		}
		
		numServicios3UltDigitos = Integer.toString(j) ;
		listaServicios3UltDigitosString=Bean_IB76.getListaServiciosString(listaServicios3UltDigitos);
		
		setLog(strCallUUID + " IBCC-CSF :::: Cantidad de servicios que coinciden = " + numServicios3UltDigitos);
		setLog(strCallUUID + " IBCC-CSF :::: Lista servicios 3 ultimos digitos " + listaServicios3UltDigitosString);
		
		//--- FIN - Recorriendo el Arreglo (Ultimos 3 digitos de la tarjeta)
	}
	
	
	result.put("numServicios3UltDigitos",numServicios3UltDigitos);
	result.put("listaServicios3UltDigitos",listaServicios3UltDigitosString);
	
    return result;
	  
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>