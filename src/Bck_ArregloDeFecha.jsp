<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>



<%@page import="middleware_support.support.MiddlewareDate"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ArregloDeFecha.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vFecha = additionalParams.get("");
	
	//Output
	String listaAudiosString = "";
	String numAudios = "";
	
	JSONObject result = new JSONObject();
	
	setLog(strCallUUID + " IBCC-CSF :::: =============== BACKEND OBTENER AUDIOS FECHA EN LISTA ================================");
	
	
	MiddlewareDate refMiddlewareDate=new MiddlewareDate(url_audio,vFecha,extension);
	listaAudiosString=refMiddlewareDate.getListaAudiosString();
	numAudios=Integer.toString(refMiddlewareDate.getListaAudios().size());
	
	setLog(strCallUUID + " IBCC-CSF :::: Lista de audios String : "+listaAudiosString);
	setLog(strCallUUID + " IBCC-CSF :::: Tamaño lista de audios  : "+numAudios);
	
	result.put("listaAudiosString", listaAudiosString);
	result.put("numAudios", numAudios);
	return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>