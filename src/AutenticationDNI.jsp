<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%!
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("AutenticationDNI.jsp");
	String vDoc = "1";
	String num_doc = additionalParams.get("vNumDoc");   
	setLog(strCallUUID + " IBCC-CSF :::: OPCION 2 - Page 86 - Autentication DNI");
	setLog(strCallUUID + " IBCC-CSF :::: Numero del Documento: " + num_doc);
	
    JSONObject result = new JSONObject();
    
    setLog(strCallUUID + " IBCC-CSF :::: Longitud : " + num_doc.length());
    if(num_doc.length() == 1)    
    	vDoc = "0";    
    else
    {
    	if(num_doc.length() == 8)
    	{	vDoc = "8";	}
    }
    setLog(strCallUUID + " IBCC-CSF :::: variable vDoc : " + vDoc);
    
    result.put("vDoc", vDoc);
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>