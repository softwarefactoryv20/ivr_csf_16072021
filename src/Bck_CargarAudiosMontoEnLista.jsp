<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="middleware_support.support.MiddlewareNumber"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_CargarAudiosMontoEnLista.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vTipoMoneda=additionalParams.get("tipoMonedaPagar");
	String vMonto=additionalParams.get("importe");
	
	//Output
	String listaAudiosString="";
	String numAudios;
	
	JSONObject result = new JSONObject();
    
	setLog(strCallUUID + " IBCC-CSF :::: ================ BACKEND CARGAR AUDIOS SEGUN MONEDA EN LISTA ================================");
	  
	
	if(vTipoMoneda.equals("PEN")){
		vTipoMoneda="0000";
	}else if (vTipoMoneda.equals("USD")){
		vTipoMoneda="0001";
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: tipo moneda: "+vTipoMoneda);
	setLog(strCallUUID + " IBCC-CSF :::: monto: "+vMonto);
	
	MiddlewareNumber refMiddlewareNumber=new MiddlewareNumber(url_audio,vTipoMoneda,vMonto,extension);
	numAudios=Integer.toString(refMiddlewareNumber.getListaAudios().size());
	listaAudiosString=refMiddlewareNumber.getListaAudiosString();
	
	setLog(strCallUUID + " IBCC-CSF :::: Lista de audios String : "+listaAudiosString);
	setLog(strCallUUID + " IBCC-CSF :::: Tamaño lista de audios  : "+numAudios);
	
	result.put("numAudios", numAudios);
	result.put("listaAudiosString", listaAudiosString);
	return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>