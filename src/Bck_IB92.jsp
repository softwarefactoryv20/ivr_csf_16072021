<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.tramas.Bean_IB92"%>

<%@page import= "IvrTransaction.Controller.GeneralController"%>
<%@page import= "IvrTransaction.modelo.SaldosTC"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
    Logger log = Logger.getLogger("Bck_IB92.jsp");
	// TODO Auto-generated method stub
			String extension=".wav";
			String servidorwas = additionalParams.get("servidorwas");
			String url = "http://" + servidorwas + ":8080";
			String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
			
			//Input
			String vInstitucion=additionalParams.get("codigoInst");
			String vTarjeta= additionalParams.get("vNumDoc");
			
			//Output
			int vCodErrorRpta = 0;
			String vCodRpta="0000"; 
			String client_desc="";
			String indMultimoneda="";
			String moneda="";
			String deudaTotalFacturadaSoles="";
			String pagoMesSoles="";
			String pagoMinimoSoles="";
			String deudaTotalFacturadaDolares="";
			String pagoMesDolares="";
			String pagoMinimoDolares="";
			
			Bean_IB92 refBean_IB92;
			
			
			JSONObject result = new JSONObject();		    
			
			IvrTransaction.Controller.GeneralController General= new IvrTransaction.Controller.GeneralController();
			IvrTransaction.modelo.SaldosTC generalModel = General.QIvrSaldosTC(vInstitucion, vTarjeta);
			vCodErrorRpta = General.getCodRetorno();
			
		    if (vCodErrorRpta == 0) {  
			
				vCodRpta=generalModel.getERROR();
				if (vCodRpta.equals("0000")) {
					
					client_desc=generalModel.getNombreCliente();
					refBean_IB92=new Bean_IB92(url_audio,generalModel,extension);
					indMultimoneda=refBean_IB92.getIndMultiMoneda();
					moneda=refBean_IB92.getMonedaTarjeta();
					deudaTotalFacturadaSoles=refBean_IB92.getPagoTotalSoles().getNumero();
					pagoMesSoles=refBean_IB92.getPagoMinimitoSoles().getNumero();
					pagoMinimoSoles=refBean_IB92.getPagoMinimoSoles().getNumero();
					deudaTotalFacturadaDolares=refBean_IB92.getPagoTotalDolares().getNumero();
					pagoMesDolares=refBean_IB92.getPagoMinimitoDolares().getNumero();
					pagoMinimoDolares=refBean_IB92.getPagoMinimoDolares().getNumero();
			
					
					
					setLog(strCallUUID + " IBCC-CSF :::: ========= INVOCACION - IB92 : SALDOS TARJETA DE CREDITO ========= ");
					
					setLog(strCallUUID + " IBCC-CSF :::: Cod. Producto: " + refBean_IB92.getNroTarjetaCredito());
					setLog(strCallUUID + " IBCC-CSF :::: Cliente:  " + client_desc);
				
					setLog(strCallUUID + " IBCC-CSF :::: Moneda: " + refBean_IB92.getMonedaTarjeta());
					setLog(strCallUUID + " IBCC-CSF :::: Ind. Multimoneda: " + refBean_IB92.getIndMultiMoneda());
					setLog(strCallUUID + " IBCC-CSF :::: Minimo Soles: " + refBean_IB92.getPagoMinimoSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Total Soles: " + refBean_IB92.getPagoTotalSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Fact. Soles: " + refBean_IB92.getPagoFactSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Minimito Soles: " + refBean_IB92.getPagoMinimitoSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Totalito Soles: " + refBean_IB92.getPagoTotalitoSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Minimo Dolares: " + refBean_IB92.getPagoMinimoDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Total Dolares: " + refBean_IB92.getPagoTotalDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Fact. Dolares: " + refBean_IB92.getPagoFactDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Minimito Dolares: " + refBean_IB92.getPagoMinimitoDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Totalito Dolares: " + refBean_IB92.getPagoTotalitoDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Linea Credito: " + refBean_IB92.getLineaCredito().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Linea Disp. Efectivo: " + refBean_IB92.getLineaDispEfectivo().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Saldo Credito: " + refBean_IB92.getSaldoCredito().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Saldo Linea Disp. Efectivo: " + refBean_IB92.getSaldoLineaDispEfectivo().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Ultimo Pago Mon. Local: " + refBean_IB92.getUltPagoMonLocal().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Ultimo Pago Mon. Ext. : " + refBean_IB92.getUltPagoMonExt().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Fecha Ultimo Pago: " + refBean_IB92.getFecUltPago().getFecha());
					setLog(strCallUUID + " IBCC-CSF :::: Scotia Ptos. Saldo Tarjeta: " + refBean_IB92.getScotiaPtosSalxTarjeta().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Sctoia Ptos. Saldo Total:  " + refBean_IB92.getScotiaPtosSalTotal().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Procesadora: " + refBean_IB92.getProcesadora());
					
					setLog(strCallUUID + " IBCC-CSF :::: ------------Output--------------");
						
					setLog(strCallUUID + " IBCC-CSF :::: Indicador Multimoneda :"+indMultimoneda);
					setLog(strCallUUID + " IBCC-CSF :::: Moneda :"+moneda);
					setLog(strCallUUID + " IBCC-CSF :::: Deuda Total Facturada Soles : "+deudaTotalFacturadaSoles);
					setLog(strCallUUID + " IBCC-CSF :::: Pago Mes Soles: "+pagoMesSoles);
					setLog(strCallUUID + " IBCC-CSF :::: Pago Minimo Soles: "+pagoMinimoSoles);
					setLog(strCallUUID + " IBCC-CSF :::: Deuda Total Facturada Dolares : "+deudaTotalFacturadaDolares);
					setLog(strCallUUID + " IBCC-CSF :::: Pago Mes Dolares: "+pagoMesDolares);
					setLog(strCallUUID + " IBCC-CSF :::: Pago Minimo Dolares: "+pagoMinimoDolares);
					
				}		
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  			
			}
		    
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
			
			
			result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
			result.put("vCodRpta", vCodRpta);
			result.put("multimoneda", indMultimoneda);
			result.put("monedaServicio", moneda);
			result.put("pagoDeudaTotalSoles", deudaTotalFacturadaSoles);
			result.put("pagoDeudaMesSoles", pagoMesSoles);
			result.put("pagoDeudaMinimoSoles", pagoMinimoSoles);
			result.put("pagoDeudaTotalDolares", deudaTotalFacturadaDolares);
			result.put("pagoDeudaMesDolares", pagoMesDolares);
			result.put("pagoDeudaMinimoDolares", pagoMinimoDolares);
			
		    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>