<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>



<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.support.MiddlewareSerie"%>

<%!

// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {	
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ObtenerAudiosSerieEnVariables.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String flagUltimos3Audios = additionalParams.get("flag3UltDig");
	String vSerie= additionalParams.get("numero");
	
	//Output
	String audioLoc_01= url_audio+"/silencio"+extension;
	String audioLoc_02= url_audio+"/silencio"+extension;
	String audioLoc_03= url_audio+"/silencio"+extension;
	String audioLoc_04= url_audio+"/silencio"+extension;
	String audioLoc_05= url_audio+"/silencio"+extension;
	String audioLoc_06= url_audio+"/silencio"+extension;
	String audioLoc_07= url_audio+"/silencio"+extension;
	String audioLoc_08= url_audio+"/silencio"+extension;
	String audioLoc_09= url_audio+"/silencio"+extension;
	String audioLoc_10= url_audio+"/silencio"+extension;
	String audioLoc_11= url_audio+"/silencio"+extension;
	String audioLoc_12= url_audio+"/silencio"+extension;
	String audioLoc_13= url_audio+"/silencio"+extension;
	String audioLoc_14= url_audio+"/silencio"+extension;
	String audioLoc_15= url_audio+"/silencio"+extension;
	String audioLoc_16= url_audio+"/silencio"+extension;
	
	JSONObject result = new JSONObject();
	
	if(flagUltimos3Audios.equals("1")){
		vSerie=vSerie.substring(vSerie.length()-3,vSerie.length());
	}
	
	MiddlewareSerie refMiddlewareSerie=new MiddlewareSerie(url_audio,vSerie,extension);
	List<String> listaAudios;
	listaAudios=refMiddlewareSerie.getListaAudios();
	
	switch(listaAudios.size()){
	
		case 1:	audioLoc_01= listaAudios.get(0);
				break;
		case 2:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				break;
		case 3:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				break;
		case 4:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				break;
		case 5:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				break;
		case 6:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				break;
		case 7:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				break;
		case 8:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				break;
		case 9:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				break;
		case 10:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				break;
		case 11:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				break;
		case 12:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				break;
		case 13:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				audioLoc_13= listaAudios.get(12);
				break;
		case 14:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				audioLoc_13= listaAudios.get(12);
				audioLoc_14= listaAudios.get(13);
				break;
		case 15:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				audioLoc_13= listaAudios.get(12);
				audioLoc_14= listaAudios.get(13);
				audioLoc_15= listaAudios.get(14);
				break;
		case 16:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				audioLoc_13= listaAudios.get(12);
				audioLoc_14= listaAudios.get(13);
				audioLoc_15= listaAudios.get(14);
				audioLoc_16= listaAudios.get(15);
				break;
	}
	setLog(strCallUUID + " IBCC-CSF :::: Lista audios Servicio : "+ refMiddlewareSerie.getSerie());
	
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_01);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_02);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_03);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_04);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_05);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_06);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_07);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_08);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_09);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_10);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_11);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_12);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_13);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_14);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_15);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_16);
	
	
	
	result.put("audioLoc_01",audioLoc_01);
	result.put("audioLoc_02",audioLoc_02);
	result.put("audioLoc_03",audioLoc_03);
	result.put("audioLoc_04",audioLoc_04);
	result.put("audioLoc_05",audioLoc_05);
	result.put("audioLoc_06",audioLoc_06);
	result.put("audioLoc_07",audioLoc_07);
	result.put("audioLoc_08",audioLoc_08);
	result.put("audioLoc_09",audioLoc_09);
	result.put("audioLoc_10",audioLoc_10);
	result.put("audioLoc_11",audioLoc_11);
	result.put("audioLoc_12",audioLoc_12);
	result.put("audioLoc_13",audioLoc_13);
	result.put("audioLoc_14",audioLoc_14);
	result.put("audioLoc_15",audioLoc_15);
	result.put("audioLoc_16",audioLoc_16);
	
	return result;

};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>