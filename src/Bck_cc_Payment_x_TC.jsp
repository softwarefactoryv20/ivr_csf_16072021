<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_cc_Payment_x_TC.jsp");
	String cc_Payment_x = additionalParams.get("cc_Payment_x"); 
	String servidorwas = additionalParams.get("servidorwas");
	String cc_Payment = "";

	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF/";
	
	setLog(strCallUUID + " IBCC-CSF :::: Eligio el tipo de pago a realizar: " + cc_Payment_x);
	
    JSONObject result = new JSONObject();
    
    if(cc_Payment_x.equals("1")){
    	cc_Payment = url_audio + "cc_payment_1.wav"; 
    }
    else if(cc_Payment_x.equals("2")){
    	cc_Payment = url_audio + "cc_payment_2.wav"; 
    } 
    else if(cc_Payment_x.equals("3")){
    	cc_Payment = url_audio + "cc_payment_3.wav"; 
    }else{
     	cc_Payment = url_audio + "cc_payment_7.wav"; 
        
    }
    
    String exchangeRate1 = url_audio + "exchange_rate_1.wav";
    String exchangeRate2 = url_audio + "exchange_rate_2.wav";
    
    setLog(strCallUUID + " IBCC-CSF :::: Locucion  Reproducir: " + cc_Payment);
    setLog(strCallUUID + " IBCC-CSF :::: Locucion  Exchange Rate 1: " + exchangeRate1);
    setLog(strCallUUID + " IBCC-CSF :::: Locucion  Exchange Rate 2: " + exchangeRate2);
    
    result.put("cc_Payment", cc_Payment);
    result.put("exchangeRate1", exchangeRate1);
    result.put("exchangeRate2", exchangeRate2);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>