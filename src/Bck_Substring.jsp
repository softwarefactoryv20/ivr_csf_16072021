<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_Substring.jsp");
	String cadena = additionalParams.get("cadena");	
    String cadenaCorta = "";
    
    JSONObject result = new JSONObject();
    
    setLog(strCallUUID + " IBCC-CSF :::: Numero de Cuenta de Tercero: " + cadena);
    
    cadenaCorta = cadena.substring(cadena.length() - 3, cadena.length());
    //String nuevoNumAbono = padLeftZeros(cadena);
    String nuevoNumAbono = cadena;
    
    setLog(strCallUUID + " IBCC-CSF :::: Los Ultimos 3 digitos de la cuenta de Terceros: " + cadenaCorta);
    setLog(strCallUUID + " IBCC-CSF :::: Autocompletar con ceros Numero de Cuenta Cargo: " + nuevoNumAbono);
    
    result.put("cadenaCorta", cadenaCorta);
    result.put("nuevoNumAbono", nuevoNumAbono);
    return result;
    
};

/*public String padLeftZeros(String cadena){
	
	if(cadena.length()<14){
		cadena=String.format("%0"+(14-cadena.length())+"d%s",0,cadena);
	}
	return cadena;
}*/

%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>