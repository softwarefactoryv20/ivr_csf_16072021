<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="middleware_support.classes.Saldo"%>
<%@page import="middleware_support.tramas.Bean_IB73"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ObtenerListaUltimos3Digitos.jsp");
		//Input
		String listaServiciosString =  additionalParams.get("listaServicios");
		String listaCuentasString =  additionalParams.get("listaCuentas");
		String vNumIngreso = additionalParams.get("inp_Bill");
			
		//Output
		String numCuentas3UltDigitos = "";
		String listaCuentas3UltDigitosString = "";
		
	    JSONObject result = new JSONObject();
	    
	    setLog(strCallUUID + " IBCC-CSF :::: ================ BACKEND OBTENER LISTA CUENTAS ULTIMOS 3 DIGITOS ================================");
	    setLog(strCallUUID + " IBCC-CSF :::: === Lista: "+listaCuentasString);
	    
	    List<Saldo> listaCuentas=Bean_IB73.getListaCuentas(listaCuentasString);
	    List<Saldo> listaCuentas3UltDigitos=new ArrayList();
		
	    setLog(strCallUUID + " IBCC-CSF :::: ----Lista Recuperada----");	
		
		if(vNumIngreso.length() == 3) {
			//--- INICIO - Recorriendo el Arreglo (Ultimos 3 digitos de la tarjeta)
			int j=0;
			setLog(strCallUUID + " IBCC-CSF :::: Cuentas cuyos 3 ultimos digitos son : "+ vNumIngreso);	
			
			for(Saldo refSaldo: listaCuentas){	
				String cadena = refSaldo.getCodigoProducto().trim();
				int longitud = cadena.length();
				if(cadena.substring(longitud-3,longitud).equals(vNumIngreso)){
					setLog(strCallUUID + " IBCC-CSF :::: " + j+" : " + cadena);
					listaCuentas3UltDigitos.add(refSaldo);
					j++;
				}
			}
			
			numCuentas3UltDigitos = Integer.toString(j) ;
			listaCuentas3UltDigitosString=Bean_IB73.getListaCuentasString(listaCuentas3UltDigitos);
			
			setLog(strCallUUID + " IBCC-CSF :::: Cantidad de cuentas que coinciden = " + numCuentas3UltDigitos);
			setLog(strCallUUID + " IBCC-CSF :::: Lista cuentas 3 ultimos digitos " + listaCuentas3UltDigitosString);
			
			//--- FIN - Recorriendo el Arreglo (Ultimos 3 digitos de la tarjeta)
		}
		
		
		result.put("numCuentas3UltDigitos",numCuentas3UltDigitos);
		result.put("listaCuentas3UltDigitosString",listaCuentas3UltDigitosString);
		
	    return result;
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>