<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%!
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("InputSaldosTarjCredito.jsp");
	String vNumInput = additionalParams.get("vNumInput");   
	setLog(strCallUUID + " IBCC-CSF :::: ELEGIR OPCION");
	setLog(strCallUUID + " IBCC-CSF :::: Numero marcado: " + vNumInput);
	
    JSONObject result = new JSONObject();
    result.put("vNumInput", vNumInput);
    return result;    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>