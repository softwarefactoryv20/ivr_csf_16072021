<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>


<%@page import="middleware_support.classes.TelefonoAfiliado"%>

<%@page import="middleware_support.tramas.Bean_IB73_IB76_IB77_IB94"%>
<%@page import="middleware_support.tramas.Bean_IB77"%>

<%@page import="IvrTransaction.Security.SecurityPassword"%>
<%@page import="IvrTransaction.RequestM3.ProfileClient"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bck_Invocacion_IB77_Telefonos.jsp");
	// TODO Auto-generated method stub
	String extension = ".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion = additionalParams.get("codigoInst");
	String vTarjeta = additionalParams.get("vNumDoc");
	String vPin = additionalParams.get("varOpcIvr");
	
	String vPinEncriptado = "";
	String vLetras= "";
	String vNumeros= "";
	
	//Output
	int vCodErrorRptaInteger = 0;
	String listaTelefonosString="";
	String numTelefonos="";
	
	String vCodErrorRpta = "";
	String vCodRpta = "0000"; //ERROR IB73
	String vCodRptaIB76 = "0000";	//ERROR IB76
	String vCodRptaIB77 = "0000";	//ERROR IB77
	String vCodRptaIB94 = "";	//ERROR IB94
	String vErrorPin = "";
	
	
	Bean_IB73_IB76_IB77_IB94 refBean_IB73_IB76_IB77_IB94;
	JSONObject result = new JSONObject();	
	
	IvrTransaction.Security.SecurityPassword SPOId=new SecurityPassword().EncriptaClave(vPin);
	
	vPinEncriptado = SPOId.getsPasswordTwo();
	vLetras=SPOId.getsLetras();
	vNumeros=SPOId.getsNumeros();
	
	IvrTransaction.RequestM3.ProfileClient ProfileClient=new IvrTransaction.RequestM3.ProfileClient();	
	
	vCodErrorRptaInteger=ProfileClient.LoadProfileClient(vInstitucion, vTarjeta ,vPinEncriptado, vLetras, vNumeros);
	vCodErrorRpta = Integer.toString(vCodErrorRptaInteger);
	vCodRpta = ProfileClient.getERROR(); 		//ERROR IB73
	vCodRptaIB76 = ProfileClient.getERROR();	//ERROR IB76
	vCodRptaIB77 = ProfileClient.getERROR();	//ERROR IB77
	vCodRptaIB94 = ProfileClient.getERROR();	//ERROR IB94
	vErrorPin = ProfileClient.getIVRErrorCode();
	

	setLog(strCallUUID + " IBCC-CSF :::: ============== BACKEND INVOCACION - IB73/IB76/IB77/IB94 : PERFIL DEL CLIENTE (TELEFONOS) =======================");
	
	
	if (vCodErrorRptaInteger==0) {					
		
		refBean_IB73_IB76_IB77_IB94=new Bean_IB73_IB76_IB77_IB94(url_audio, ProfileClient ,extension);
		
		listaTelefonosString=refBean_IB73_IB76_IB77_IB94.getRefBean_IB77().getListaTelefonosString();
		numTelefonos=refBean_IB73_IB76_IB77_IB94.getRefBean_IB77().getNroRegistros();
		
		if(vCodRptaIB77.equals("0000")){
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB77------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Nro Registros " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB77().getNroRegistros());

			setLog(strCallUUID + " IBCC-CSF :::: Telefonos");
			
			
			setLog(strCallUUID + " IBCC-CSF :::: N   Nro Telefono   Descripcion");
			int i=1;
			for(TelefonoAfiliado refTelefonoAfiliado :refBean_IB73_IB76_IB77_IB94.getRefBean_IB77().getListaTelefonosAfiliados()){
				
				setLog(strCallUUID + " IBCC-CSF :::: " + i+"   "+
									String.format("%1$-12s",refTelefonoAfiliado.getNroTelefono())+"  "+
									String.format("%1$-20s",refTelefonoAfiliado.getDscrTelefono()));
				i++;
			}
		}
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRptaIB77);
	
	result.put("vCodRpta", vCodRpta);
    result.put("vCodErrorRpta", vCodErrorRpta);
    result.put("vCodRptaIB76", vCodRptaIB76);
    result.put("vCodRptaIB77", vCodRptaIB77);
    result.put("vCodRptaIB94", vCodRptaIB94);
    result.put("vErrorPin", vErrorPin);
    
    result.put("listaTelefonos",listaTelefonosString);
    result.put("numTelefonos",numTelefonos);
    
  	return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>