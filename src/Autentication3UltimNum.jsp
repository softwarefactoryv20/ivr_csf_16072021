<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%!
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Autentication3UltimNum.jsp");
	String vNumIngreso = "1";
	String opc3UltNumero = additionalParams.get("vOpc3UltNumero");   
	setLog(strCallUUID + " IBCC-CSF :::: OPCION 2 - Select Multiple Deposit Account");
	setLog(strCallUUID + " IBCC-CSF :::: Numero del Documento: " + opc3UltNumero);
	
    JSONObject result = new JSONObject();
    
    setLog(strCallUUID + " IBCC-CSF :::: Longitud : " + opc3UltNumero.length());
    if(opc3UltNumero.length() == 1){
    	vNumIngreso = "0";    
    }
    else if(opc3UltNumero.length() == 3){	
    	vNumIngreso = "3";	
    }
    setLog(strCallUUID + " IBCC-CSF :::: variable vNumIngreso : " + vNumIngreso);
    
    result.put("vNumIngreso", vNumIngreso);
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>