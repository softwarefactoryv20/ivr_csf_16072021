<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.classes.Movimiento"%>
<%@page import="middleware_support.tramas.Bean_IB72"%>
<%!

// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {	
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_RecorrerArregloMovimientosCredito.jsp");
			//Input
			String vIndice = additionalParams.get("cont");
			String vListaMovimientosString = additionalParams.get("lista");
			
			//Output
			String vFecOperacion="";
			String vImporte="";
			String vSignoImporte="";
			String vFecProceso="";
			String vNroCuotas="";
			String vTransType="";
			String vMonedaMovimiento="";
			
			
			JSONObject result = new JSONObject();
			
			int i=Integer.parseInt(vIndice);
			List<Movimiento> listaMovimientos;
			setLog(strCallUUID + " IBCC-CSF :::: =============== BACKEND RECORRER ARREGLO MOVIMIENTOS CREDITO ================================");
			listaMovimientos=Bean_IB72.getListaMovimientos(vListaMovimientosString);
			
			Movimiento refMovimiento=listaMovimientos.get(i);
			
			vFecOperacion=refMovimiento.getFecOperacion().getFecha();
			vImporte=refMovimiento.getImporte().getNumero();
			vSignoImporte=refMovimiento.getSignoImporte();
			vFecProceso=refMovimiento.getFecOperacion().getFecha();
			vNroCuotas=refMovimiento.getNroCuotas();
			vTransType=refMovimiento.getTransType();
			vMonedaMovimiento=refMovimiento.getMonedaMovimiento();
			
			if(vTransType.equals("1")){
				
				vTransType="-1";
			}
			
			setLog(strCallUUID + " IBCC-CSF ::::  Fecha operacion : "+ vFecOperacion);
			setLog(strCallUUID + " IBCC-CSF ::::  Importe : "+ vImporte);
			setLog(strCallUUID + " IBCC-CSF ::::  Signo importe : "+ vSignoImporte);
			setLog(strCallUUID + " IBCC-CSF ::::  Fecha proceso : "+ vFecProceso);
			setLog(strCallUUID + " IBCC-CSF ::::  Nro Cuotas : "+ vNroCuotas);
			setLog(strCallUUID + " IBCC-CSF ::::  Tipo transaccion : "+ vTransType);
			setLog(strCallUUID + " IBCC-CSF ::::  Moneda movimiento : "+ vMonedaMovimiento);
			
			result.put("fechaMovimiento", vFecOperacion);
			result.put("importe",vImporte );
			result.put("tipoMovimiento", vTransType);
			result.put("monedaMovimiento", vMonedaMovimiento);
			
			return result;	
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>