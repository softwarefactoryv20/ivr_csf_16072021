<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.tramas.Bean_IB71"%>

<%@page import="middleware_support.support.MiddlewareValue"%>

<%@page import="middleware_support.support.MiddlewareDate"%>

<%@page import="middleware_support.support.MiddlewareNumber"%>
<%@page import="middleware_support.classes.Movimiento"%>

<%@page import="IvrTransaction.Security.SecurityPassword"%>
<%@page import="IvrTransaction.Controller.CredBalanceController"%>
<%@page import="IvrTransaction.modelo.CredBalanceInformation"%>

<%!

// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {	
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_Invocacion_IB71.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vTipoValidacion=additionalParams.get("tipoVal");
	String vPin=additionalParams.get("varOpcIvr");
	String vDni="";
	
	String vPinEncriptado = "";
	String vLetras= "";
	String vNumeros= "";
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String client_desc="";
	String city_desc="";
	String ctry_desc="";
	String vListaAudiosSaldoCredito="";
	String vListaAudiosSaldoLineaDispEfectivo="";
	String vListaAudiosPagoTotal="";
	String vListaAudiosPagoFacturado="";
	String vListaAudiosPagoTotalito="";
	String vListaAudiosPagoMinimo="";
	String vListaAudiosPagoMinimito="";
	String vListaAudiosScotiapuntos="";
	String vMoneda="";

	String numAudiosSaldoCredito="";
	String numAudiosSaldoLineaDispEfectivo="";
	String numAudiosPagoTotal="";
	String numAudiosPagoFacturado="";
	String numAudiosPagoTotalito="";
	String numAudiosPagoMinimo="";
	String numAudiosPagoMinimito="";
	String numAudiosScotiapuntos="";
	
	Bean_IB71 refBean_IB71;
	
	JSONObject result = new JSONObject();
    
	if(vTipoValidacion.equals("P")){
		try{
			IvrTransaction.Security.SecurityPassword SPOId=new SecurityPassword().EncriptaClave(vPin);
			vPinEncriptado = SPOId.getsPasswordTwo();
			vLetras=SPOId.getsLetras();
			vNumeros=SPOId.getsNumeros();
		}catch(StringIndexOutOfBoundsException ex){
			setLog(strCallUUID + " IBCC-CSF :::: PIN invalido");
		}
	}
	
	CredBalanceController Balance = new CredBalanceController();
	CredBalanceInformation BalInfor = Balance.QIvrCredBalanceInformationController(vInstitucion, vTarjeta,vTipoValidacion, vPinEncriptado, vLetras,vNumeros,vDni);
	vCodErrorRpta = Balance.getCodRetorno();
	
	
    setLog(strCallUUID + " IBCC-CSF :::: ================= BACKEND INVOCACION  IB71-CREDIT BALANCE INFORMATION X PIN ================");
	
    setLog(strCallUUID + " IBCC-CSF :::: Tarjeta : "+ vTarjeta);    
	
	if (vCodErrorRpta == 0) {  
	
		vCodRpta=BalInfor.getERROR();
		
		if (vCodRpta.equals("0000")) {
			
			client_desc=BalInfor.getNombreCliente();
			city_desc=BalInfor.getCiudad();
			ctry_desc=BalInfor.getPais();
			
			refBean_IB71=new Bean_IB71(url_audio,vTipoValidacion,BalInfor,extension);
			
			
			vListaAudiosSaldoCredito=refBean_IB71.getSaldoCredito().getListaAudiosString();
			vListaAudiosSaldoLineaDispEfectivo=refBean_IB71.getSaldoLinDispEfectivo().getListaAudiosString();
			vListaAudiosPagoTotal=refBean_IB71.getListaAudiosPagoTotalString(url_audio, extension);
			vListaAudiosPagoFacturado=refBean_IB71.getListaAudiosPagoFacturadoString(url_audio, extension);
			vListaAudiosPagoMinimo=refBean_IB71.getListaAudiosPagoMinimoString(url_audio, extension);
			vListaAudiosPagoTotalito=refBean_IB71.getListaAudiosPagoTotalitoString(url_audio, extension);
			vListaAudiosPagoMinimito=refBean_IB71.getListaAudiosPagoMinimitoString(url_audio, extension);
			vListaAudiosScotiapuntos=refBean_IB71.getScotiaPuntosSaldTotal().getListaAudiosString();
			numAudiosSaldoCredito=Integer.toString(refBean_IB71.getSaldoCredito().getListaAudios().size());
			numAudiosSaldoLineaDispEfectivo=Integer.toString(refBean_IB71.getSaldoLinDispEfectivo().getListaAudios().size());
			numAudiosPagoTotal=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoTotal).size());
			numAudiosPagoFacturado=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoFacturado).size());
			numAudiosPagoMinimo=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoMinimo).size());
			numAudiosPagoTotalito=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoTotalito).size());
			numAudiosPagoMinimito=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoMinimito).size());
			numAudiosScotiapuntos=Integer.toString(refBean_IB71.getScotiaPuntosSaldTotal().getListaAudios().size());
			vMoneda = refBean_IB71.getMoneda();
			
			setLog(strCallUUID + " IBCC-CSF :::: Nombre del Cliente              " + client_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Ciudad 			 	            " + city_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Pais 			 	            " + ctry_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de tarjeta               " + refBean_IB71.getMoneda());
			setLog(strCallUUID + " IBCC-CSF :::: Indicador de MultiMoneda        " + refBean_IB71.getIndMultiMoneda());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Credito   		  " + refBean_IB71.getSaldoCredito().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Linea Disp. Efectivo   		  " + refBean_IB71.getSaldoLinDispEfectivo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Total Soles               " + refBean_IB71.getPagTotSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Total Dolares               " + refBean_IB71.getPagTotDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Facturado Soles               " + refBean_IB71.getPagFactSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Facturado Dolares             " + refBean_IB71.getPagFactDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Minimo Soles               " + refBean_IB71.getPagMinSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Minimo Dolares             " + refBean_IB71.getPagMinDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Minimito Soles             " + refBean_IB71.getPagMinimitoSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Minimito Dolares             " + refBean_IB71.getPagMinimitoDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Scotia puntos vSaldo x tarj.   	" + refBean_IB71.getScotiaPuntosSaldxTarjeta().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Scotia puntos vSaldo Total    	" + refBean_IB71.getScotiaPuntosSaldTotal().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Totalito Soles             " + refBean_IB71.getPagTotalitoSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Totalito Dolares           " + refBean_IB71.getPagTotalitoDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Linea de Credito                " + refBean_IB71.getLineaCredito().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Linea disp. Efectivo            " + refBean_IB71.getLineaDispEfectivo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Fecha Ultimo vPago				" + refBean_IB71.getFecUltimaPago().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Ultimo vPago realizado mda Nac.  " + refBean_IB71.getUltPagRealizadoSol().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Ultimo vPago realizado mda Ext.    " + refBean_IB71.getUltPagRealizadoDol().getNumero());
			
			
		}		
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              			
	}

	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("client_desc", client_desc);
	result.put("city_desc", city_desc);
	result.put("ctry_desc", ctry_desc);
	result.put("vListaAudiosSaldoCredito", vListaAudiosSaldoCredito);
	result.put("vListaAudiosSaldoLineaDispEfectivo", vListaAudiosSaldoLineaDispEfectivo);
	result.put("vListaAudiosPagoTotal", vListaAudiosPagoTotal);		
	result.put("vListaAudiosPagoFacturado", vListaAudiosPagoFacturado);
	result.put("vListaAudiosPagoMinimo", vListaAudiosPagoMinimo);
	result.put("vListaAudiosPagoTotalito", vListaAudiosPagoTotalito);		
	result.put("vListaAudiosPagoMinimito", vListaAudiosPagoMinimito);
	result.put("vListaAudiosScotiapuntos", vListaAudiosScotiapuntos);
	result.put("numAudiosSaldoCredito", numAudiosSaldoCredito);
	result.put("numAudiosSaldoLineaDispEfectivo", numAudiosSaldoLineaDispEfectivo);
	result.put("numAudiosPagoTotal", numAudiosPagoTotal);		
	result.put("numAudiosPagoFacturado", numAudiosPagoFacturado);
	result.put("numAudiosPagoMinimo", numAudiosPagoMinimo);
	result.put("numAudiosPagoTotalito", numAudiosPagoTotalito);		
	result.put("numAudiosPagoMinimito", numAudiosPagoMinimito);
	result.put("numAudiosScotiapuntos", numAudiosScotiapuntos);
	result.put("vMoneda", vMoneda);
		
	return result;
	    
    
};


%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>