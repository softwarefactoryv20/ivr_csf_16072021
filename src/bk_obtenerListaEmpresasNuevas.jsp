<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.CallableStatement"%>


<%@page import="middleware_support.support.MiddlewareValue"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bk_obtenerListaEmpresasNuevas.jsp");
	//INPUT
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	String vServidor_bd = additionalParams.get("servidor_bd");
	String vUsuario =additionalParams.get("usuario");
	String vClave = additionalParams.get("clave");
	String vPuerto = additionalParams.get("puerto");
	String vCategoria = "T";
	
	//OUTPUT
	String listaEmpresasString="";
	String listaCodNegocioString="";
	String numEmpresas="";
	
	JSONObject result = new JSONObject();
		
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	CallableStatement proc = null;
	String codNegocio="";
	String audioEmpresa="";
	List<String> listaCodNegocio=new ArrayList();
	List<String> listaEmpresas=new ArrayList();
	
	setLog(strCallUUID + " IBCC-CSF :::: =============== BACKEND OBTENER LISTA EMPRESAS NUEVAS ================================");
	
    try 
    {
    	setLog(strCallUUID + " IBCC-CSF :::: Iniciando Conexion SQL");
    	proc = getMSSQLSERVER(strCallUUID, vServidor_bd, vPuerto, vUsuario, vClave).prepareCall("{ call international_data_store.dbo.sp_obtener_empresas_por_categoria(?) }",
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);

      	proc.setString(1, vCategoria);
      	
        setLog(strCallUUID + " IBCC-CSF :::: Iniciando ejecucion de SP");
        rs = proc.executeQuery();
        setLog(strCallUUID + " IBCC-CSF :::: Terminando ejecucion de SP");
        while (rs.next()) {
            codNegocio = rs.getString(1);
            audioEmpresa = rs.getString(3);
            listaCodNegocio.add(codNegocio);
            listaEmpresas.add(url_audio+"/"+audioEmpresa+extension);
            
        }
	} catch (Exception e) {
		setLog(strCallUUID + " IBCC-CSF :::: ERROR - SE EJECUTO INCORRECTAMENTE EL QUERY");
    	e.printStackTrace();
	} finally {
    	try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
        try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
        try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
	}
    
    numEmpresas=Integer.toString(listaEmpresas.size());
    listaEmpresasString=MiddlewareValue.getListaString(listaEmpresas);
    listaCodNegocioString=MiddlewareValue.getListaString(listaCodNegocio);
    
    setLog(strCallUUID + " IBCC-CSF :::: Num Empresas = "+numEmpresas);
    setLog(strCallUUID + " IBCC-CSF :::: Lista Audios Empresas = "+listaEmpresasString);
    setLog(strCallUUID + " IBCC-CSF :::: Lista Cod Negocio = "+listaCodNegocioString);
	    
    
	result.put("numEmpresas", numEmpresas);
	result.put("listaEmpresasString", listaEmpresasString);
	result.put("listaCodNegocioString", listaCodNegocioString);
	return result;
		
    
};

public Connection getMSSQLSERVER(String strCallUUID, String servidorbd, String puerto, String usuario, String clave) throws SQLException {
	Logger log = Logger.getLogger("ConsultaBDMySQL.jsp");
    String url = "jdbc:sqlserver://" + servidorbd + ":" + puerto + ";databaseName=international_data_store;user=" + usuario + ";password=" + clave + ";";
    setLog(strCallUUID + " IBCC-CSF :::: URL = " + url);    
    Connection conexion = null;
    try {
    	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    	conexion = DriverManager.getConnection(url);
    } catch (SQLException e) {
    	setLog(strCallUUID + " IBCC-CSF :::: SQL Exception: " + e.toString());
    } catch (ClassNotFoundException cE) {
    	setLog(strCallUUID + " IBCC-CSF :::: Class Not Found Exception: " + cE.toString());
    } 
    return conexion;
}  

%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>