<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.support.MiddlewareValue"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bk_recorrerArregloEmpresas.jsp");
	//INPUT
	String vIndice = additionalParams.get("indice");
	String vListaEmpresasString = additionalParams.get("listaCodNegocio");
	
	//OUTPUT
	String vCodNegocio="";
	
	JSONObject result = new JSONObject();
	
	int i=Integer.parseInt(vIndice);
	List<String> listaEmpresas=MiddlewareValue.getUrlsFromJsonString(vListaEmpresasString);
	setLog(strCallUUID + " IBCC-CSF :::: =============== BACKEND RECORRER ARREGLO EMPRESAS ===============================");
	vCodNegocio=listaEmpresas.get(i);	
	setLog(strCallUUID + " IBCC-CSF :::: " + i+" : "+vCodNegocio);
	
	result.put("vCodNegocio", vCodNegocio);
	return result;

    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>