<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.classes.TelefonoAfiliado"%>
<%@page import="middleware_support.tramas.Bean_IB77"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bck_Verificar_Telefono_Registrado.jsp");
	//Input
	String vListaTelefonos = additionalParams.get("listaTelefonos");
	String vAni = additionalParams.get("nc_ANI");
	
	//Output
	String telefonoRegistrado = "0";
	
    JSONObject result = new JSONObject();
    
    setLog(strCallUUID + " IBCC-CSF :::: ================ BACKEND VERIFICAR TELEFONO REGISTRADO ================================");
    setLog(strCallUUID + " IBCC-CSF ::::  Lista: "+vListaTelefonos);
    
    List<TelefonoAfiliado> listaTelefonos=Bean_IB77.getListaTelefonos(vListaTelefonos);
    
    setLog(strCallUUID + " IBCC-CSF ::::  Lista Recuperada ");	

	int j=0;
	setLog(strCallUUID + " IBCC-CSF :::: Empieza a buscar el ANI: " + vAni);
	while(j<listaTelefonos.size()){
		TelefonoAfiliado reftTelefonoAfiliado=listaTelefonos.get(j);
		String cadena = reftTelefonoAfiliado.getNroTelefono().trim();
		if(cadena.equals(vAni)){
			telefonoRegistrado= "1";
			setLog(strCallUUID + " IBCC-CSF :::: ANI se encuentra afiliado");
		}
		j++;
	}		
	
	result.put("telefonoRegistrado",telefonoRegistrado);
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>