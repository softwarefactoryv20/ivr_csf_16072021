<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="IvrTransaction.Controller.TipoCambioController"%>
<%@page import="IvrTransaction.modelo.TipoCambio"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB05_Tipo_Cambio.jsp");
	//Input
	String vInstitucion=additionalParams.get("codigoInst") ;  
	String vTipoMoneda=additionalParams.get("vTipoMoneda");
	
	//Output
	String tipoCambioCompra="";
	String tipoCambioVenta="";
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	
	JSONObject result = new JSONObject();
    
	IvrTransaction.Controller.TipoCambioController Cambio = new IvrTransaction.Controller.TipoCambioController();
	IvrTransaction.modelo.TipoCambio TipoCmb = Cambio.QIvrTipoCambio(vInstitucion, vTipoMoneda);
	
	setLog(strCallUUID + " IBCC-CSF ::::  ================ BACKEND INVOCACION IB05-TIPO DE CAMBIO =================");     
	
	
	
	vCodErrorRpta=Cambio.getCodRetorno();
	if (vCodErrorRpta == 0) {
		
		vCodRpta=TipoCmb.getERROR();
		if (TipoCmb.getERROR().equals("0000")) {
			tipoCambioVenta= TipoCmb.getTipoCambioVenta().toString().replace('.', '-');
			tipoCambioCompra= TipoCmb.getTipoCambioCompra().toString().replace('.', '-');
			setLog(strCallUUID + " IBCC-CSF :::: Venta = " + tipoCambioVenta);	
			setLog(strCallUUID + " IBCC-CSF :::: Compra = " + tipoCambioCompra);
			
		}
	
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: HOST ACTIVO? = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Transaccion OK? = " + vCodRpta);  

	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("tipoCambioVenta", tipoCambioVenta);
	result.put("tipoCambioCompra", tipoCambioCompra);
	
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>