<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_CerosAIzquierda.jsp");
	String monto = additionalParams.get("vIngresarMonto");	
    
    JSONObject result = new JSONObject();
    
	setLog(strCallUUID + " IBCC-CSF :::: Cadena de Monto Anterior: " + monto);
    
    String nuevoMontoAbono = padLeftZeros(monto);
    
    setLog(strCallUUID + " IBCC-CSF :::: Cadena de Monto Nuevo: " + nuevoMontoAbono);
    
    result.put("nuevoMontoAbono", nuevoMontoAbono);
    
    return result;
    
};

public String padLeftZeros(String cadena){
	
	if(cadena.length()<15){
		cadena=String.format("%0"+(15-cadena.length())+"d%s",0,cadena);
	}
	return cadena;
}

%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>