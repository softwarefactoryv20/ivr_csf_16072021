<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
       
        
    String ServidorBus = additionalParams.get("ServidorBus");
    String PuertoBus = additionalParams.get("PuertoBus");  
    String param1 = additionalParams.get("Cli_Cod_Inst");  
    String param2 = additionalParams.get("Cli_dni");
    String param3 = additionalParams.get("Cli_TipIdent");  
		
    JSONObject result = new JSONObject();
    String strCallUUID = state.getString("CallUUID");

		String url = "http://"+ServidorBus+":"+PuertoBus+"/ebdperu.conector/rest/ib99/ivrib99";
		String Body = "{\"institucion\": \""+param1+"\",\"nrodocident\": \""+param2+"\",\"tipodocident\": \""+param3+"\"}";
				
		setLog(strCallUUID + " IBCC-CSF :::: Sending 'POST' request to URL = " + url);
		setLog(strCallUUID + " IBCC-CSF :::: Sending 'POST' request to Body = " + Body);
		
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();						
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("charset", "utf-8");
		con.setRequestProperty("apikey", "89d02f12ab242bd922af20fcc");
		byte[] input = Body.getBytes();
		con.setRequestProperty("Content-Length", "1091");
    	
    	OutputStream os = con.getOutputStream();
    	byte[] input2 = Body.getBytes("utf-8");
    	os.write(input,0,input.length);
    	
    	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
    	
   		//print result
		String tempString = response.toString();
		String CodigoRetorno = "NULL";
		String tarjeta_Credito = "NULL";
		String linea_Paralela = "NULL";
		String tarjeta_Linea_Paralela = "NULL";
		String abrir_Linea_Paralela = "NULL";
		
		try{
		JSONObject jsonobj = new JSONObject(tempString);
		CodigoRetorno = jsonobj.getString("ERROR");
		tarjeta_Credito = jsonobj.getString("tarjeta_Credito");	
		linea_Paralela = jsonobj.getString("linea_Paralela");	
		tarjeta_Linea_Paralela = jsonobj.getString("tarjeta_Linea_Paralela");	
		abrir_Linea_Paralela = jsonobj.getString("abrir_Linea_Paralela");	
			
		}catch(JSONException Error){
		setLog(strCallUUID + " IBCC-CSF :::: Response Error Json = " + Error);
		}
		
		setLog(strCallUUID + " IBCC-CSF :::: Response Code = " + tempString);	
		setLog(strCallUUID + " IBCC-CSF :::: Response CodigoRetorno = " + CodigoRetorno);	
		setLog(strCallUUID + " IBCC-CSF :::: Response tarjeta_Credito = " + tarjeta_Credito);
		setLog(strCallUUID + " IBCC-CSF :::: Response linea_Paralela = " + linea_Paralela);
		setLog(strCallUUID + " IBCC-CSF :::: Response tarjeta_Linea_Paralela = " + tarjeta_Linea_Paralela);
		setLog(strCallUUID + " IBCC-CSF :::: Response abrir_Linea_Paralela = " + abrir_Linea_Paralela);		
	
	result.put("ParamOut1", CodigoRetorno);
	result.put("ParamOut2", tarjeta_Credito);
	result.put("ParamOut3", linea_Paralela);
	result.put("ParamOut4", tarjeta_Linea_Paralela);
	result.put("ParamOut6", abrir_Linea_Paralela);

    return result;

};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>

<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.cert.X509Certificate"%>
<%@page import="javax.net.ssl.HostnameVerifier"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="javax.net.ssl.SSLSession"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="javax.net.ssl.X509TrustManager"%>

<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>