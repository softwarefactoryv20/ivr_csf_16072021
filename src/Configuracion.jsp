<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="Encripta.IvrString.Encripta"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	///-- INICIO - Leer Archivo de Configuracion
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	String nc_DNIS = additionalParams.get("nc_DNIS");
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Configuracion.jsp");
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Primera linea de LOG Configuracion.jsp");
		
	path = consultarIp() + "/compartido/inicio_RetornoSeguros_SBP.xml";
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Ruta XML = " + path);
	
	java.io.File file = new java.io.File(path);
	Properties properties = new Properties();
	java.io.InputStream inputStream=null;
	
	if(file.exists()){
		properties.clear();
		
		try {
			inputStream = new java.io.FileInputStream(file);
		    } 
		catch (FileNotFoundException e) {
			e.printStackTrace(); }
		
		try {
			properties.loadFromXML(inputStream);
		    } 
		catch (InvalidPropertiesFormatException e) {
 			 e.printStackTrace(); } 
		catch (IOException e) {
			e.printStackTrace(); }		
	}

	String servidorwas = properties.getProperty("SBP_servidorwas");
	String servidor_bd = properties.getProperty("SBP_servidor_bd");
	String usuario = properties.getProperty("SBP_usuario");
	String clave = properties.getProperty("SBP_clave");
	String puerto = properties.getProperty("SBP_puerto");
	String TxFromCredit = properties.getProperty("SBP_TxFromCredit");
	String codigoInst = properties.getProperty("codigoInst");

	String SBP_DID_1 = properties.getProperty("SBP_DID_1");
	String SBP_DID_2 = properties.getProperty("SBP_DID_2");
	String SBP_DID_3 = properties.getProperty("SBP_DID_3");
	String SBP_DID_4 = properties.getProperty("SBP_DID_4");
	String SBP_DID_5 = properties.getProperty("SBP_DID_5");
	String SBP_DID_6 = properties.getProperty("SBP_DID_6");
	String SBP_DID_7 = properties.getProperty("SBP_DID_7");
	String SBP_DID_8 = properties.getProperty("SBP_DID_8");
	String SBP_DID_9 = properties.getProperty("SBP_DID_9");
	String SBP_DID_10 = properties.getProperty("SBP_DID_10");

	String SBP_VDN_1 = properties.getProperty("SBP_VDN_1");
	String SBP_VDN_2 = properties.getProperty("SBP_VDN_2");
	String SBP_VDN_3 = properties.getProperty("SBP_VDN_3");
	String SBP_VDN_4 = properties.getProperty("SBP_VDN_4");
	String SBP_VDN_5 = properties.getProperty("SBP_VDN_5");
	String SBP_VDN_6 = properties.getProperty("SBP_VDN_6");
	String SBP_VDN_7 = properties.getProperty("SBP_VDN_7");
	String SBP_VDN_8 = properties.getProperty("SBP_VDN_8");
	String SBP_VDN_9 = properties.getProperty("SBP_VDN_9");
	String SBP_VDN_10 = properties.getProperty("SBP_VDN_10");

	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Servidor WAS: " + servidorwas);
	
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Servidor Base de Datos: " + servidor_bd);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Usuario: " + usuario);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Clave: " + clave);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Puerto: " + puerto);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Transferencia de Credito Activa?: " + TxFromCredit);	
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Codigo de Institucion: " + codigoInst);


	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_1: " + SBP_DID_1);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_2: " + SBP_DID_2);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_3: " + SBP_DID_3);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_4: " + SBP_DID_4);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_5: " + SBP_DID_5);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_6: " + SBP_DID_6);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_7: " + SBP_DID_7);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_8: " + SBP_DID_8);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_9: " + SBP_DID_9);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_DID_10: " + SBP_DID_10);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_1: " + SBP_VDN_1);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_2: " + SBP_VDN_2);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_3: " + SBP_VDN_3);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_4: " + SBP_VDN_4);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_5: " + SBP_VDN_5);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_6: " + SBP_VDN_6);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_7: " + SBP_VDN_7);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_8: " + SBP_VDN_8);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_9: " + SBP_VDN_9);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: SBP_VDN_10: " + SBP_VDN_10);

	
	// INICIO - Desencriptar CLAVE BD
		try{
			Encripta encripta = new Encripta();
			String claveDesencriptada = encripta.decrypt(clave);
			String usuarioDesencriptado = encripta.decrypt(usuario);
			clave = claveDesencriptada;
			usuario = usuarioDesencriptado;			
		}catch (Exception e) {
			setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Error al desencriptar Clave BD: " + e);
		}	
	// FIN - Desencriptar CLAVE BD
	
	
	///-- FIN - Leer Archivo de Configuracion
	
	///-- INICIO - Locucion de Bienvenida.
	String horario = "";         
    
	Calendar calendario = Calendar.getInstance();
	int hour = calendario.get(Calendar.HOUR_OF_DAY);
	int minute = calendario.get(Calendar.MINUTE);
   	int second = calendario.get(Calendar.SECOND);
	int nro_dia = calendario.get(Calendar.DAY_OF_WEEK);
	String dia = String.valueOf(nro_dia);
	
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: El dia de hoy es : " + dia);
	
   	String shour = Integer.toString(hour);
   	int lhour = shour.length();
   	String sminute = Integer.toString(minute);
   	int lminute = sminute.length();
   	String ssecond = Integer.toString(second);
   	int lsecond = ssecond.length();
   	
   	if(lhour == 1){
   		shour = "0" + shour;}
   	if(lminute == 1){
   		sminute = "0" + sminute;}
   	if(lsecond == 1){
   		ssecond = "0" + ssecond;}
   	
   	String horaActual = shour + sminute + ssecond;
   	setLog(strCallUUID + " La hora actual es : " + horaActual);
   	
   	int ihoraACtual = Integer.parseInt(horaActual);
   	
   	if (ihoraACtual >= 000000 && ihoraACtual < 120000)
   	{
   		//Buenos dias
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruCSF/AudiosCSF/cre_welcome_1.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}
   	if (ihoraACtual >= 120000 && ihoraACtual < 180000)
   	{
   		//Buenos Tardes  
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruCSF/AudiosCSF/cre_welcome_2.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}
   	if (ihoraACtual >= 180000 && ihoraACtual < 235959)
   	{
   		//Buenos Noches
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruCSF/AudiosCSF/cre_welcome_3.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}     	 
	
	///-- FIN - Locucion de Bienvenida.
	
    JSONObject result = new JSONObject();    
    
    result.put("servidorwas", servidorwas);
    result.put("servidor_bd", servidor_bd);
    result.put("usuario", usuario);
    result.put("clave", clave);
    result.put("puerto", puerto);
    result.put("TxFromCredit", TxFromCredit);
    result.put("codigoInst", codigoInst);
    result.put("horario", horario);
    result.put("dia", dia);
    result.put("horaActual", horaActual);

		
	result.put("SBP_DID_1", SBP_DID_1);
	result.put("SBP_DID_2", SBP_DID_2);
	result.put("SBP_DID_3", SBP_DID_3);
	result.put("SBP_DID_4", SBP_DID_4);
	result.put("SBP_DID_5", SBP_DID_5);
	result.put("SBP_DID_6", SBP_DID_6);
	result.put("SBP_DID_7", SBP_DID_7);
	result.put("SBP_DID_8", SBP_DID_8);
	result.put("SBP_DID_9", SBP_DID_9);
	result.put("SBP_DID_10", SBP_DID_10);
	result.put("SBP_VDN_1", SBP_VDN_1);
	result.put("SBP_VDN_2", SBP_VDN_2);
	result.put("SBP_VDN_3", SBP_VDN_3);
	result.put("SBP_VDN_4", SBP_VDN_4);
	result.put("SBP_VDN_5", SBP_VDN_5);
	result.put("SBP_VDN_6", SBP_VDN_6);
	result.put("SBP_VDN_7", SBP_VDN_7);
	result.put("SBP_VDN_8", SBP_VDN_8);
	result.put("SBP_VDN_9", SBP_VDN_9);
	result.put("SBP_VDN_10", SBP_VDN_10);

    return result;    
};

public String consultarIp() throws UnknownHostException {
	InetAddress address = InetAddress.getLocalHost();
	//String sHostName = address.getHostName();
	 byte[] bIPAddress = address.getAddress();
	 String sIPAddress = "";
		 for (int x=0; x< bIPAddress.length; x++) {
			 if (x > 0) {
				 sIPAddress += ".";
			 }
		 sIPAddress += bIPAddress[x] & 255;
		 }
	return "//"+sIPAddress;
}
	
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.InvalidPropertiesFormatException"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>