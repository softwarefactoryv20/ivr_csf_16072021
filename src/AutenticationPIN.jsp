<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%!
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("AutenticationPIN.jsp");
	String vDoc = "1";
	String vTipo = "1";
	String num_doc = additionalParams.get("varOpcIvr");   
	setLog(strCallUUID + " IBCC-CSF :::: OPCION 2 - Autentication PIN");
	
    JSONObject result = new JSONObject();
    
    if(num_doc.length() == 1)    
    	vDoc = "0";    
    else
    {
    	if(num_doc.length() == 4)
    	{	
    		vDoc = "4";	
    		vTipo = "P";
    	}
    }
    setLog(strCallUUID + " IBCC-CSF :::: variable vDoc : " + vDoc);
    
    result.put("vDoc", vDoc);
    result.put("vTipo", vTipo);
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>