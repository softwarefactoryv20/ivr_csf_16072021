<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("http_Request_Transfer.jsp");
	setLog(strCallUUID + " IBCC-CSF :::: Primera linea de LOG http_Request_Transfer.jsp");	
	
	String USER_AGENT = "Mozilla/5.0";
	String EDUID = additionalParams.get("eduid");
	String VDN = additionalParams.get("vdn");
	setLog(strCallUUID + " IBCC-CSF :::: VDN = " + VDN);
    String urlTX = "http://SPCC-WTS2.per.bns:9171/bootcmp.htm?action=start_script&eduid="+EDUID+"&script=httpvox.transfer&destination="+ VDN;

	JSONObject result = new JSONObject();
    
	try{
        URL obj = new URL(urlTX);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		setLog(strCallUUID + " IBCC-CSF :::: Sending 'GET' request to URL : " + urlTX);
		setLog(strCallUUID + " IBCC-CSF :::: Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		setLog(strCallUUID + " IBCC-CSF :::: EL RESPONSE - REQUEST TRANSFER - ES : " + response.toString());
	}
	catch(Exception ex)
	{
		setLog(strCallUUID + " IBCC-CSF :::: " + ex.toString());
	}
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>