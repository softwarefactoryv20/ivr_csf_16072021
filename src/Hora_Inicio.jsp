<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%!
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Hora_Inicio.jsp");
	Calendar calendario = Calendar.getInstance();
	int anio, mes, dia, hora, minuto, segundo;
	
	setLog(strCallUUID + " IBCC-CSF :::: Primera linea de LOG BRU - OPCION 0");
    JSONObject result = new JSONObject();    
    
	anio = calendario.get(Calendar.YEAR);
	mes = calendario.get(Calendar.MONTH)+1;
	dia = calendario.get(Calendar.DAY_OF_MONTH);
	hora = calendario.get(Calendar.HOUR_OF_DAY);
	minuto = calendario.get(Calendar.MINUTE);
	segundo = calendario.get(Calendar.SECOND);
	String v_Start_Time = anio + "," + mes + "," + dia + "," + hora + "," + minuto + "," + segundo;
	setLog(strCallUUID + " IBCC-CSF :::: Fecha y Hora de INICIO = " + v_Start_Time);
	result.put("v_Start_Time", v_Start_Time);
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>