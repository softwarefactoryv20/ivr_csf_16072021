<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_subSegmento.jsp");
	String SBP_VDN_MP_OPC_0 = "";
	String subSegmento = additionalParams.get("subSegmento");
	String SBP_SubSeg_1_ini = additionalParams.get("SBP_SubSeg_1_ini");
	String SBP_SubSeg_2_ini = additionalParams.get("SBP_SubSeg_2_ini");
	String SBP_SubSeg_3_ini = additionalParams.get("SBP_SubSeg_3_ini");
	String SBP_SubSeg_4_ini = additionalParams.get("SBP_SubSeg_4_ini");
	String SBP_SubSeg_1_fin = additionalParams.get("SBP_SubSeg_1_fin");
	String SBP_SubSeg_2_fin = additionalParams.get("SBP_SubSeg_2_fin");
	String SBP_SubSeg_3_fin = additionalParams.get("SBP_SubSeg_3_fin");
	String SBP_SubSeg_4_fin = additionalParams.get("SBP_SubSeg_4_fin");
	String SBP_Prioridad_VDN_1 = additionalParams.get("SBP_Prioridad_VDN_1");
	String SBP_Prioridad_VDN_2 = additionalParams.get("SBP_Prioridad_VDN_2");
	String SBP_Prioridad_VDN_3 = additionalParams.get("SBP_Prioridad_VDN_3");
	String SBP_Prioridad_VDN_4 = additionalParams.get("SBP_Prioridad_VDN_4");
	
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento retorna de trama: " + subSegmento);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento SBP_SubSeg_1_ini: " + SBP_SubSeg_1_ini);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento SBP_SubSeg_1_fin: " + SBP_SubSeg_1_fin);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento SBP_SubSeg_2_ini: " + SBP_SubSeg_2_ini);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento SBP_SubSeg_2_fin: " + SBP_SubSeg_2_fin);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento SBP_SubSeg_3_ini: " + SBP_SubSeg_3_ini);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento SBP_SubSeg_3_fin: " + SBP_SubSeg_3_fin);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento SBP_SubSeg_4_ini: " + SBP_SubSeg_4_ini);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento SBP_SubSeg_4_fin: " + SBP_SubSeg_4_fin);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento - Prioridad 1 VDN a Transferir: " + SBP_Prioridad_VDN_1);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento - Prioridad 2 VDN a Transferir: " + SBP_Prioridad_VDN_2);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento - Prioridad 3 VDN a Transferir: " + SBP_Prioridad_VDN_3);
	setLog(strCallUUID + " IBCC-CSF :::: SubSegmento - Prioridad 4 VDN a Transferir: " + SBP_Prioridad_VDN_4);	
    
    JSONObject result = new JSONObject();
    
    if(Integer.parseInt(SBP_SubSeg_1_ini)<= Integer.parseInt(subSegmento) && Integer.parseInt(SBP_SubSeg_1_fin)>= Integer.parseInt(subSegmento)){
    	SBP_VDN_MP_OPC_0 = SBP_Prioridad_VDN_1;
    }
	else if(Integer.parseInt(SBP_SubSeg_2_ini)<= Integer.parseInt(subSegmento) && Integer.parseInt(SBP_SubSeg_2_fin)>= Integer.parseInt(subSegmento)){
    	SBP_VDN_MP_OPC_0 = SBP_Prioridad_VDN_2;
    }
	else if(Integer.parseInt(SBP_SubSeg_3_ini)<= Integer.parseInt(subSegmento) && Integer.parseInt(SBP_SubSeg_3_fin)>= Integer.parseInt(subSegmento)){
    	SBP_VDN_MP_OPC_0 = SBP_Prioridad_VDN_3;
    }
	else if(Integer.parseInt(SBP_SubSeg_4_ini)<= Integer.parseInt(subSegmento) && Integer.parseInt(SBP_SubSeg_4_fin)>= Integer.parseInt(subSegmento)){
    	SBP_VDN_MP_OPC_0 = SBP_Prioridad_VDN_4;
    }
    
    setLog(strCallUUID + " IBCC-CSF :::: La Proiridad transferira al VDN SBP_VDN_MP_OPC_0: " + SBP_VDN_MP_OPC_0);
    
    result.put("SBP_VDN_MP_OPC_0", SBP_VDN_MP_OPC_0);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>