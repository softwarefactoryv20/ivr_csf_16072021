<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="middleware_support.support.MiddlewareDate"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_NumeroMoneda.jsp");
	String listaAudioMontoMonedaString = additionalParams.get("audioMontoMoneda");
	String servidorwas = additionalParams.get("servidorwas");	
	int cantMontoMoneda = Integer.parseInt(additionalParams.get("vCantMontoMoneda"));
	int contadorMontoMoneda = Integer.parseInt(additionalParams.get("contadorMontoMoneda"));
	String vAudio = "";
    JSONObject result = new JSONObject();
        
    setLog(strCallUUID + " IBCC-CSF :::: === BACKEND ARREGLO ================================================");
    setLog(strCallUUID + " IBCC-CSF :::: === Contador: "+contadorMontoMoneda);
    setLog(strCallUUID + " IBCC-CSF :::: === NumCuentas: "+cantMontoMoneda );
    setLog(strCallUUID + " IBCC-CSF :::: === Lista: "+listaAudioMontoMonedaString);
    
    setLog(strCallUUID + " IBCC-CSF :::: ========= Inicio uso GSON =========");
	Gson gson = new Gson();
    setLog(strCallUUID + " IBCC-CSF :::: Objeto GSON creado");
  	
	JsonParser parser = new JsonParser();
    setLog(strCallUUID + " IBCC-CSF :::: Objeto JsonParser creado");
     
    JsonArray array = parser.parse(listaAudioMontoMonedaString).getAsJsonArray();
    setLog(strCallUUID + " IBCC-CSF :::: Objeto JsonArray creado");
     
    List<Bean_NumeroTipoMoneda> listaMontoMoneda =  new ArrayList<Bean_NumeroTipoMoneda>();
    setLog(strCallUUID + " IBCC-CSF :::: Lista Bean creada");
     
	for(final JsonElement json: array){
		Bean_NumeroTipoMoneda entity = gson.fromJson(json, Bean_NumeroTipoMoneda.class);
		listaMontoMoneda.add(entity);
	}
	setLog(strCallUUID + " IBCC-CSF :::: Lista Armada");
	setLog(strCallUUID + " IBCC-CSF :::: --------Fin uso GSON-------");

	setLog(strCallUUID + " IBCC-CSF :::: ----Lista Recuperada----");		
	
	//--- INICIO - Recorriendo el Arreglo (Ultimos 3 digitos de la tarjeta)
	Bean_NumeroTipoMoneda refCuentai=listaMontoMoneda.get(contadorMontoMoneda);
	vAudio = refCuentai.getRutaMoneda().trim();
	//--- FIN - Recorriendo el Arreglo (Ultimos 3 digitos de la tarjeta)
	
    result.put("vAudio",vAudio);
    return result;
    
};

public class Bean_NumeroTipoMoneda implements Serializable {

	private String rutaMoneda;

	public String getRutaMoneda() {
		return rutaMoneda;
	}

	public void setRutaMoneda(String rutaMoneda) {
		this.rutaMoneda = rutaMoneda;
	}
}
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>