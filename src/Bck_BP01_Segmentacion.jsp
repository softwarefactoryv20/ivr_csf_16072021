<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="IvrTransaction.Security.SecurityPassword"%>
<%@page import="IvrTransaction.modelo.ClientePremium02"%>
<%@page import="IvrTransaction.Controller.ClientePremiumController02"%>


<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
		String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
		//sesion =  strCallUUID + ", ";
		Logger log = Logger.getLogger("Bck_BP01_Segmentacion.jsp");
	    //Input
		String vInstitucion = additionalParams.get("codInst") ;  
  		String vPais = "589";
  		String vTipo = additionalParams.get("vTipo") ;  
  		String vDocumento = vTipo + additionalParams.get("vDoc");
  		String vPin = additionalParams.get("vPin");
  		  		
  		//Output
  		String Paterno="";
  	    String Materno="";
  	    String Nombre1="";
  	    String Nombre2="";
  	  	String Nombre=""; 
  	    String TipoDocumento=""; 
  	    String NroDocumento=""; 
  	    String Segmento=""; 
  	    String DescSegmento="";
  	    String Cumpleanios=""; 
  	    String CuentaBT="";
  	    String Tarjeta=""; 
  	    String vPremium="NO";
  	    int vCodErrorRpta = 0;
  		String vCodRpta="0000"; 
  		
  		JSONObject result = new JSONObject();
  		
  		ClientePremiumController02 refController=new ClientePremiumController02(); 
  		IvrTransaction.Security.SecurityPassword SPOId=new SecurityPassword().EncriptaClave(vPin);
  		
  		String pinEncriptado = SPOId.getsPasswordTwo();
  		String letras=SPOId.getsLetras();
  		String numeros=SPOId.getsNumeros();
  		
  		
  		ClientePremium02 refClient = refController.QIvrClientePremium(vInstitucion, vPais,vDocumento, pinEncriptado, letras, numeros) ;

  		setLog(strCallUUID + " IBCC-CSF :::: =============== INVOCACION - BP02 : CLIENTE PREMIUM  ================ ");
  		
  		vCodErrorRpta=refController.getCodRetorno();
  		
  		if (vCodErrorRpta == 0) {  
  			
  			vCodRpta=refClient.getERROR();
  			
  			if (vCodRpta.equals("0000")) {
  				
  				Paterno=refClient.getPaterno().trim();
  				Materno=refClient.getMaterno().trim();
  				Nombre1=refClient.getNombre1().trim();
  				Nombre2=refClient.getNombre2().trim();
  				Nombre=Nombre1 + " " + Nombre2 + " " + Paterno + " " + Materno;
  				TipoDocumento=refClient.getTipoDocumento().trim();
  				NroDocumento=refClient.getNroDocumento().trim();
  				Segmento=refClient.getSegmento().trim();
  				DescSegmento=refClient.getDescSegmento().trim();
  				Cumpleanios=refClient.getCumpleanios().trim();
  				CuentaBT=refClient.getCuentaBT().trim();
  				Tarjeta=refClient.getTarjeta().trim();
  				
  				setLog(strCallUUID + " IBCC-CSF :::: Paterno = " + Paterno);
  				setLog(strCallUUID + " IBCC-CSF :::: Materno = " + Materno);
  				setLog(strCallUUID + " IBCC-CSF :::: Nombre1 = " + Nombre1);
  				setLog(strCallUUID + " IBCC-CSF :::: Nombre2 = " + Nombre2);
  				setLog(strCallUUID + " IBCC-CSF :::: Nombre Completo = " + Nombre);
  				setLog(strCallUUID + " IBCC-CSF :::: TipoDocumento = " + TipoDocumento);
  				setLog(strCallUUID + " IBCC-CSF :::: NroDocumento = " + NroDocumento);
  				setLog(strCallUUID + " IBCC-CSF :::: Segmento = " + Segmento);
  				setLog(strCallUUID + " IBCC-CSF :::: DescSegmento = " + DescSegmento);
  				setLog(strCallUUID + " IBCC-CSF :::: Cumpleanios = " + Cumpleanios);
  				setLog(strCallUUID + " IBCC-CSF :::: CuentaBT = " + CuentaBT);
  				setLog(strCallUUID + " IBCC-CSF :::: Subsegmento = " + Tarjeta);
  				
  			}
  		}
  				
  		setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
  		setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
  		
  		result.put("vCodRpta", vCodRpta);
  	    result.put("vCodErrorRpta", vCodErrorRpta);
  	    result.put("Nombre", Nombre);
  	    result.put("TipoDocumento", TipoDocumento);
  	    result.put("NroDocumento", NroDocumento);
  	    result.put("Segmento", Segmento);
  	    result.put("DescSegmento", DescSegmento);
  	    result.put("Cumpleanios", Cumpleanios);
  	    result.put("CuentaBT", CuentaBT);
  	    result.put("Tarjeta", Tarjeta);
  	    result.put("Subsegmento", Tarjeta);
  	    result.put("vPremium", vPremium);
	    
    
    
	    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>