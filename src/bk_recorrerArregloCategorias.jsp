<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.support.MiddlewareValue"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bk_recorrerArregloCategorias.jsp");
		//INPUT
		String vIndice = additionalParams.get("indice");
		String vListaCategoriasString = additionalParams.get("listaCategorias");
		
		//OUTPUT
		String vCategoriaId="";
		
		JSONObject result = new JSONObject();
		
		int i=Integer.parseInt(vIndice);
		List<String> listaCategorias=MiddlewareValue.getUrlsFromJsonString(vListaCategoriasString);
		setLog(strCallUUID + " IBCC-CSF :::: =============== BACKEND RECORRER ARREGLO CATEGORIAS ===============================");
		vCategoriaId=listaCategorias.get(i);	
		setLog(strCallUUID + " IBCC-CSF :::: " + i+" : "+ vCategoriaId);
		
		
		result.put("categoriaId", vCategoriaId);
		return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>