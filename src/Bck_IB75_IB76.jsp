<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>


<%@page import="middleware_support.classes.ServicioAfiliado"%>

<%@page import="middleware_support.tramas.Bean_IB75_IB76"%>

<%@page import="IvrTransaction.RequestM3.Afiliaciones"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB75_IB76.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTipoAccion=additionalParams.get("tipoAccion");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vCodNegocio= additionalParams.get("codigoNegocio");
	String vNumServicio= additionalParams.get("nroServicio");
	String vCategoria= additionalParams.get("categoriaId");
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String listaServiciosString="";
	int numServicios = 0;
	String vOrigen = "";
	String vRuc = "";
	String tipoServicio = "";
	
	Bean_IB75_IB76 refBean_IB75_IB76;
	JSONObject result = new JSONObject();
	
	IvrTransaction.RequestM3.Afiliaciones modelo=new IvrTransaction.RequestM3.Afiliaciones();
	modelo.IvrAfiliaciones(vInstitucion, vTipoAccion, vTarjeta, vCodNegocio, vNumServicio, vCategoria);
	vCodErrorRpta=modelo.getCodRetorno();
	
	setLog(strCallUUID + " IBCC-CSF :::: ============== BACKEND INVOCACION - IB75/IB76 : AGREGAR/ELIMINAR SERVICIOS ===============");
		
	
	if (vCodErrorRpta==0) {
			
		vCodRpta=modelo.getERROR();
		refBean_IB75_IB76=new Bean_IB75_IB76(url_audio,modelo,extension);
		setLog(strCallUUID + " IBCC-CSF :::: Num servicio : "+vNumServicio);
		setLog(strCallUUID + " IBCC-CSF :::: Cod Negocio : "+vCodNegocio);
			
		if(vCodRpta.equals("0000")){
			
			listaServiciosString=refBean_IB75_IB76.getRefBean_IB76().getListaServiciosString();
			numServicios=refBean_IB75_IB76.getRefBean_IB76().getListaServiciosAfiliados().size();
			
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB75------------");
			vRuc = refBean_IB75_IB76.getRefBean_IB75().getRucEmpServicio();
			vOrigen = refBean_IB75_IB76.getRefBean_IB75().getOrigenInstitucion();
			tipoServicio = refBean_IB75_IB76.getRefBean_IB75().getTipoServicio();
			
			setLog(strCallUUID + " IBCC-CSF :::: Ruc Emp. Servicio : " + vOrigen);
			setLog(strCallUUID + " IBCC-CSF :::: Origen Institucion : " + vRuc);
			setLog(strCallUUID + " IBCC-CSF :::: Tipo Servicio : " + tipoServicio);
			
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB76------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: N° Tarjeta : " + refBean_IB75_IB76.getRefBean_IB76().getNumTarjeta());
			setLog(strCallUUID + " IBCC-CSF :::: Mas Datos : " + refBean_IB75_IB76.getRefBean_IB76().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Sig. Pagina : " + refBean_IB75_IB76.getRefBean_IB76().getSiguientePagina());
			setLog(strCallUUID + " IBCC-CSF :::: N° Registros : " + refBean_IB75_IB76.getRefBean_IB76().getNroRegistros());
			
			setLog(strCallUUID + " IBCC-CSF :::: Servicios");
			
			
			setLog(strCallUUID + " IBCC-CSF :::: RUC         TipoServ     NumServ          Zonal     Descr Servicio      Decr. Serv. Usuario   Origen  Tipo Afil. Cod.negocio");
			
			
			for(ServicioAfiliado refServicioAfiliado: refBean_IB75_IB76.getRefBean_IB76().getListaServiciosAfiliados()){
				
				
				setLog(strCallUUID + " IBCC-CSF :::: " +	String.format("%1$-11s",refServicioAfiliado.getRucEmpServicio())+ "   "+
						String.format("%1$-3s",refServicioAfiliado.getTipoServicio())+"      "+
						String.format("%1$-16s",refServicioAfiliado.getNumServicio())+"     "+
						String.format("%1$-3s",refServicioAfiliado.getZonal())+"      "+
						String.format("%1$-17s",refServicioAfiliado.getDscrServicio())  +"    "+
						String.format("%1$-17s",refServicioAfiliado.getDscrServUsuario())	+"     "	+ 
						String.format("%1$-5s",refServicioAfiliado.getOrigenInstitucion()) +"    "	+
						String.format("%1$-5s",refServicioAfiliado.getTipoAfiliacion()) +"     "	+
						String.format("%1$-20s",refServicioAfiliado.getCodigoNegocio()));
		 
			}
			
			setLog(strCallUUID + " IBCC-CSF :::: Numero de servicios  : "+numServicios);
			setLog(strCallUUID + " IBCC-CSF :::: Lista servicios String : "+listaServiciosString);
		}
		
			
	}

	
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	result.put("numServicios", Integer.toString(numServicios));
	result.put("listaServicios",listaServiciosString);
	result.put("vOrigen",vOrigen);
	result.put("vRuc",vRuc);
	result.put("tipoServicio",tipoServicio);
    
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	
    return result;
  
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>