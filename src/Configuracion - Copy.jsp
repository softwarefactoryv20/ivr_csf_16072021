<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="Encripta.IvrString.Encripta"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	///-- INICIO - Leer Archivo de Configuracion
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	String nc_DNIS = additionalParams.get("nc_DNIS");
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Configuracion.jsp");
	setLog(strCallUUID + " IBCC-CSF :::: Primera linea de LOG Configuracion.jsp");
	
	//--- INICIO vdn_cabinas.xml
	
	String path_cabina = null;
	try {
		
		path_cabina = consultarIp() + "/compartido/inicio_Cabinas.xml";
		setLog(strCallUUID + " IBCC-CSF :::: Ruta XML Cabinas = " + path_cabina);
	} catch (UnknownHostException e1) {
		e1.printStackTrace();
	}
	java.io.File file_cabina = new java.io.File(path_cabina);
	Properties properties_cabina = new Properties();
	java.io.InputStream inputStream_cab=null;
	
	if(file_cabina.exists()){
		properties_cabina.clear();
		
		try {
			inputStream_cab = new java.io.FileInputStream(file_cabina);
		    } 
		catch (FileNotFoundException e) {
			e.printStackTrace(); }
		
		try {
			properties_cabina.loadFromXML(inputStream_cab);
		    } 
		catch (InvalidPropertiesFormatException e) {
 			 e.printStackTrace(); } 
		catch (IOException e) {
			e.printStackTrace(); }		
	}
	String SBP_VDN_Cabinas = properties_cabina.getProperty("CSF_VDN_Cabinas");
	setLog(strCallUUID + " IBCC-CSF :::: VDN - Banca Telefonica o Cabinas: " + SBP_VDN_Cabinas);
	
	//--- FIN vdn_cabinas.xml
	
	String path=null;
	try {
		
		if(nc_DNIS.equals(SBP_VDN_Cabinas)){
			path = consultarIp() + "/compartido/inicio_CSF_Cab.xml";
		}else{
			path = consultarIp() + "/compartido/inicio_CSF.xml";
		}
		setLog(strCallUUID + " IBCC-CSF :::: Ruta XML = " + path);
	} catch (UnknownHostException e1) {
		e1.printStackTrace();
	}
	java.io.File file = new java.io.File(path);
	Properties properties = new Properties();
	java.io.InputStream inputStream=null;
	
	if(file.exists()){
		properties.clear();
		
		try {
			inputStream = new java.io.FileInputStream(file);
		    } 
		catch (FileNotFoundException e) {
			e.printStackTrace(); }
		
		try {
			properties.loadFromXML(inputStream);
		    } 
		catch (InvalidPropertiesFormatException e) {
 			 e.printStackTrace(); } 
		catch (IOException e) {
			e.printStackTrace(); }		
	}
	String servidorwas = properties.getProperty("CSF_servidorwas");
	String TrxAgente = properties.getProperty("CSF_VDN_TrxAgente");
	String TransActivaMP = properties.getProperty("CSF_TransActivaMP");
	String TransActivaMP_Opc0 = properties.getProperty("CSF_TransActivaMP_Opc0");
	String TransActivaMP_Opc1 = properties.getProperty("CSF_TransActivaMP_Opc1");
	String TransActivaMP_Opc2 = properties.getProperty("CSF_TransActivaMP_Opc2");
	String TransActivaMP_Opc3 = properties.getProperty("CSF_TransActivaMP_Opc3");
	String TransActivaMP_Opc4 = properties.getProperty("CSF_TransActivaMP_Opc4");
	String TransActivaMP_Opc5 = properties.getProperty("CSF_TransActivaMP_Opc5");
	String TransActivaMP_Opc8 = properties.getProperty("CSF_TransActivaMP_Opc8");
	String TransActivaMP_OpcM = properties.getProperty("CSF_TransActivaMP_OpcM");
	String servidor_bd = properties.getProperty("CSF_servidor_bd");
	String usuario = properties.getProperty("CSF_usuario");
	String clave = properties.getProperty("CSF_clave");
	String puerto = properties.getProperty("CSF_puerto");
	String TxFromCredit = properties.getProperty("CSF_TxFromCredit");
	String codigoInst = properties.getProperty("codigoInst");
	String SBP_VDN_MP_OPC_0 = properties.getProperty("CSF_VDN_MP_OPC_0");
	String SBP_VDN_MP_OPC_1 = properties.getProperty("CSF_VDN_MP_OPC_1");
	String SBP_VDN_MP_OPC_2 = properties.getProperty("CSF_VDN_MP_OPC_2");
	String SBP_VDN_MP_OPC_3 = properties.getProperty("CSF_VDN_MP_OPC_3");
	String SBP_VDN_MP_OPC_4 = properties.getProperty("CSF_VDN_MP_OPC_4");
	String SBP_VDN_MP_OPC_5 = properties.getProperty("CSF_VDN_MP_OPC_5");
	String SBP_VDN_MP_OPC_5_1 = properties.getProperty("CSF_VDN_MP_OPC_5_1");
	String SBP_VDN_MP_OPC_8 = properties.getProperty("CSF_VDN_MP_OPC_8");
	String SBP_VDN_MP_OPC_M = properties.getProperty("CSF_VDN_MP_OPC_M");
	String SBP_VDN_MP_OPC_NV = properties.getProperty("CSF_VDN_MP_OPC_NV");
	String SBP_VDN_MP_OPC_TO = properties.getProperty("CSF_VDN_MP_OPC_TO");

	String broadcast_MP = properties.getProperty("CSF_Broadcast_MP");
	String broadcast_OPC0 = properties.getProperty("CSF_Broadcast_Opc0");
	String broadcast_OPC1 = properties.getProperty("CSF_Broadcast_Opc1");
	String broadcast_OPC2 = properties.getProperty("CSF_Broadcast_Opc2");
	String broadcast_OPC3 = properties.getProperty("CSF_Broadcast_Opc3");
	String broadcast_OPC4 = properties.getProperty("CSF_Broadcast_Opc4");
	String broadcast_OPC5 = properties.getProperty("CSF_Broadcast_Opc5");
	String broadcast_OPC8 = properties.getProperty("CSF_Broadcast_Opc8");
	String broadcast_OPCM = properties.getProperty("CSF_Broadcast_OpcM");
	
	setLog(strCallUUID + " IBCC-CSF :::: Servidor WAS: " + servidorwas);
	setLog(strCallUUID + " IBCC-CSF :::: Transferencia Agente: " + TrxAgente);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal: " + TransActivaMP);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal - Opcion 0: " + TransActivaMP_Opc0);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal - Opcion 1: " + TransActivaMP_Opc1);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal - Opcion 2: " + TransActivaMP_Opc2);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal - Opcion 3: " + TransActivaMP_Opc3);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal - Opcion 4: " + TransActivaMP_Opc4);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal - Opcion 5: " + TransActivaMP_Opc5);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal - Opcion 8: " + TransActivaMP_Opc8);
	setLog(strCallUUID + " IBCC-CSF :::: Transmision Activa Menu Principal - Opcion #: " + TransActivaMP_OpcM);
	setLog(strCallUUID + " IBCC-CSF :::: Servidor Base de Datos: " + servidor_bd);
	setLog(strCallUUID + " IBCC-CSF :::: Usuario: " + usuario);
	setLog(strCallUUID + " IBCC-CSF :::: Clave: " + clave);
	setLog(strCallUUID + " IBCC-CSF :::: Puerto: " + puerto);
	setLog(strCallUUID + " IBCC-CSF :::: Transferencia de Credito Activa?: " + TxFromCredit);	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo de Institucion: " + codigoInst);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion 0: " + SBP_VDN_MP_OPC_0);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion 1: " + SBP_VDN_MP_OPC_1);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion 2: " + SBP_VDN_MP_OPC_2);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion 3: " + SBP_VDN_MP_OPC_3);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion 4: " + SBP_VDN_MP_OPC_4);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion 5: " + SBP_VDN_MP_OPC_5);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion 5_1: " + SBP_VDN_MP_OPC_5_1);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion 8: " + SBP_VDN_MP_OPC_8);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion #: " + SBP_VDN_MP_OPC_M);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion Opc. No Valido: " + SBP_VDN_MP_OPC_NV);
	setLog(strCallUUID + " IBCC-CSF :::: VDN Menu Principal Opcion TimeOut: " + SBP_VDN_MP_OPC_TO);
	
	// INICIO - Desencriptar CLAVE BD
		try{
			Encripta encripta = new Encripta();
			String claveDesencriptada = encripta.decrypt(clave);
			String usuarioDesencriptado = encripta.decrypt(usuario);
			clave = claveDesencriptada;
			usuario = usuarioDesencriptado;			
		}catch (Exception e) {
			setLog(strCallUUID + " IBCC-CSF :::: Error al desencriptar Clave BD: " + e);
		}	
	// FIN - Desencriptar CLAVE BD
	
	String SBP_Broadcast_MP = "", SBP_Broadcast_Opc0 = "", SBP_Broadcast_Opc1 = "", SBP_Broadcast_Opc2 = "", SBP_Broadcast_Opc3 = "", SBP_Broadcast_Opc4 = "", SBP_Broadcast_Opc5 = "", SBP_Broadcast_Opc8 = "", SBP_Broadcast_OpcM = "";
	String extension=".wav";
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	if(broadcast_MP.equals("")){
		SBP_Broadcast_MP = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_MP = url_audio + "/" + broadcast_MP + extension;
	}
	
	if(broadcast_OPC0.equals("")){
		SBP_Broadcast_Opc0 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc0 = url_audio + "/" + broadcast_OPC0 + extension;
	}
	
	if(broadcast_OPC1.equals("")){
		SBP_Broadcast_Opc1 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc1 = url_audio + "/" + broadcast_OPC1 + extension;
	}
	
	if(broadcast_OPC2.equals("")){
		SBP_Broadcast_Opc2 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc2 = url_audio + "/" + broadcast_OPC2 + extension;
	}
	
	if(broadcast_OPC3.equals("")){
		SBP_Broadcast_Opc3 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc3 = url_audio + "/" + broadcast_OPC3 + extension;
	}
	
	if(broadcast_OPC4.equals("")){
		SBP_Broadcast_Opc4 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc4 = url_audio + "/" + broadcast_OPC4 + extension;
	}
	
	if(broadcast_OPC5.equals("")){
		SBP_Broadcast_Opc5 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc5 = url_audio + "/" + broadcast_OPC5 + extension;
	}
	
	if(broadcast_OPC8.equals("")){
		SBP_Broadcast_Opc8 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc8 = url_audio + "/" + broadcast_OPC8 + extension;
	}
	
	if(broadcast_OPCM.equals("")){
		SBP_Broadcast_OpcM = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_OpcM = url_audio + "/" + broadcast_OPCM + extension;
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal: " + SBP_Broadcast_MP);
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal Opcion 0: " + SBP_Broadcast_Opc0);
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal Opcion 1: " + SBP_Broadcast_Opc1);
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal Opcion 2: " + SBP_Broadcast_Opc2);
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal Opcion 3: " + SBP_Broadcast_Opc3);
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal Opcion 4: " + SBP_Broadcast_Opc4);
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal Opcion 5: " + SBP_Broadcast_Opc5);
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal Opcion 8: " + SBP_Broadcast_Opc8);
	setLog(strCallUUID + " IBCC-CSF :::: Broadcast del Menu Principal Opcion M: " + SBP_Broadcast_OpcM);
	
	///-- FIN - Leer Archivo de Configuracion
	
	///-- INICIO - Locucion de Bienvenida.
	String horario = "";         
    
	Calendar calendario = Calendar.getInstance();
	int hour = calendario.get(Calendar.HOUR_OF_DAY);
	int minute = calendario.get(Calendar.MINUTE);
   	int second = calendario.get(Calendar.SECOND);
	int nro_dia = calendario.get(Calendar.DAY_OF_WEEK);
	String dia = String.valueOf(nro_dia);
	
	setLog(strCallUUID + " IBCC-CSF :::: El dia de hoy es : " + dia);
	
   	String shour = Integer.toString(hour);
   	int lhour = shour.length();
   	String sminute = Integer.toString(minute);
   	int lminute = sminute.length();
   	String ssecond = Integer.toString(second);
   	int lsecond = ssecond.length();
   	
   	if(lhour == 1){
   		shour = "0" + shour;}
   	if(lminute == 1){
   		sminute = "0" + sminute;}
   	if(lsecond == 1){
   		ssecond = "0" + ssecond;}
   	
   	String horaActual = shour + sminute + ssecond;
   	setLog(strCallUUID + " La hora actual es : " + horaActual);
   	
   	int ihoraACtual = Integer.parseInt(horaActual);
   	
   	if (ihoraACtual >= 000000 && ihoraACtual < 120000)
   	{
   		//Buenos dias
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruCSF/AudiosCSF/cre_welcome_1.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}
   	if (ihoraACtual >= 120000 && ihoraACtual < 180000)
   	{
   		//Buenos Tardes  
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruCSF/AudiosCSF/cre_welcome_2.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}
   	if (ihoraACtual >= 180000 && ihoraACtual < 235959)
   	{
   		//Buenos Noches
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruCSF/AudiosCSF/cre_welcome_3.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}     	 
	
	///-- FIN - Locucion de Bienvenida.
	
    JSONObject result = new JSONObject();    
    
    result.put("servidorwas", servidorwas);
    result.put("TrxAgente", TrxAgente);
    result.put("TransActivaMP", TransActivaMP);
    result.put("TransActivaMP_Opc0", TransActivaMP_Opc0);
    result.put("TransActivaMP_Opc1", TransActivaMP_Opc1);
    result.put("TransActivaMP_Opc2", TransActivaMP_Opc2);
    result.put("TransActivaMP_Opc3", TransActivaMP_Opc3);
    result.put("TransActivaMP_Opc4", TransActivaMP_Opc4);
    result.put("TransActivaMP_OpcM", TransActivaMP_OpcM);
    result.put("servidor_bd", servidor_bd);
    result.put("usuario", usuario);
    result.put("clave", clave);
    result.put("puerto", puerto);
    result.put("TxFromCredit", TxFromCredit);
    result.put("codigoInst", codigoInst);
    result.put("horario", horario);
    result.put("dia", dia);
    result.put("horaActual", horaActual);
    result.put("SBP_VDN_MP_OPC_0", SBP_VDN_MP_OPC_0);
    result.put("SBP_VDN_MP_OPC_1", SBP_VDN_MP_OPC_1);
    result.put("SBP_VDN_MP_OPC_2", SBP_VDN_MP_OPC_2);
    result.put("SBP_VDN_MP_OPC_3", SBP_VDN_MP_OPC_3);
    result.put("SBP_VDN_MP_OPC_4", SBP_VDN_MP_OPC_4);
    result.put("SBP_VDN_MP_OPC_5", SBP_VDN_MP_OPC_5);
    result.put("SBP_VDN_MP_OPC_5_1", SBP_VDN_MP_OPC_5_1);
    result.put("SBP_VDN_MP_OPC_8", SBP_VDN_MP_OPC_8);
    result.put("SBP_VDN_MP_OPC_M", SBP_VDN_MP_OPC_M);
    result.put("SBP_VDN_MP_OPC_NV", SBP_VDN_MP_OPC_NV);
    result.put("SBP_VDN_MP_OPC_TO", SBP_VDN_MP_OPC_TO);
    result.put("SBP_Broadcast_MP", SBP_Broadcast_MP);
    result.put("SBP_Broadcast_Opc0", SBP_Broadcast_Opc0);
    result.put("SBP_Broadcast_Opc1", SBP_Broadcast_Opc1);
    result.put("SBP_Broadcast_Opc2", SBP_Broadcast_Opc2);
    result.put("SBP_Broadcast_Opc3", SBP_Broadcast_Opc3);
    result.put("SBP_Broadcast_Opc4", SBP_Broadcast_Opc4);
    result.put("SBP_Broadcast_Opc5", SBP_Broadcast_Opc5);
    result.put("SBP_Broadcast_Opc8", SBP_Broadcast_Opc8);
    result.put("SBP_Broadcast_OpcM", SBP_Broadcast_OpcM);
    result.put("SBP_VDN_Cabinas", SBP_VDN_Cabinas);
        
    return result;    
};

public String consultarIp() throws UnknownHostException {
	InetAddress address = InetAddress.getLocalHost();
	//String sHostName = address.getHostName();
	 byte[] bIPAddress = address.getAddress();
	 String sIPAddress = "";
		 for (int x=0; x< bIPAddress.length; x++) {
			 if (x > 0) {
				 sIPAddress += ".";
			 }
		 sIPAddress += bIPAddress[x] & 255;
		 }
	return "//"+sIPAddress;
}
	
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.InvalidPropertiesFormatException"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>