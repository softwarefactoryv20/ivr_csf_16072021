<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.tramas.Bean_IB80_IB93"%>
<%@page import="middleware_support.classes.Saldo"%>

<%@page import="IvrTransaction.RequestM3.Transferencias"%>

<%!

public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB80_IB93.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vCuentaCargo= additionalParams.get("codigoProductoCargo");
	String vTipoCuentaCargo=additionalParams.get("tipoCuentaCargo");
	String vMonedaCargo=additionalParams.get("monedaCuentaCargo");
	String vCuentaAbono=additionalParams.get("codigoProducto");
	String vTipoCuentaAbono=additionalParams.get("tipoCuenta");
	String vMonedaAbono=additionalParams.get("monedaCuenta");
	String vMonto=additionalParams.get("vIngresarMonto");
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta=""; 
	String vCodRpta93=""; 
	
	String vCodOperacion="";
	
	String vTipoCambioCargo="";
	String vTipoCambioAbono="";
	String vSaldoDispCargo1="";
	String vSaldoContCargo1="";
	String vImporteConvertido="";
	String vImporteCargo="";
	String vSaldoDispCargo2="";
	String vSaldoContCargo2="";
	String vImporteAbono="";
	String vSaldoDispAbono="";
	String vSaldoContAbono="";
		
	Bean_IB80_IB93 refBean_IB80_IB93;
    
    JSONObject result = new JSONObject();
    
    vMonto=padLeftZeros(15, vMonto);
	IvrTransaction.RequestM3.Transferencias Transferencias=new IvrTransaction.RequestM3.Transferencias();
	Transferencias.TranferenciasFull(vInstitucion,vTarjeta, vCuentaCargo, vTipoCuentaCargo, vMonedaCargo, vCuentaAbono, vTipoCuentaAbono, vMonedaAbono, vMonto);
	vCodErrorRpta=Transferencias.getCodRetorno();
	
	setLog(strCallUUID + " IBCC-CSF :::: ============== BACKEND INVOCACION - IB80/IB93 : TRANSFERENCIAS ================");
    
	if (vCodErrorRpta==0) {
		
		
	 	vCodRpta = Transferencias.getTranfCtasPT().ERROR;
	 	vCodRpta93 = Transferencias.getActSaldosCtas().ERROR;
	 	
		refBean_IB80_IB93=new Bean_IB80_IB93(url_audio, Transferencias, extension);
			
		//if(vCodRpta.equals("0000") && vCodRpta93.equals("0000")){
			if(vCodRpta.equals("0000")){
			
			 vCodOperacion=refBean_IB80_IB93.getRefBean_IB80().getNumeroOperacion().getSerie();
			 vCuentaCargo=refBean_IB80_IB93.getRefBean_IB80().getCodProductCargo().getSerie();
			 vCuentaAbono=refBean_IB80_IB93.getRefBean_IB80().getCodProductAbono().getSerie();
			
			 vTipoCambioCargo=refBean_IB80_IB93.getRefBean_IB80().getTipoCambioCargo();
			 vTipoCambioAbono=refBean_IB80_IB93.getRefBean_IB80().getTipoCambioAbono();
			 vSaldoDispCargo1=refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaCargo1().getNumero();
			 vSaldoContCargo1=refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaCargo1().getNumero();
			 vImporteConvertido=refBean_IB80_IB93.getRefBean_IB80().getImporteConvertidoCargo().getNumero();
			 vImporteCargo=refBean_IB80_IB93.getRefBean_IB80().getImporteCtaCargo().getNumero();
			 vSaldoDispCargo2=refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaCargo2().getNumero();
			 vSaldoContCargo2=refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaCargo2().getNumero();
			 vImporteAbono=refBean_IB80_IB93.getRefBean_IB80().getImporteCtaAbono().getNumero();
			 vSaldoDispAbono=refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaAbono().getNumero();
			 vSaldoContAbono=refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaAbono().getNumero();
	
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB80------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Clave BT " + refBean_IB80_IB93.getRefBean_IB80().getClaveBT());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disponible Cta. Cargo 1  : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaCargo1().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Disponible Cta. Cargo 1 : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoDispCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Contable Cuenta Cargo 1 : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaCargo1().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Contable  Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoContCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Tipo de Cambio Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getIndTipoCambioCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo Cambio Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getTipoCambioCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Importe Convertido Cargo : " + vImporteConvertido);
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe Convertido Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSigImporteConvertCargo());
			setLog(strCallUUID + " IBCC-CSF :::: N° Operacion : " + vCodOperacion);
			setLog(strCallUUID + " IBCC-CSF :::: Sim Moneda Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSimMonedaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Cuenta Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getIndCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo Cuenta Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getTipoCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Cod. Producto Cargo : " + vCuentaCargo);
			setLog(strCallUUID + " IBCC-CSF :::: Importe Cuenta Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getImporteCtaCargo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe Cuenta Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSigImporteCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disponible Cuenta Cargo 2 : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaCargo2().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Disponible Cuent Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSigDispCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Contable Cuenta Cargo 2 : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaCargo2().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo contables : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoContCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Tipo de Cambio Abono : " + refBean_IB80_IB93.getRefBean_IB80().getIndTipoCambioAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Cambio Abono : " + refBean_IB80_IB93.getRefBean_IB80().getTipoCambioAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Sim Moneda Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSimMonedaAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getIndCtaAbono() );
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getTipCtaAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Cod Producto Abono : " + vCuentaAbono);
			setLog(strCallUUID + " IBCC-CSF :::: Importe Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getImporteCtaAbono().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSigImporteAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disp Cuenta : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaAbono().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoCtaAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Contable Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaAbono().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Contable Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoContAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre beneficiario : " + refBean_IB80_IB93.getRefBean_IB80().getNombreBeneficiario());
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Moneda Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getCodigoMonedaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Moneda Abono : " + refBean_IB80_IB93.getRefBean_IB80().getCodigoMonedaAbono());
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB93------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Mas datos : " + refBean_IB80_IB93.getRefBean_IB93().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Sgte. Pagina : " + refBean_IB80_IB93.getRefBean_IB93().getNroSiguentePagina());
			setLog(strCallUUID + " IBCC-CSF :::: Saldos");
			
			setLog(strCallUUID + " IBCC-CSF :::: N°  TipoCta  CodSucursal  NombreSucursal                    Mon.    Cod.Prod.     Ind.Banco SaldoContable     Signo     SaldoDisp    Signo      Tasa            Signo    Fecha         Desc.         Cod CCI            Ind-Afil");
			
			int i=1;
			
			for(Saldo refSaldo: refBean_IB80_IB93.getRefBean_IB93().getListaSaldosCTAS()){
					
				setLog(strCallUUID + " IBCC-CSF :::: " + i+"    "+
						String.format("%1$-5s",refSaldo.getTipoCta())+"      "+
						String.format("%1$-5s",refSaldo.getCodigoSucursal())+"      "+
						String.format("%1$-30s",refSaldo.getNombreSucursal())+"    "+
						String.format("%1$-5s",refSaldo.getMonedaBT())+"    "+
						String.format("%1$-11s",refSaldo.getCodigoProducto())+"    "+
						String.format("%1$-3s",refSaldo.getIndicadorBanco())+"    "+
						String.format("%1$-15s",refSaldo.getSaldoContable().getNumero())+"    "+
						String.format("%1$-3s",refSaldo.getSignoSaldoContable())+"    "+
						String.format("%1$-15s",refSaldo.getSaldoDisponible().getNumero())+"    "+
						String.format("%1$-3s",refSaldo.getSignoSalDisponible())+"    "+
						String.format("%1$-15s",refSaldo.getTasaIntereses().getNumero())+"    "+
						String.format("%1$-3s",refSaldo.getSignoTasaIntereses())+"   "+
						String.format("%1$-10s",refSaldo.getFechaApertura().getFecha())+"    "+
						String.format("%1$-10s",refSaldo.getDescripcionEstado())+"    "+
						String.format("%1$-3s",refSaldo.getCodigoCCI())+"    "+
						String.format("%1$-3s",refSaldo.getIndicadorAfiliacion())+"");
				i++;
			}	
			
			setLog(strCallUUID + " IBCC-CSF ::::  Cod operacion  : "+vCodOperacion);
			setLog(strCallUUID + " IBCC-CSF ::::  Cuenta Cargo  : "+vCuentaCargo);
			setLog(strCallUUID + " IBCC-CSF ::::  Cuenta Abono  : "+vCuentaAbono);
			
			setLog(strCallUUID + " IBCC-CSF ::::  Tipo Cambio Cargo  : "+vTipoCambioCargo);
			setLog(strCallUUID + " IBCC-CSF ::::  Tipo Cambio Abono  : "+vTipoCambioAbono);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Disp Cargo 1  : "+vSaldoDispCargo1);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Cont Cargo 1  : "+vSaldoContCargo1);
			setLog(strCallUUID + " IBCC-CSF ::::  Importe Convertido  : "+vImporteConvertido);
			setLog(strCallUUID + " IBCC-CSF ::::  Importe Cargo  : "+vImporteCargo);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Disp Cargo 2  : "+vSaldoDispCargo2);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Cont Cargo 2  : "+vSaldoContCargo2);
			setLog(strCallUUID + " IBCC-CSF ::::  Importe Abono  : "+vImporteAbono);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Disp Abono  : "+vSaldoDispAbono);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Cont Abono  : "+vSaldoContAbono);			
		}
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion IB80= " + vCodRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion IB93 = " + vCodRpta93);	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("vCodRpta93", vCodRpta93);
	result.put("vCodOperacion",vCodOperacion );
	result.put("vTipoCambioCargo",vTipoCambioCargo );
	result.put("vTipoCambioAbono",vTipoCambioAbono );
	result.put("vSaldoDispCargo1",vSaldoDispCargo1 );
	result.put("vSaldoContCargo1",vSaldoContCargo1 );
	result.put("vImporteConvertido",vImporteConvertido );
	result.put("vImporteCargo",vImporteCargo );
	result.put("vSaldoDispCargo2",vSaldoDispCargo2 );	
    result.put("vSaldoContCargo2",vSaldoContCargo2 );
    result.put("vImporteAbono",vImporteAbono );
	result.put("vSaldoDispAbono",vSaldoDispAbono );
	result.put("vSaldoContAbono",vSaldoContAbono );
    
    return result;
    
};

public String padLeftZeros(int size,String cadena){
		
		if(cadena.length()<size){
			cadena=String.format("%0"+(size-cadena.length())+"d%s",0,cadena);
		}
		return cadena;
	}
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>