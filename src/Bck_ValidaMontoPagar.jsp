<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String numvalido = "", longitud = "0";
    String inp_Bill_Amt_1 = additionalParams.get("inp_Bill_Amt_1");
    
    JSONObject result = new JSONObject();    
   
    if(inp_Bill_Amt_1.length() == 1){
    	longitud = "1";
    }
    	
    result.put("longitud", longitud);
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>