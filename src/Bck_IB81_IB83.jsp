<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.tramas.Bean_IB81_IB83_IB85"%>

<%@page import="IvrTransaction.RequestM3.ConsultaDeudasVarias"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB81_IB83.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vRuc=additionalParams.get("vRuc");
	String vTipoServicio=additionalParams.get("vTipoServicio"); 
	String vNroServicio=additionalParams.get("vNroServicio");  
	String vOrigen=additionalParams.get("vOrigen");
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta=""; 
	String vCodigoCliente="";
	String vNroDocumento="";
	String vFechaVencimiento="00000000";
	String vFlagPagoParcial="";
	String vMonedaPago="";
	String vMontoPago="";
	
	Bean_IB81_IB83_IB85 refBean_IB81_IB83_IB85;
	JSONObject result = new JSONObject();
    
	IvrTransaction.RequestM3.ConsultaDeudasVarias modelo=new IvrTransaction.RequestM3.ConsultaDeudasVarias();
	
	vCodErrorRpta=modelo.IvrConsultasVarias(vInstitucion, vTarjeta,  vRuc, vTipoServicio, vNroServicio, vOrigen);
	
	setLog(strCallUUID + " IBCC-CSF :::: ============= BACKEND INVOCACION - IB81/IB83/IB85 : CONSULTAS DEUDAS VARIAS =================");
	
	if(vCodErrorRpta==0){
		
		vCodRpta=modelo.getERROR();
		if(vCodRpta.equals("0000")){
			refBean_IB81_IB83_IB85=new Bean_IB81_IB83_IB85(url_audio, modelo, extension);
			vCodigoCliente=refBean_IB81_IB83_IB85.getCODIGOCLIENTE();
			vNroDocumento=refBean_IB81_IB83_IB85.getDOCUMENTO();
			vFechaVencimiento=refBean_IB81_IB83_IB85.getFECVEN().getFecha();	
			vFlagPagoParcial=refBean_IB81_IB83_IB85.getFLAGPAGOPARCIAL();
			vMontoPago=refBean_IB81_IB83_IB85.getIMPORTTOTPAGAR().getNumero();
			String monedaDeuda=refBean_IB81_IB83_IB85.getMONEDADEUDA();
			String moneda=refBean_IB81_IB83_IB85.getMONEDA();
			
			if(monedaDeuda.equals("0000") || monedaDeuda.equals("0001")){
				vMonedaPago = monedaDeuda;
			} else if(moneda.equals("0000") || moneda.equals("0001")){
				vMonedaPago = moneda;
			}
			
			setLog(strCallUUID + " IBCC-CSF :::: Cod cliente : " + refBean_IB81_IB83_IB85.getCODIGOCLIENTE());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Servicio : " + refBean_IB81_IB83_IB85.getTIPOSERVICIO());
			setLog(strCallUUID + " IBCC-CSF :::: Documento : " + refBean_IB81_IB83_IB85.getDOCUMENTO());
			setLog(strCallUUID + " IBCC-CSF :::: Fecha Vencimiento :  " + refBean_IB81_IB83_IB85.getFECVEN().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda : " + refBean_IB81_IB83_IB85.getMONEDA());
			setLog(strCallUUID + " IBCC-CSF :::: Importe a Pagar : " + refBean_IB81_IB83_IB85.getIMPORTTOTPAGAR().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Importe Minimo : " + refBean_IB81_IB83_IB85.getIMPORTMINIMO().getNumero());
			
			setLog(strCallUUID + " IBCC-CSF :::: Estado Cliente: " + refBean_IB81_IB83_IB85.getESTADOCLIENTE());
			setLog(strCallUUID + " IBCC-CSF :::: Flag Cronologico : " + refBean_IB81_IB83_IB85.getFLAGCRONOLOGICO());
			setLog(strCallUUID + " IBCC-CSF :::: Pago vencido" + refBean_IB81_IB83_IB85.getPAGOVENCIDO());
			setLog(strCallUUID + " IBCC-CSF :::: Flag Pago Parcial : "   + refBean_IB81_IB83_IB85.getFLAGPAGOPARCIAL());
			
			setLog(strCallUUID + " IBCC-CSF :::: Fecha emision : "   + refBean_IB81_IB83_IB85.getFECHAEMISION().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Fecha facturacion  : "   + refBean_IB81_IB83_IB85.getFECHAFACTURACION().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre del titular : "   + refBean_IB81_IB83_IB85.getNOMBRETITULAR());

			setLog(strCallUUID + " IBCC-CSF :::: Nro de Servicio : "   + refBean_IB81_IB83_IB85.getNUMEROSERVICIO());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de inscripcion : "   + refBean_IB81_IB83_IB85.getNUMEROINSCRIPCION());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe total : "   + refBean_IB81_IB83_IB85.getSIGNOIMPTOTAL());
			
			setLog(strCallUUID + " IBCC-CSF :::: Importe consumo : " + refBean_IB81_IB83_IB85.getIMPORTCONSUMO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe consumo : " + refBean_IB81_IB83_IB85.getSIGNOIMPCONSUMO());
			setLog(strCallUUID + " IBCC-CSF :::: Importe mora : "   + refBean_IB81_IB83_IB85.getIMPORTMORA().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe mora : "   + refBean_IB81_IB83_IB85.getSIGNOIMPORTEMORA());
			setLog(strCallUUID + " IBCC-CSF :::: Importe Reconexion: "   + refBean_IB81_IB83_IB85.getIMPORTERECONEXION().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe reconexion : "   + refBean_IB81_IB83_IB85.getSIGNOIMPRECONEXION());
			setLog(strCallUUID + " IBCC-CSF :::: Importe IGV : " + refBean_IB81_IB83_IB85.getIMPORTIGV().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe IGV : " + refBean_IB81_IB83_IB85.getSIGNOIMPORTIGV());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda Editada : "  + refBean_IB81_IB83_IB85.getMONEDAEDITADA());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de la deuda : " + refBean_IB81_IB83_IB85.getMONEDADEUDA());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre institucion : " + refBean_IB81_IB83_IB85.getNOMBREINSTITUCION());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Operacion : " + refBean_IB81_IB83_IB85.getNROOPERACION());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Ruc: "   + refBean_IB81_IB83_IB85.getNRORUC());
			
			
		}
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("vCodigoCliente", vCodigoCliente);
	result.put("vNroDocumento", vNroDocumento);
	result.put("vFechaVencimiento", vFechaVencimiento);
	result.put("vFlagPagoParcial", vFlagPagoParcial);
	result.put("vMonedaPago", vMonedaPago);
	result.put("vMontoPago", vMontoPago);
	
	
	
	return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>