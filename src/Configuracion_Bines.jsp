<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="java.io.File"%>
<%@page import="java.util.*"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
        
    String BIN = additionalParams.get("BIN");
    String strCallUUID = state.getString("CallUUID");
	setLog(strCallUUID + " IBCC-CSF :::: Lee XML BINES_SCF.xml");
    JSONObject result = new JSONObject();
    
    setLog(strCallUUID + " IBCC-CSF :::: BIN = " + BIN);
    //setLog(strCallUUID + " IBCC-CSF :::: GSW_BIN_TARJETA = " + GSW_BIN_TARJETA);
    
    String path = System.getProperty("user.dir");
    String archivo = "";
    String BIN_DOC = "";
    String GSW_BIN_TARJETA = "";
    String ruta = "";
    
    
    try {		    	
    	ruta = "D:\\EmergenciaIVR\\CSF\\BINES_SCF.xml";
    	setLog(strCallUUID + " IBCC-CSF :::: Ruta XML = " + ruta);
		File fXmlFile = new File(ruta);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("BINES");
        int tamLista = nList.getLength();
        for (int temp = 0; temp < tamLista; temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                BIN_DOC = eElement.getElementsByTagName("BIN").item(0).getTextContent();

                if(BIN_DOC.equals(BIN)){
                   GSW_BIN_TARJETA =  eElement.getElementsByTagName("Descripcion").item(0).getTextContent();
                   temp = tamLista;
                }
                
            }
        }
	 } 
	 catch(Exception e) {
            setLog(strCallUUID + " IBCC-CSF :::: Error Leer XML = " + e.toString());
     }
    setLog(strCallUUID + " IBCC-CSF :::: GSW_BIN_TARJETA = " + GSW_BIN_TARJETA);
    result.put("GSW_BIN_TARJETA", GSW_BIN_TARJETA);
    
    return result;
    
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.InvalidPropertiesFormatException"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>