<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.tramas.Bean_IB79"%>

<%@page import="IvrTransaction.Controller.GeneralController"%>
<%@page import="IvrTransaction.modelo.MovimCtasVarias"%>
<%@page import="IvrTransaction.modelo.DetalleMovimientoCtas"%>


<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {   
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB79.jsp");
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta=additionalParams.get("vNumTarjeta");
	String vTipoCuenta=additionalParams.get("vTipoCuenta");
	String vMoneda=additionalParams.get("vTipoMoneda");
	String vCodigoProducto=additionalParams.get("vCodProducto");
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String vListaMovimientosString="";
	String vNumMovimientos="";
	String vSaldoContable="";
	String vSignoSaldoContable="";
	String vSaldoDisponible="";
	String vSignoSaldoDisponible="";
	
	Bean_IB79 refBean_IB79;
	
	JSONObject result = new JSONObject();

	IvrTransaction.Controller.GeneralController General= new IvrTransaction.Controller.GeneralController();
	IvrTransaction.modelo.MovimCtasVarias generalModel = General.QIvrMovimCtasVarias(vInstitucion, vTarjeta, vCodigoProducto, vTipoCuenta, vMoneda);
	
	setLog(strCallUUID + " IBCC-CSF :::: ================= BACKEND INVOCACION - IB79 : CONSULTA MOVIMIENTOS ================");
	
	vCodErrorRpta=General.getCodRetorno();
	
	if(vCodErrorRpta==0){
		
		vCodRpta=generalModel.getERROR();
		
		if (vCodRpta.equals("0000")) {
			
			refBean_IB79=new Bean_IB79(generalModel);
			vNumMovimientos=refBean_IB79.getNroRegDevueltos();
			vListaMovimientosString=refBean_IB79.getListaMovimientosString();
			vSaldoContable=refBean_IB79.getSaldoContable();
			vSignoSaldoContable=refBean_IB79.getSignoSaldContable();
			vSaldoDisponible=refBean_IB79.getSaldoDisponible();
			vSignoSaldoDisponible=refBean_IB79.getSignoSaldDisponible();
			
			setLog(strCallUUID + " IBCC-CSF :::: Cuenta BT : " + refBean_IB79.getCuentaBT());
			setLog(strCallUUID + " IBCC-CSF :::: N° registros devueltos :  " + refBean_IB79.getNroRegDevueltos());
			setLog(strCallUUID + " IBCC-CSF :::: ------- Detalle Movimientos-------");
			
			setLog(strCallUUID + " IBCC-CSF :::: N°  Fecha    Descripcion                    Importe N° Operacion            N° Referencia   Documento   Ordinal/Subordinal");
				
			int i=1;
			for(DetalleMovimientoCtas detalle : refBean_IB79.getDetMovimientos()){
				
				setLog(strCallUUID + " IBCC-CSF :::: " + i+"  " + 
									detalle.getFechaMovimiento()+"  "+
									detalle.getDscrMovimiento()+"  "+
									detalle.getImporte()+"  "+
									detalle.getNroOperacion()+"  "+
									detalle.getNroReferencia()+"  "+
									detalle.getNroDocumento()+"  "+
									detalle.getOrdinalSubOrdinal());
				
				i++;	
			}
			setLog(strCallUUID + " IBCC-CSF :::: Saldo contable : " + refBean_IB79.getSaldoContable());
			setLog(strCallUUID + " IBCC-CSF :::: Signo saldo contable : " + refBean_IB79.getSignoSaldContable());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo disponible : " + refBean_IB79.getSaldoDisponible());
			setLog(strCallUUID + " IBCC-CSF :::: Signo saldo disponible : " + refBean_IB79.getSignoSaldDisponible());
			
		}else{
		    setLog(strCallUUID + " IBCC-CSF :::: Error " + generalModel.getERROR());		
		}
		
	}
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("vNumMovimientos", vNumMovimientos);
    result.put("vListaMovimientosString", vListaMovimientosString);
    result.put("vSaldoContable", vSaldoContable);
    result.put("vSignoSaldoContable", vSignoSaldoContable);
    result.put("vSaldoDisponible", vSaldoDisponible);
    result.put("vSignoSaldoDisponible", vSignoSaldoDisponible);

    return result;
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>