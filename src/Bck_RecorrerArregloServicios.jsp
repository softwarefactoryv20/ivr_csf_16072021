<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.classes.ServicioAfiliado"%>
<%@page import="middleware_support.tramas.Bean_IB76"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_RecorrerArregloServicios.jsp");
	//Input
			String vIndice = additionalParams.get("contadorServicios");
			String vListaServiciosString = additionalParams.get("listaServicios");
			
			
			//Output
			String RucEmpServicio="";
			String TipoServicio="";
			String NumServicio="";
			String Zonal="";
			String DscrServicio="";
			String DscrServUsuario="";
			String OrigenInstitucion="";
			String TipoAfiliacion="";
			String CodigoNegocio="";
			
			JSONObject result = new JSONObject();
			
			int i=Integer.parseInt(vIndice);
			List<ServicioAfiliado> listaServicios;
			setLog(strCallUUID + " IBCC-CSF :::: ===============BACKEND RECORRER ARREGLO SERVICIOS================================");
			listaServicios=Bean_IB76.getListaServicios(vListaServiciosString);
			
			ServicioAfiliado refServicioAfiliado=listaServicios.get(i);

			RucEmpServicio=refServicioAfiliado.getRucEmpServicio();
			 TipoServicio=refServicioAfiliado.getTipoServicio();
			 NumServicio=refServicioAfiliado.getNumServicio();
			 Zonal=refServicioAfiliado.getZonal();
			 DscrServicio=refServicioAfiliado.getDscrServicio();
			 DscrServUsuario=refServicioAfiliado.getDscrServUsuario();
			 OrigenInstitucion=refServicioAfiliado.getOrigenInstitucion();
			 TipoAfiliacion=refServicioAfiliado.getTipoAfiliacion();
			 CodigoNegocio=refServicioAfiliado.getCodigoNegocio();
			
			
				
			setLog(strCallUUID + " IBCC-CSF ::::  RUC : "+ RucEmpServicio);
			setLog(strCallUUID + " IBCC-CSF ::::  Tipo Servico : "+ TipoServicio);
			setLog(strCallUUID + " IBCC-CSF ::::  N° Servicio : "+ NumServicio);
			setLog(strCallUUID + " IBCC-CSF ::::  Zonal : "+ Zonal);
			setLog(strCallUUID + " IBCC-CSF ::::  Descr Servicio : "+ DscrServicio);
			setLog(strCallUUID + " IBCC-CSF ::::  Descr Servicio Usuario : "+ DscrServUsuario);
			setLog(strCallUUID + " IBCC-CSF ::::  Origen Institucion : "+ OrigenInstitucion);
			setLog(strCallUUID + " IBCC-CSF ::::  Tipo Afiliacion : "+ TipoAfiliacion);
			setLog(strCallUUID + " IBCC-CSF ::::  Codigo negocio : "+ CodigoNegocio);
			
			
			
			
			result.put("vRuc", RucEmpServicio);
			result.put("vTipoServicio",TipoServicio );
			result.put("vNroServicio",NumServicio );
			result.put("vOrigen",OrigenInstitucion );
			result.put("vTipoAfiliacion", TipoAfiliacion);
			result.put("vCodigoNegocio",CodigoNegocio );
			
			return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>