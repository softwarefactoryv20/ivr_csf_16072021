<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.classes.ServicioAfiliado"%>

<%@page import="middleware_support.tramas.Bean_IB73_IB76_IB77_IB94"%>
<%@page import="middleware_support.tramas.Bean_IB76"%>

<%@page import="IvrTransaction.Security.SecurityPassword"%>
<%@page import ="IvrTransaction.RequestM3.ProfileClient"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_Invocacion_IB76_Servicios.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta=additionalParams.get("vNumDoc");
	String vPin=additionalParams.get("varOpcIvr");
	
	String vPinEncriptado = "";
	String vLetras= "";
	String vNumeros= "";
	
	//Output
	int vCodErrorRptaInteger = 0;
	String vCodErrorRpta = "";
	String vCodRpta = "0000"; //ERROR IB73
	String vCodRptaIB76 = "";	//ERROR IB76
	String vCodRptaIB77 = "";	//ERROR IB77
	String vCodRptaIB94 = "";	//ERROR IB94
	String vErrorPin = "";
	String listaServiciosString="";
	int numServicios = 0;
	
	Bean_IB73_IB76_IB77_IB94 refBean_IB73_IB76_IB77_IB94;
	JSONObject result = new JSONObject();
	
	IvrTransaction.Security.SecurityPassword SPOId=new SecurityPassword().EncriptaClave(vPin);
	
	vPinEncriptado = SPOId.getsPasswordTwo();
	vLetras=SPOId.getsLetras();
	vNumeros=SPOId.getsNumeros();
	
	IvrTransaction.RequestM3.ProfileClient ProfileClient=new IvrTransaction.RequestM3.ProfileClient();	
	
	vCodErrorRptaInteger=ProfileClient.LoadProfileClient(vInstitucion, vTarjeta ,vPinEncriptado, vLetras, vNumeros);
	
	vCodErrorRpta = Integer.toString(vCodErrorRptaInteger);
	vCodRpta = ProfileClient.getERROR(); 		//ERROR IB73
	vCodRptaIB76 = ProfileClient.getERROR76();	//ERROR IB76
	vCodRptaIB77 = ProfileClient.getERROR77();	//ERROR IB77
	vCodRptaIB94 = ProfileClient.getERROR94();	//ERROR IB94
	vErrorPin = ProfileClient.getIVRErrorCode();
	
	setLog(strCallUUID + " IBCC-CSF :::: ================= BACKEND INVOCACION - IB73/IB76/IB77/IB94 : PERFIL DEL CLIENTE (SERVICIOS) ===========");		
	
	if (vCodErrorRptaInteger==0) {
			
		refBean_IB73_IB76_IB77_IB94=new Bean_IB73_IB76_IB77_IB94(url_audio, ProfileClient ,extension);
		listaServiciosString=refBean_IB73_IB76_IB77_IB94.getRefBean_IB76().getListaServiciosString();
		numServicios=refBean_IB73_IB76_IB77_IB94.getRefBean_IB76().getListaServiciosAfiliados().size();
		
			if(vCodRptaIB76.equals("0000")){			
					
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB76------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: N° Tarjeta : " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB76().getNumTarjeta());
			setLog(strCallUUID + " IBCC-CSF :::: Mas Datos : " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB76().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Sig. Pagina : " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB76().getSiguientePagina());
			setLog(strCallUUID + " IBCC-CSF :::: N° Registros : " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB76().getNroRegistros());
			
			setLog(strCallUUID + " IBCC-CSF :::: Servicios");
			
			
			setLog(strCallUUID + " IBCC-CSF :::: RUC         TipoServ     NumServ          Zonal     Descr Servicio      Decr. Serv. Usuario   Origen  Tipo Afil. Cod.negocio");
				
			
			for(ServicioAfiliado refServicioAfiliado: refBean_IB73_IB76_IB77_IB94.getRefBean_IB76().getListaServiciosAfiliados()){
				setLog(strCallUUID + " IBCC-CSF :::: " + 	String.format("%1$-11s",refServicioAfiliado.getRucEmpServicio())+ "   "+
						String.format("%1$-3s",refServicioAfiliado.getTipoServicio())+"      "+
						String.format("%1$-16s",refServicioAfiliado.getNumServicio())+"     "+
						String.format("%1$-3s",refServicioAfiliado.getZonal())+"      "+
						String.format("%1$-17s",refServicioAfiliado.getDscrServicio())  +"    "+
						String.format("%1$-17s",refServicioAfiliado.getDscrServUsuario())	+"     "	+ 
						String.format("%1$-5s",refServicioAfiliado.getOrigenInstitucion()) +"    "	+
						String.format("%1$-5s",refServicioAfiliado.getTipoAfiliacion()) +"     "	+
						String.format("%1$-20s",refServicioAfiliado.getCodigoNegocio()));
		 
			}
		
			setLog(strCallUUID + " IBCC-CSF :::: Numero de servicios  : "+numServicios);
			setLog(strCallUUID + " IBCC-CSF :::: Lista servicios String : "+listaServiciosString);
		
		}
			
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB76 = " + vCodRptaIB76);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB77 = " + vCodRptaIB77);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB94 = " + vCodRptaIB94);
	setLog(strCallUUID + " IBCC-CSF :::: Error Pin = " + vErrorPin);

	
	result.put("numServicios", Integer.toString(numServicios));
	result.put("listaServicios",listaServiciosString);
    
	result.put("vCodRpta", vCodRpta);
    result.put("vCodErrorRpta", vCodErrorRpta);
    result.put("vCodRptaIB76", vCodRptaIB76);
    result.put("vCodRptaIB77", vCodRptaIB77);
    result.put("vCodRptaIB94", vCodRptaIB94);
    result.put("vErrorPin", vErrorPin);  
    
        
    return result;    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>