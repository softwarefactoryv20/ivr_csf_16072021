<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!

public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("http_Request_New_Call.jsp");
	String portextension = additionalParams.get("ANI");
	String USER_AGENT = "Mozilla/5.0";
	String tempString = "", EDUID = "", nc_ANI = "", nc_DNIS = "";
	
	JSONObject result = new JSONObject();
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Primera linea de LOG http_Request_New_Call.jsp");
	try{
		String url = "http://SPCC-WTS2.per.bns:9171/bootcmp.htm?action=start_script&script=httpvox.newcall&portextension="+portextension;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Sending 'GET' request to URL = " + url);
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Sending 'GET' request to URL = " + url);
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		//print result
		tempString = response.toString();
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Response Code = " + response.toString());		
		
		int firstEDU = tempString.indexOf("<eduid>");
		int lastEDU = tempString.indexOf("</eduid>");		
		EDUID = tempString.substring(firstEDU+7, lastEDU);
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: EDUID = " + EDUID);
		
		int firstANI = tempString.indexOf("<primary_ani>");
		int lastANI = tempString.indexOf("</primary_ani>");		
		nc_ANI = tempString.substring(firstANI+13, lastANI);
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: nc_ANI = " + nc_ANI);
		
		int firstDNIS = tempString.indexOf("<primary_dnis>");
		int lastDNIS = tempString.indexOf("</primary_dnis>");		
		nc_DNIS = tempString.substring(firstDNIS+14, lastDNIS);
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: nc_DNIS = " + nc_DNIS);
		
		result.put("eduid", EDUID);
		result.put("nc_ANI", nc_ANI);
		result.put("nc_DNIS", nc_DNIS);
		
	}
	catch(Exception ex)
	{
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Response Code = " + ex.toString());
	}
	
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>