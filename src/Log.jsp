<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
 	FileWriter fichero = null;
    PrintWriter pw = null;
    
    String mensajes = additionalParams.get("Param1");
    String dia;
    String mes;
    String anio;
	String hora;
	String minuto;
	String segundo;
    String fecha="";
	String fec="";
    
    JSONObject result = new JSONObject();   
    Date d= new Date();
	    //Capturamos dia , mes y anio
    dia = "00" + Integer.toString(d.getDate()); 
    mes = "00" + Integer.toString(d.getMonth()+1);
    anio= Integer.toString(d.getYear()+ 1900);
	fec = ( dia.substring(dia.length()-2, dia.length()) ) + ( mes.substring(mes.length()-2, mes.length()) ) + anio;
    //Capturamos Hora , minuto y segundis
	hora = "00"+d.getHours();
	minuto = "00" + d.getMinutes();
	segundo = "00" + d.getSeconds();
	String time = ( hora.substring(hora.length()-2, hora.length()) ) + ":" + ( minuto.substring(minuto.length()-2, minuto.length()) ) + ":" + ( segundo.substring(segundo.length()-2, segundo.length()) );
    
    //Concatenamos
    fecha = fec + " "+ time ;

  	String path = System.getProperty("user.dir");
        try
        {
            fichero = new FileWriter(path + "\\webapps\\APP_IVR_PeruSBP\\LOG\\log_" + fec + ".txt", true);
            pw = new PrintWriter(fichero);
            
            pw.println(fecha + ", " +mensajes);
 
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    
    
    
    return result;  
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/backend.jspf" %>