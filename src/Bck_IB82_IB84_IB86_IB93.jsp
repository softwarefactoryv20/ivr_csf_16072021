<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.tramas.Bean_IB82_IB84_IB86_IB93"%>
<%@page import="middleware_support.classes.Saldo"%>
<%@page import="IvrTransaction.RequestM3.PagoRecibos"%>
<%@page import="middleware_support.support.MiddlewareDate"%>
<%@page import="middleware_support.support.MiddlewareNumber"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB82_IB84_IB86_IB93.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vOrigen=additionalParams.get("vOrigen");
	String vCodigoProducto=additionalParams.get("codigoProducto");
	String vTipoCuenta=additionalParams.get("tipoCuenta");
	String vMonedaBT= additionalParams.get("tipoMoneda");
	String vRucInstitucion= additionalParams.get("vRuc");
	String vNroServicio= additionalParams.get("nroServicio");
	String vCodigoCliente= additionalParams.get("codigoCliente");
	String vNroDocumento= additionalParams.get("nroDocumento");
	String vTrnPagum=additionalParams.get("trnPagum");
	String vIndPagoDocEnv= additionalParams.get("indPagoDocEnv");
	String vMonedaPagoDocEnv= additionalParams.get("monedaPago");
	String vImportPagoDocEnv= additionalParams.get("importePago");
	vIndPagoDocEnv = "S";
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String vCodOperacion="";
	String vSaldoDisponibleCargo="";
	
	Bean_IB82_IB84_IB86_IB93 refBean_IB82_IB84_IB86_IB93;
	JSONObject result = new JSONObject();
	
	vImportPagoDocEnv = padLeftZeros(vImportPagoDocEnv);
	
	IvrTransaction.RequestM3.PagoRecibos modelo=new IvrTransaction.RequestM3.PagoRecibos();
	modelo.PagarRecibo(vInstitucion, vTarjeta, vOrigen, vCodigoProducto , vTipoCuenta, vMonedaBT, vRucInstitucion, vNroServicio, vCodigoCliente,vNroDocumento,  vTrnPagum, vIndPagoDocEnv, vMonedaPagoDocEnv, vImportPagoDocEnv);
	vCodErrorRpta=modelo.getCodRetorno();
	
	setLog(strCallUUID + " IBCC-CSF :::: =========== BACKEND INVOCACION - IB82/IB84/IB86/IB93 : PAGO DE RECIBOS ================");
    
	if (vCodErrorRpta==0) {
		
		vCodRpta=modelo.getERROR();
		setLog(strCallUUID + " IBCC-CSF :::: vCodRpta = " + vCodRpta);
		if(vCodRpta.equals("0000")){
			//refBean_IB82_IB84_IB86_IB93=new Bean_IB82_IB84_IB86_IB93(url_audio, modelo, extension);
			//vCodOperacion=refBean_IB82_IB84_IB86_IB93.getRelacionBT();
			//vSaldoDisponibleCargo=refBean_IB82_IB84_IB86_IB93.getSaldDispCtaCargo().getNumero();
			
			vCodOperacion = modelo.getRelacionBT();
			vSaldoDisponibleCargo = modelo.getSaldDispCtaCargo();
						
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB82/IB84/IB86------------");
		
			setLog(strCallUUID + " IBCC-CSF :::: Codigo de Operacion: " + vCodOperacion);
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disponible Cuenta Cargp: " + vSaldoDisponibleCargo);
			
			/*setLog(strCallUUID + " IBCC-CSF :::: Fecha primera cuota : " + refBean_IB82_IB84_IB86_IB93.getFecPrimeraCuota().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Relacion BT : " + refBean_IB82_IB84_IB86_IB93.getRelacionBT());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo disponible de cta cargo : " + refBean_IB82_IB84_IB86_IB93.getSaldDispCtaCargo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de saldo disponible : " + refBean_IB82_IB84_IB86_IB93.getSignoSaldDisp());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo contable de cta cargo : " + refBean_IB82_IB84_IB86_IB93.getSaldContCtaCargo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo saldo contable : " + refBean_IB82_IB84_IB86_IB93.getSignoSaldCont());

			setLog(strCallUUID + " IBCC-CSF :::: Indicador de tipo de cambio : " + refBean_IB82_IB84_IB86_IB93.getIndTipoCambio());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de cambio : " + refBean_IB82_IB84_IB86_IB93.getTipoCambio());
			setLog(strCallUUID + " IBCC-CSF :::: Importe convertido : " + refBean_IB82_IB84_IB86_IB93.getImportConvert().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Sig. importe convertido : " + refBean_IB82_IB84_IB86_IB93.getSignoImportConvert());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de operacion : " + refBean_IB82_IB84_IB86_IB93.getNumOperacion());
			setLog(strCallUUID + " IBCC-CSF :::: Codigo de cliente : " + refBean_IB82_IB84_IB86_IB93.getCodigoCliente());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre de institucion : " + refBean_IB82_IB84_IB86_IB93.getNombreInstitucion());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda editada : " + refBean_IB82_IB84_IB86_IB93.getMonedaEditada());
			
			setLog(strCallUUID + " IBCC-CSF :::: Importe total: " + refBean_IB82_IB84_IB86_IB93.getImportTotal().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe total : " + refBean_IB82_IB84_IB86_IB93.getSignoImportTotal());
			setLog(strCallUUID + " IBCC-CSF :::: Numero de documento : " + refBean_IB82_IB84_IB86_IB93.getNumDocumento());
			setLog(strCallUUID + " IBCC-CSF :::: Fecha de vencimiento : " + refBean_IB82_IB84_IB86_IB93.getFecVencimiento().getFecha());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 01 : " + refBean_IB82_IB84_IB86_IB93.getConcepto01());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 01 : " + refBean_IB82_IB84_IB86_IB93.getImporte01().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 01 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport01());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 02 : " + refBean_IB82_IB84_IB86_IB93.getConcepto02());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 02 : " + refBean_IB82_IB84_IB86_IB93.getImporte02().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 02 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport02());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 03 : " + refBean_IB82_IB84_IB86_IB93.getConcepto03());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 03 : " + refBean_IB82_IB84_IB86_IB93.getImporte03().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 03 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport03());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 04 : " + refBean_IB82_IB84_IB86_IB93.getConcepto04());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 04 : " + refBean_IB82_IB84_IB86_IB93.getImporte04().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 04 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport04());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 05 : " + refBean_IB82_IB84_IB86_IB93.getConcepto05());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 05 : " + refBean_IB82_IB84_IB86_IB93.getImporte05().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 05 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport05());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 06 : " + refBean_IB82_IB84_IB86_IB93.getConcepto06());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 06 : " + refBean_IB82_IB84_IB86_IB93.getImporte06().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 06 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport06());
			
			setLog(strCallUUID + " IBCC-CSF :::: Mora : " + refBean_IB82_IB84_IB86_IB93.getMora().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de mora : " + refBean_IB82_IB84_IB86_IB93.getSignoMora());
			
			setLog(strCallUUID + " IBCC-CSF :::: Nombre de la empresa : " + refBean_IB82_IB84_IB86_IB93.getNombrEmpresa());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de RUC : " + refBean_IB82_IB84_IB86_IB93.getNroRuc());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre del abonado : " + refBean_IB82_IB84_IB86_IB93.getNombreAbonado());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de servicio : " + refBean_IB82_IB84_IB86_IB93.getNroServicio());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de inscripcion : " + refBean_IB82_IB84_IB86_IB93.getNroInscripcion());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de recibo : " + refBean_IB82_IB84_IB86_IB93.getNroRecibo());
				
			setLog(strCallUUID + " IBCC-CSF :::: Fecha de emision : " + refBean_IB82_IB84_IB86_IB93.getFecEmision().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Importe total a pagar : " + refBean_IB82_IB84_IB86_IB93.getImportTotalPagar().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe total : " + refBean_IB82_IB84_IB86_IB93.getSignoImportTotal());
			
			setLog(strCallUUID + " IBCC-CSF :::: Importe de consumo : " + refBean_IB82_IB84_IB86_IB93.getImportConsumo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe de consumo : " + refBean_IB82_IB84_IB86_IB93.getSignoImportConsumo());
			setLog(strCallUUID + " IBCC-CSF :::: Importe de mora : " + refBean_IB82_IB84_IB86_IB93.getImportMora().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe de mora : " + refBean_IB82_IB84_IB86_IB93.getSignoImpMora());
			setLog(strCallUUID + " IBCC-CSF :::: Importe de reconexion : " + refBean_IB82_IB84_IB86_IB93.getImportReconex().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe de reconexion : " + refBean_IB82_IB84_IB86_IB93.getSignoImportReconexion());
			
			setLog(strCallUUID + " IBCC-CSF :::: Importe IGV : " + refBean_IB82_IB84_IB86_IB93.getImportIGV().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe IGV : " + refBean_IB82_IB84_IB86_IB93.getSignoImpIGV());
			
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB93------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Mas datos : " + refBean_IB82_IB84_IB86_IB93.getRefBean_IB93().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Sgte. Pagina : " + refBean_IB82_IB84_IB86_IB93.getRefBean_IB93().getNroSiguentePagina());
			setLog(strCallUUID + " IBCC-CSF :::: Saldos");
			
			setLog(strCallUUID + " IBCC-CSF :::: N°  TipoCta  CodSucursal  NombreSucursal Mon.  Cod.Prod. Ind.Banco SaldoContable Signo SaldoDisp Signo Tasa       Signo Fecha        Desc.        Cod CCI Ind-Afil");
			
			int i=1;
			
			for(Saldo refSaldo: refBean_IB82_IB84_IB86_IB93.getRefBean_IB93().getListaSaldosCTAS()){
					
				setLog(strCallUUID + " IBCC-CSF :::: " + i+"    "+
						refSaldo.getTipoCta()+"         "+
						refSaldo.getCodigoSucursal()+"         "+
						refSaldo.getNombreSucursal()+"       "+
						refSaldo.getMonedaBT()+"    "+
						refSaldo.getCodigoProducto()+"    "+
						refSaldo.getIndicadorBanco()+"    "+
						refSaldo.getSaldoContable().getNumero()+"    "+
						refSaldo.getSignoSaldoContable()+"    "+
						refSaldo.getSaldoDisponible().getNumero()+"    "+
						refSaldo.getSignoSalDisponible()+"    "+
						refSaldo.getTasaIntereses().getNumero()+"    "+
						refSaldo.getSignoTasaIntereses()+"   "+
						refSaldo.getFechaApertura().getFecha()+"    "+
						refSaldo.getDescripcionEstado()+"    "+
						refSaldo.getCodigoCCI()+"    "+
						refSaldo.getIndicadorAfiliacion()+"");
				i++;
			}	*/
			
		
		}
		
		
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("codOperacion", vCodOperacion);
	result.put("saldoDispCargo", vSaldoDisponibleCargo);
	
    return result;
    
};

public String padLeftZeros(String cadena){
	
	if(cadena.length()<15){
		cadena=String.format("%0"+(15-cadena.length())+"d%s",0,cadena);
	}
	return cadena;
}

%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>