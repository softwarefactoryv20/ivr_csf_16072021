<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.tramas.Bean_IB72"%>
<%@page import="middleware_support.classes.Movimiento"%>
<%@page import="middleware_support.support.MiddlewareDate"%>
<%@page import="middleware_support.support.MiddlewareNumber"%>
<%@page import="middleware_support.support.MiddlewareValue"%>

<%@page import="IvrTransaction.Security.SecurityPassword"%>
<%@page import="IvrTransaction.Controller.CrediCardHistoryController"%>
<%@page import="IvrTransaction.modelo.CreditCardHistory"%>

<%!

// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {	
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_Invocacion_IB72.jsp");
	// TODO Auto-generated method stub
			String extension=".wav";
			String servidorwas = additionalParams.get("servidorwas");
			String url = "http://" + servidorwas + ":8080";
			String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
			
			//Input
			String vInstitucion=additionalParams.get("codigoInst");
			String vTarjeta= additionalParams.get("vNumDoc");
			//Output
			int vCodErrorRpta = 0;
			String vCodRpta="0000"; 
			String client_desc="";
			String vListaMovimientos="";
			String vNumMovimientos="";
			
			Bean_IB72 refBean_IB72;
			JSONObject result = new JSONObject();
		    
			CrediCardHistoryController History = new CrediCardHistoryController();
			CreditCardHistory CardHistory = History.QIvrCreditCardHistory(vInstitucion,  vTarjeta);
			vCodErrorRpta = History.getCodRetorno();
			
			setLog(strCallUUID + " IBCC-CSF :::: ============== BACKEND INVOCACION - IB72 : CREDIT CARD HISTORY =================== ");
			
			
		    if (vCodErrorRpta == 0) {  
			
				vCodRpta=CardHistory.getERROR();
				if (vCodRpta.equals("0000")) {
					
					client_desc=CardHistory.getNombreCliente();
					refBean_IB72=new Bean_IB72(url_audio,CardHistory,extension);
					vListaMovimientos=refBean_IB72.getListaMovimientosString();
					vNumMovimientos=Integer.toString(refBean_IB72.getListaMovimientos().size());
					
					setLog(strCallUUID + " IBCC-CSF :::: Nombre del Cliente  " + client_desc);
					setLog(strCallUUID + " IBCC-CSF :::: Tipo de Tarjeta     " + refBean_IB72.getTipoTarjeta());
					setLog(strCallUUID + " IBCC-CSF :::: Clave de Pagina  " + refBean_IB72.getClavePagina());
					setLog(strCallUUID + " IBCC-CSF :::: Moneda de Tarjeta  " + refBean_IB72.getMonedaTarjeta());
					setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo de Credito  " + refBean_IB72.getSignoSaldCredito());
					setLog(strCallUUID + " IBCC-CSF :::: Numero de movimientos  " + refBean_IB72.getNumMovimientos());
					
					setLog(strCallUUID + " IBCC-CSF :::: Movimientos ");
					setLog(strCallUUID + " IBCC-CSF :::: FECHA        DESCRIPCION        MON. IMOPORTE  SIG. PROCESO CUOTAS TRANTYPE");
					
					for(Movimiento refMovimiento: refBean_IB72.getListaMovimientos()){
						
						setLog(strCallUUID + " IBCC-CSF :::: " + 	refMovimiento.getFecOperacion().getFecha() + "   "+refMovimiento.getDescripcion()+"   "+
								refMovimiento.getSimboloMoneda()+ "   "+refMovimiento.getImporte().getNumero()    +"   "+ 
								refMovimiento.getSignoImporte() + "   "+refMovimiento.getFecProceso().getFecha() +"   "+
								refMovimiento.getNroCuotas()    + "   "+refMovimiento.getTransType());
			
					}
						
					setLog(strCallUUID + " IBCC-CSF :::: Lista Movimientos JSON String : "+ vListaMovimientos+"");
				}		
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  			
			}
		
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
			
			
			result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
			result.put("vCodRpta", vCodRpta);
			result.put("listaMovimientos", vListaMovimientos);
			result.put("numMovimientos", vNumMovimientos);
			
		    return result;
    
};


%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>