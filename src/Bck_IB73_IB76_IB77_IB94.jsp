<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.classes.Saldo"%>

<%@page import="middleware_support.tramas.Bean_IB73_IB76_IB77_IB94"%>
<%@page import="middleware_support.tramas.Bean_IB73"%>

<%@page import="IvrTransaction.Security.SecurityPassword"%>
<%@page import="IvrTransaction.RequestM3.ProfileClient"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB73_IB76_IB77_IB94.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//INPUT
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta=additionalParams.get("vNumDoc");
	String vPin=additionalParams.get("varOpcIvr");
	
	String vPinEncriptado = "";
	String vLetras= "";
	String vNumeros= "";
	
	//OUTPUT
	int vCodErrorRptaInteger = 0;
	String client_desc="";
	String city_desc="";
	String ctry_desc="";
	String listaCuentasString="";
	int numCuentas = 0;
	int numTotalCuentas = 0;
	
	String vCodErrorRpta = "";
	String vCodRpta = "0000"; //ERROR IB73
	String vCodRptaIB76;	//ERROR IB76
	String vCodRptaIB77;	//ERROR IB77
	String vCodRptaIB94 = "";	//ERROR IB94
	String vErrorPin = "";
			
	Bean_IB73_IB76_IB77_IB94 refBean_IB73_IB76_IB77_IB94;
	JSONObject result = new JSONObject();
	
	IvrTransaction.Security.SecurityPassword SPOId=new SecurityPassword().EncriptaClave(vPin);
	
	String pinEncriptado = SPOId.getsPasswordTwo();
	vLetras=SPOId.getsLetras();
	vNumeros=SPOId.getsNumeros();
	
	IvrTransaction.RequestM3.ProfileClient ProfileClient=new IvrTransaction.RequestM3.ProfileClient();	
	
	vCodErrorRptaInteger=ProfileClient.LoadProfileClient(vInstitucion, vTarjeta ,pinEncriptado, vLetras, vNumeros);
	vCodErrorRpta = Integer.toString(vCodErrorRptaInteger);
	vCodRpta = ProfileClient.getERROR(); 		//ERROR IB73
	vCodRptaIB76 = ProfileClient.getERROR();	//ERROR IB76
	vCodRptaIB77 = ProfileClient.getERROR();	//ERROR IB77
	vCodRptaIB94 = ProfileClient.getERROR();	//ERROR IB94
	vErrorPin = ProfileClient.getIVRErrorCode();
	
	
	setLog(strCallUUID + " IBCC-CSF :::: ================ BACKEND INVOCACION - IB73/IB76/IB77/IB94 : PERFIL DEL CLIENTE (CUENTAS) ==================");
		
	
	if (vCodErrorRptaInteger==0) {
			
		client_desc=ProfileClient.getValPinSCBCard().getNombreCliente();
		city_desc=ProfileClient.getValPinSCBCard().getCiudad();
		ctry_desc=ProfileClient.getValPinSCBCard().getPais();
		
		refBean_IB73_IB76_IB77_IB94=new Bean_IB73_IB76_IB77_IB94(url_audio, ProfileClient ,extension);
		
		//listaCuentasString=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldosCorrienteAhorrosString();
		//numCuentas=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldosCorrientesAhorros().size();
		listaCuentasString=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldosString();
		numCuentas=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldos().size();
		numTotalCuentas=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldos().size();
		
		if(vCodRpta.equals("0000")){
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB73------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Nombre del Cliente  " + client_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Ciudad  " + city_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Pais  " + ctry_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de clave " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getTipoClave());
			setLog(strCallUUID + " IBCC-CSF :::: Mas datos  " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Numero Siguiente Pagina " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getNroSiguientePagina());
			
			setLog(strCallUUID + " IBCC-CSF :::: DATOS DEL PRODUCTO (14)");
			
			int i=0;
			for(Saldo refSaldo: refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldos()){
				i++;
				setLog(strCallUUID + " IBCC-CSF :::: ---------------"+i+"-----------------"); 
				
				setLog(strCallUUID + " IBCC-CSF :::: Tipo de Cuenta				"+ refSaldo.getTipoCta()); 
				setLog(strCallUUID + " IBCC-CSF :::: Codigo de sucursal			"+ refSaldo.getCodigoSucursal());
				setLog(strCallUUID + " IBCC-CSF :::: Nombre de sucursal			"+ refSaldo.getNombreSucursal());
				setLog(strCallUUID + " IBCC-CSF :::: Moneda BT					"+ refSaldo.getMonedaBT());
				setLog(strCallUUID + " IBCC-CSF :::: Codigo de Producto			"+ refSaldo.getCodigoProducto());
				setLog(strCallUUID + " IBCC-CSF :::: Indicador banco				"+ refSaldo.getIndicadorBanco());
				setLog(strCallUUID + " IBCC-CSF :::: Saldo contable				"+ refSaldo.getSaldoContable().getNumero());
				setLog(strCallUUID + " IBCC-CSF :::: Sig de saldo contable		"+ refSaldo.getSignoSaldoContable());
				setLog(strCallUUID + " IBCC-CSF :::: Saldo disponible         	"+ refSaldo.getSaldoDisponible().getNumero());
				setLog(strCallUUID + " IBCC-CSF :::: Sig de saldo disponible  	"+ refSaldo.getSignoSalDisponible());
				setLog(strCallUUID + " IBCC-CSF :::: Tasa de interes 			"+ refSaldo.getTasaIntereses().getNumero());
				setLog(strCallUUID + " IBCC-CSF :::: Sig de tasa de interes  	"+ refSaldo.getSignoTasaIntereses());
				setLog(strCallUUID + " IBCC-CSF :::: Fecha apertura				"+ refSaldo.getFechaApertura().getFecha());
				setLog(strCallUUID + " IBCC-CSF :::: Descripcion  estad			"+ refSaldo.getDescripcionEstado());
				setLog(strCallUUID + " IBCC-CSF :::: Codigo CCI					"+ refSaldo.getCodigoCCI());
				setLog(strCallUUID + " IBCC-CSF :::: Indicador de afiliacion	 	"+ refSaldo.getIndicadorAfiliacion());

			}
			setLog(strCallUUID + " IBCC-CSF :::: Numero de cuentas [20 o 21] String : "+numCuentas);
			setLog(strCallUUID + " IBCC-CSF :::: Numero de cuentas String : "+numTotalCuentas);
			setLog(strCallUUID + " IBCC-CSF :::: Lista cuentas String : "+listaCuentasString);
		
		}			
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB76 = " + vCodRptaIB76);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB77 = " + vCodRptaIB77);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB94 = " + vCodRptaIB94);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Error PIN = " + vErrorPin);

	
	result.put("vCodRpta", vCodRpta);
    result.put("vCodErrorRpta", vCodErrorRpta);
    result.put("vCodRptaIB76", vCodRptaIB76);
    result.put("vCodRptaIB77", vCodRptaIB77);
    result.put("vCodRptaIB94", vCodRptaIB94);
    result.put("vErrorPin", vErrorPin);
    
    result.put("listaCuentas",listaCuentasString);
	result.put("numCuentas", Integer.toString(numCuentas));
	result.put("numTotalCuentas", numTotalCuentas);
	    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>