<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bck_ObtenerAudioTipoCuentaYMoneda.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vTipoCuenta= additionalParams.get("tipoCuenta");
	String vTipoMoneda= additionalParams.get("monedaCuenta");
	
	//Output
	String audio = url_audio+"/silencio"+extension;
	
	JSONObject result = new JSONObject();
	
	setLog(strCallUUID + " IBCC-CSF :::: =============== BACKEND OBTENER AUDIO TIPO CUENTA Y MONEDA ================================");
	
	
	if(vTipoMoneda.equals("0000") || vTipoMoneda.equals("PEN")){
		if(vTipoCuenta.equals("20")){
			audio=url_audio+"/acct_type_1"+extension;
		}else if(vTipoCuenta.equals("21")){
			audio=url_audio+"/acct_type_2"+extension;
		}else if(vTipoCuenta.equals("121")){
			audio=url_audio+"/acct_type_4"+extension;
		}
	}else if(vTipoMoneda.equals("0001") || vTipoMoneda.equals("USD")){
		if(vTipoCuenta.equals("20")){
			audio=url_audio+"/acct_type_5"+extension;
		}else if(vTipoCuenta.equals("21")){
			audio=url_audio+"/acct_type_6"+extension;
		}else if(vTipoCuenta.equals("121")){
			audio=url_audio+"/acct_type_8"+extension;
		}
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Audio Tipo Cuenta y Moneda : "+audio);
	
	
	result.put("audio", audio);
	return result;    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>