<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.classes.Saldo"%>
<%@page import="middleware_support.tramas.Bean_IB73"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_RecorrerArregloCuenta.jsp");
	//Input
	String vIndice = additionalParams.get("contadorCuentas");
	String vListaCuentasString = additionalParams.get("listaCuentas");
	
	//Output
	String monedaCuenta="";
	String saldoCuenta="";
	String signoSaldo="";
	String codigoProducto="";
	String tipoCuenta="";
			
			
    JSONObject result = new JSONObject();
    
    int i=Integer.parseInt(vIndice);
	List<Saldo> listaCuentas;
	setLog(strCallUUID + " IBCC-CSF :::: ===============BACKEND RECORRER ARREGLO CUENTAS================================");
	
	listaCuentas=Bean_IB73.getListaCuentas(vListaCuentasString);
	
	Saldo refSaldo=listaCuentas.get(i);
	monedaCuenta=refSaldo.getMonedaBT();
	saldoCuenta=refSaldo.getSaldoDisponible().getNumero();
	signoSaldo=refSaldo.getSignoSalDisponible();
	codigoProducto=refSaldo.getCodigoProducto();
	tipoCuenta=refSaldo.getTipoCta();
	
	setLog(strCallUUID + " IBCC-CSF ::::  Codigo producto : "+ codigoProducto);
	setLog(strCallUUID + " IBCC-CSF ::::  Tipo cuenta : "+ tipoCuenta);
	setLog(strCallUUID + " IBCC-CSF ::::  Moneda : "+ monedaCuenta);
	setLog(strCallUUID + " IBCC-CSF ::::  Saldo Disponible : "+ saldoCuenta);
	setLog(strCallUUID + " IBCC-CSF ::::  Signo saldo : "+ signoSaldo);
	
	
	
	result.put("monedaCuenta",monedaCuenta);		
	result.put("saldoCuenta", saldoCuenta);
	result.put("signoSaldo", signoSaldo);
	result.put("codigoProducto",codigoProducto );
	result.put("tipoCuenta",tipoCuenta );
	
	return result;
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>