<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="middleware_support.tramas.Bean_IB87_IB88_IB93"%>
<%@page import="middleware_support.classes.Saldo"%>
<%@page import="IvrTransaction.RequestM3.PagoTarjetaCredito"%>
<%!

public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB87_IB88_IB93.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vCodProducto=additionalParams.get("codigoProducto");
	String vTipoCuenta=additionalParams.get("tipoCuenta");
	String vMonedaBT=additionalParams.get("monedaCuenta");
	String vTipoPago=additionalParams.get("tipoPago");
	String vMonedaPago=additionalParams.get("monedaPago");
	String vImportePagoCta=additionalParams.get("importePago");
	String vNroTarjeta=additionalParams.get("nroServicio");
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String codOperacion="";
	String saldodisponibleCargo="";
	
	Bean_IB87_IB88_IB93 refBean_IB87_IB88_IB93;
	JSONObject result = new JSONObject();
	
	vImportePagoCta=vImportePagoCta.replace("+", "");
	
	IvrTransaction.RequestM3.PagoTarjetaCredito modelo=new IvrTransaction.RequestM3.PagoTarjetaCredito();
	modelo.IvrPagoTarjetaCredito(vInstitucion, vTarjeta, vCodProducto, vTipoCuenta, vMonedaBT, vTipoPago, vMonedaPago, vImportePagoCta, vNroTarjeta);
	vCodErrorRpta=modelo.getCodRetorno();
		
	setLog(strCallUUID + " IBCC-CSF :::: ----------------- INVOCACION - IB87/IB88/IB93 : PAGO TARJETA DE CREDITO -------------------");
	   
	if (vCodErrorRpta==0) {
		
		
		vCodRpta=modelo.getERROR();
		
		if(vCodRpta.equals("0000")){
			refBean_IB87_IB88_IB93=new Bean_IB87_IB88_IB93(url_audio, modelo, extension);
			codOperacion=refBean_IB87_IB88_IB93.getRELACIONBT();
			saldodisponibleCargo=refBean_IB87_IB88_IB93.getSALDISCTACARGO().getNumero();
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB87/IB88------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Relacion BT : " + refBean_IB87_IB88_IB93.getRELACIONBT());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disp Cta Cargo :  " + refBean_IB87_IB88_IB93.getSALDISCTACARGO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Disp Cta Cargo : " + refBean_IB87_IB88_IB93.getSIGSALDODISP());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Contable Cta Cargo : " + refBean_IB87_IB88_IB93.getSALCONTCTACARGO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Contable Cta Cargo : " + refBean_IB87_IB88_IB93.getSIGSALDOCONT());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Tipo de Cambio : " + refBean_IB87_IB88_IB93.getINDTIPCAMBIO());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Cambio : " + refBean_IB87_IB88_IB93.getTIPOCAMBIO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Importe a Convertir : " + refBean_IB87_IB88_IB93.getIMPORTECONVERT().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe a Convertir : " + refBean_IB87_IB88_IB93.getSIGIMPORTCONV());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de Pago : " + refBean_IB87_IB88_IB93.getMONEDAPAGO());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de Tarjeta : " + refBean_IB87_IB88_IB93.getMONEDATARJETA());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Tarjeta : " + refBean_IB87_IB88_IB93.getTIPOTARJETA());
			setLog(strCallUUID + " IBCC-CSF :::: Importe Pago Cuenta : " + refBean_IB87_IB88_IB93.getIMPORTEPAGCTA().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe Pago Cuenta : " + refBean_IB87_IB88_IB93.getSIGPAGOCTA());
			setLog(strCallUUID + " IBCC-CSF :::: Importe de Cargo : " + refBean_IB87_IB88_IB93.getIMPORTECARGO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de Cargo : " + refBean_IB87_IB88_IB93.getMONEDACARGO());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe de Cargo : " + refBean_IB87_IB88_IB93.getSIGIMPORTCARGADO());
			setLog(strCallUUID + " IBCC-CSF :::: Procesadora : " + refBean_IB87_IB88_IB93.getPROCESADORA());
					
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB93------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Mas datos : " + refBean_IB87_IB88_IB93.getRefBean_IB93().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Sgte. Pagina : " + refBean_IB87_IB88_IB93.getRefBean_IB93().getNroSiguentePagina());
			setLog(strCallUUID + " IBCC-CSF :::: Saldos");
			
			setLog(strCallUUID + " IBCC-CSF :::: N°  TipoCta  CodSucursal  NombreSucursal Mon.  Cod.Prod. Ind.Banco SaldoContable Signo SaldoDisp Signo Tasa       Signo Fecha        Desc.        Cod CCI Ind-Afil");
			
			int i=1;
			
			for(Saldo refSaldo: refBean_IB87_IB88_IB93.getRefBean_IB93().getListaSaldosCTAS()){
					
				setLog(strCallUUID + " IBCC-CSF :::: " + i+"    "+
						refSaldo.getTipoCta()+"         "+
						refSaldo.getCodigoSucursal()+"         "+
						refSaldo.getNombreSucursal()+"       "+
						refSaldo.getMonedaBT()+"    "+
						refSaldo.getCodigoProducto()+"    "+
						refSaldo.getIndicadorBanco()+"    "+
						refSaldo.getSaldoContable().getNumero()+"    "+
						refSaldo.getSignoSaldoContable()+"    "+
						refSaldo.getSaldoDisponible().getNumero()+"    "+
						refSaldo.getSignoSalDisponible()+"    "+
						refSaldo.getTasaIntereses().getNumero()+"    "+
						refSaldo.getSignoTasaIntereses()+"   "+
						refSaldo.getFechaApertura().getFecha()+"    "+
						refSaldo.getDescripcionEstado()+"    "+
						refSaldo.getCodigoCCI()+"    "+
						refSaldo.getIndicadorAfiliacion()+"");
				i++;
			}	
			
			
			
		}
		
		
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("codOperacion", codOperacion);
	result.put("saldoDispCargo", saldodisponibleCargo);
	
	return result;
		
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>