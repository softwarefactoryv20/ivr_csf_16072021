<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ValidarMontoIngresado.jsp");
	String vIngresarMonto = additionalParams.get("vIngresarMonto");
	String valorIngresado = "0";	
	
    JSONObject result = new JSONObject();
    setLog(strCallUUID + " IBCC-CSF :::: Ingreso la opcion o Monto: " + vIngresarMonto);
    
    setLog(strCallUUID + " IBCC-CSF :::: Longitud : " + vIngresarMonto.length());
    if(vIngresarMonto.length() != 1) {
    	valorIngresado = "1";
    }    	
    
    result.put("valorIngresado", valorIngresado);
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>