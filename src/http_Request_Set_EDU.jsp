<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	Logger log = Logger.getLogger("http_Request_Set_EDU.jsp");
	setLog(strCallUUID + " IBCC-CSF :::: Primera linea de LOG http_Request_Set_EDU.jsp");
	
	String USER_AGENT = "Mozilla/5.0";
	String v_Start_Time = additionalParams.get("v_Start_Time");
	String vedu = additionalParams.get("eduid");	
	String app_prd = additionalParams.get("app_prd");					//productType		app_prd
	String ani_num = additionalParams.get("nc_ANI");					//ANI				ani_num
	String ani_pre = additionalParams.get("nc_ANI");					//preANI			ani_pre
	String client_desc = additionalParams.get("client_desc");			//client			client_desc
	String city_desc = additionalParams.get("city_desc");				//city				city_desc	LIMA
	String ctry_desc = additionalParams.get("ctry_desc");				//country			ctry_desc	PERU
	String id_num = additionalParams.get("id_num");						//CardId			id_num
	String dnis_num = additionalParams.get("nc_DNIS");					//DNIS				dnis_num
	String ivr_num = additionalParams.get("ivr_num");					//lastIvrOption		ivr_num
	String levelIdentification = "";									//levelIdentification	
	String tvr_trx = additionalParams.get("tvr_trx");					//TransferToIvr		tvr_trx
	String cif_key = additionalParams.get("cif_key");					//cif_key			cif_key		DNI
	String aut_type = additionalParams.get("aut_type");
	
	setLog(strCallUUID + " IBCC-CSF :::: v_Start_Time = " + v_Start_Time);
	setLog(strCallUUID + " IBCC-CSF :::: vedu = " + vedu);	
	setLog(strCallUUID + " IBCC-CSF :::: app_prd = " + app_prd);					
	setLog(strCallUUID + " IBCC-CSF :::: ani_num = " + ani_num);					
	setLog(strCallUUID + " IBCC-CSF :::: ani_pre = " + ani_pre);					
	setLog(strCallUUID + " IBCC-CSF :::: client_desc = " + client_desc);			
	setLog(strCallUUID + " IBCC-CSF :::: city_desc = " + city_desc);				
	setLog(strCallUUID + " IBCC-CSF :::: ctry_desc = " + ctry_desc);
	setLog(strCallUUID + " IBCC-CSF :::: id_num = " + id_num);						
	setLog(strCallUUID + " IBCC-CSF :::: dnis_num = " + dnis_num);					
	setLog(strCallUUID + " IBCC-CSF :::: ivr_num = " + ivr_num);					
	setLog(strCallUUID + " IBCC-CSF :::: tvr_trx = " + tvr_trx);					
	setLog(strCallUUID + " IBCC-CSF :::: cif_key = " + cif_key);
	setLog(strCallUUID + " IBCC-CSF :::: aut_type = " + aut_type);
	
    JSONObject result = new JSONObject();
    
    /// INICIO - OBTENIENDO LOS SEGUNDOS DESDE QUE EL CLIENTE MARCO LA OPCION 0.
    
    try
    {
    	String f_h[] = v_Start_Time.split(",");
    	java.util.Date fechaMenor = new java.util.Date(Integer.parseInt(f_h[0]), Integer.parseInt(f_h[1]), Integer.parseInt(f_h[2]), Integer.parseInt(f_h[3]), Integer.parseInt(f_h[4]), Integer.parseInt(f_h[5]));
    	Calendar calendario = Calendar.getInstance();
    	int anio, mes, dia, hora, minuto, segundo;
    	anio = calendario.get(Calendar.YEAR);
    	mes = calendario.get(Calendar.MONTH) + 1;
    	dia = calendario.get(Calendar.DAY_OF_MONTH);
    	hora = calendario.get(Calendar.HOUR_OF_DAY);
    	minuto = calendario.get(Calendar.MINUTE);
    	segundo = calendario.get(Calendar.SECOND);
    	setLog(strCallUUID + " IBCC-CSF :::: HORA FIN = " +anio + "/" + mes + "/" + dia + " " + hora + ":" + minuto + ":" + segundo);
    	
    	java.util.Date fechaMayor = new java.util.Date(anio, mes, dia, hora, minuto, segundo);
    	long diferenciaMils = fechaMayor.getTime() - fechaMenor.getTime();
    	long solo_segundos = diferenciaMils / 1000;
    	
    	setLog(strCallUUID + " IBCC-CSF :::: Segundos = " + solo_segundos);
    	
    	/// FIN
    	
    	/// INICIO - ENVIANDO REQUEST SET_EDU
    	String EDUSTR = "http://SPCC-WTS2.per.bns:9171/bootcmp.htm?ctry_code=PERU&client_desc=" + client_desc + "&ani=" + ani_num + "&id_doc=DNI&ctry_desc=PERU&eduid=" + vedu +"&script=httpvox.iccmd&id_num=" + id_num + "&aut_type="+aut_type+"&city_desc=LIMA&ivr_num=" + ivr_num + "&cif_key=" + cif_key + "&app_prd=TB&action=start_script&command=setedu&city_code=LIMA";
    	
		EDUSTR = EDUSTR.replace(" ", "%20");
		EDUSTR = Normalizer.normalize(EDUSTR, Normalizer.Form.NFD);
		EDUSTR = EDUSTR.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		
    	URL obj = new URL(EDUSTR);
    	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    	
    	// optional default is GET
    	con.setRequestMethod("GET");

    	//add request header
    	con.setRequestProperty("User-Agent", USER_AGENT);

    	int responseCode = con.getResponseCode();
    	setLog(strCallUUID + " IBCC-CSF :::: Sending 'GET' request to URL = " + EDUSTR);
    	setLog(strCallUUID + " IBCC-CSF :::: Response Code = " + responseCode);

    	BufferedReader in = new BufferedReader(
    	        new InputStreamReader(con.getInputStream()));
    	String inputLine;
    	StringBuffer response = new StringBuffer();

    	while ((inputLine = in.readLine()) != null) {
    		response.append(inputLine);
    	}
    	in.close();

    	//print result
    	setLog(strCallUUID + " IBCC-CSF :::: Response Code = " + response.toString());	
    }
    catch(Exception ex)
	{
    	setLog(strCallUUID + " IBCC-CSF :::: Response Code = " + ex.toString());
	}    
       
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.text.Normalizer"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>