<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="IvrTransaction.modelo.DetalleMovimientoCtas"%>
<%@page import="middleware_support.tramas.Bean_IB79"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
 	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ArregloMovimientoCuentas.jsp");
	//Input
	String vIndice = additionalParams.get("contadorMovimientos");
	String vListaMovimientosString = additionalParams.get("vListaMovimientosString");
	
	//Output
	String FechaMovimiento="";
	String DscrMovimiento="";
	String Importe="";
	String NroOperacion="";
	String NroReferencia="";
	String NroDocumento="";
	String OrdinalSubOrdinal="";
	String TipoMovimiento="";
	
	JSONObject result = new JSONObject();
	
	int i=Integer.parseInt(vIndice);
	List<DetalleMovimientoCtas> listaMovimientos;
	setLog(strCallUUID + " IBCC-CSF :::: =============== BACKEND RECORRER ARREGLO MOVIMIENTOS ================================");
	listaMovimientos=Bean_IB79.getListaMovimientos(vListaMovimientosString);
	
	DetalleMovimientoCtas refDetalleMovimientoCtas=listaMovimientos.get(i);
	
	FechaMovimiento=refDetalleMovimientoCtas.getFechaMovimiento();
	DscrMovimiento=refDetalleMovimientoCtas.getDscrMovimiento();
	Importe=refDetalleMovimientoCtas.getImporte();
	NroOperacion=refDetalleMovimientoCtas.getNroOperacion();
	NroReferencia=refDetalleMovimientoCtas.getNroReferencia();
	NroDocumento=refDetalleMovimientoCtas.getNroDocumento();
	OrdinalSubOrdinal=refDetalleMovimientoCtas.getOrdinalSubOrdinal();
	TipoMovimiento=Integer.toString(Importe.indexOf("+"));		
	
	setLog(strCallUUID + " IBCC-CSF ::::  Fecha Movimiento : "+ FechaMovimiento);
	setLog(strCallUUID + " IBCC-CSF ::::  Descrip. Movimiento : "+ DscrMovimiento);
	setLog(strCallUUID + " IBCC-CSF ::::  Importe : "+ Importe);
	setLog(strCallUUID + " IBCC-CSF ::::  Nro Operacion : "+ NroOperacion);
	setLog(strCallUUID + " IBCC-CSF ::::  Nro Referencia : "+ NroReferencia);
	setLog(strCallUUID + " IBCC-CSF ::::  Nro Documento : "+ NroDocumento);
	setLog(strCallUUID + " IBCC-CSF ::::  Ordinal SubOrdinal : "+ OrdinalSubOrdinal);
	setLog(strCallUUID + " IBCC-CSF ::::  Tipo movimiento : "+ TipoMovimiento);
	
	
	result.put("FechaMovimiento", FechaMovimiento);
	result.put("DscrMovimiento",DscrMovimiento );
	result.put("Importe",Importe );
	result.put("NroOperacion",NroOperacion );
	result.put("NroReferencia",NroReferencia );
	result.put("NroDocumento", NroDocumento);
	result.put("OrdinalSubOrdinal",OrdinalSubOrdinal );
	result.put("TipoMovimiento",TipoMovimiento );
	
	return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>