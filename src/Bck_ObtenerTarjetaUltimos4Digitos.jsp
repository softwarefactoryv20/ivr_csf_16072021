<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>


<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonParser"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ObtenerTarjetaUltimos4Digitos.jsp");
	//Input
	String listaTarjetasString =  additionalParams.get("listaTarjetas");
	String vNumIngreso = additionalParams.get("ult4Digitos");
	
	//Output
	String tarjeta4UltDigitos = "0";
	
    JSONObject result = new JSONObject();
    
	 setLog(strCallUUID + " IBCC-CSF :::: ================ BACKEND OBTENER LISTA TARJETAS 4 ULTIMOS DIGITOS ================================");

	    setLog(strCallUUID + " IBCC-CSF ::::  Lista: "+listaTarjetasString);
	    
	    Gson gson=new Gson();
		JsonParser parser = new JsonParser();
		JsonArray array=parser.parse(listaTarjetasString).getAsJsonArray();

		List<String> listaTarjetas=new ArrayList();
		
		for(JsonElement json:array){
			String refTarjeta=gson.fromJson(json, String.class);
			listaTarjetas.add(refTarjeta);
		}
		
		if(vNumIngreso.length() == 4) {
			int j=0;
			setLog(strCallUUID + " IBCC-CSF :::: Cuentas cuyos 4 ultimos digitos son : "+ vNumIngreso);	
			
			for(String tarjeta: listaTarjetas){	
				tarjeta = tarjeta.trim();
				int longitud = tarjeta.length();
				if(tarjeta.substring(longitud-4,longitud).equals(vNumIngreso)){
					setLog(strCallUUID + " IBCC-CSF :::: " + j+" : " +  tarjeta);
					tarjeta4UltDigitos=tarjeta;
					j++;
				}
			}
			
			setLog(strCallUUID + " IBCC-CSF :::: Tarjeta con los 4 ultimos digitos " + tarjeta4UltDigitos);
			
		}	
	
	result.put("tarjeta4UltDigitos",tarjeta4UltDigitos);
	
    return result;
	  
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>