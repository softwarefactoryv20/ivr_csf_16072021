<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="Encripta.IvrString.Encripta"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	///-- INICIO - Leer Archivo de Configuracion
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Configuracion.jsp");
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Primera linea de LOG Configuracion_WS.jsp");
	//--- INICIO Configuracion WS.xml
	
	String path_WS = null;
	try {
		
		path_WS = consultarIp() + "/compartido/WebServices_EBD.xml";
		setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Ruta XML config WS = " + path_WS);
	} catch (UnknownHostException e1) {
		e1.printStackTrace();
	}
	java.io.File file_WS = new java.io.File(path_WS);
	Properties properties_WS = new Properties();
	java.io.InputStream inputStream_WS=null;
	
	if(file_WS.exists()){
		properties_WS.clear();
		
		
		try {
			inputStream_WS = new java.io.FileInputStream(file_WS);
		    } 
		catch (FileNotFoundException e) {
			e.printStackTrace(); }
		
		try {
			properties_WS.loadFromXML(inputStream_WS);
		    } 
		catch (InvalidPropertiesFormatException e) {
 			 e.printStackTrace(); } 
		catch (IOException e) {
			e.printStackTrace(); }		
	}
	//--- FIN Configuracion WS.xml

	String ServidorWAS = properties_WS.getProperty("SBP_servidorWAS");
	String PuertoWAS = properties_WS.getProperty("SBP_PuertoWAS");
	String ServidorBUS = properties_WS.getProperty("SBP_servidorBUS");
	String PuertoBUS = properties_WS.getProperty("SBP_PuertoBUS");
	
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: Configuracion de WS ");
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: ServidorWAS : " + ServidorWAS);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: PuertoWAS: " + PuertoWAS);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: ServidorBUS: " + ServidorBUS);
	setLog(strCallUUID + " IBCC-RETORNO-SEGUROS :::: PuertoBUS: " + PuertoBUS);
	
	///-- FIN - Leer Archivo de Configuracion 
	
	
    JSONObject result = new JSONObject();    
    
    result.put("Paramoutput1", ServidorWAS);
    result.put("Paramoutput2", PuertoWAS);
    result.put("Paramoutput3", ServidorBUS);
    result.put("Paramoutput4", PuertoBUS);    
    return result;    
};

public String consultarIp() throws UnknownHostException {
	InetAddress address = InetAddress.getLocalHost();
	//String sHostName = address.getHostName();
	 byte[] bIPAddress = address.getAddress();
	 String sIPAddress = "";
		 for (int x=0; x< bIPAddress.length; x++) {
			 if (x > 0) {
				 sIPAddress += ".";
			 }
		 sIPAddress += bIPAddress[x] & 255;
		 }
	return "//"+sIPAddress;
}
	
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.InvalidPropertiesFormatException"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>