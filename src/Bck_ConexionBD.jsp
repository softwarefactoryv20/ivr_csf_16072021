<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="Encripta.IvrString.Encripta"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	///-- INICIO - Leer Archivo de Configuracion	
		String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
		//sesion =  strCallUUID + ", ";
		Logger log = Logger.getLogger("Bck_ConexionBD.jsp");
		setLog(strCallUUID + " IBCC-CSF :::: Primera linea de LOG Configuracion.jsp");
		
		String path=null;
		try {
			path = consultarIp() + "/compartido/inicio_CSF.xml";
			setLog(strCallUUID + " IBCC-CSF :::: Ruta XML = " + path);
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		java.io.File file = new java.io.File(path);
		Properties properties = new Properties();
		java.io.InputStream inputStream=null;
		
		if(file.exists()){
			properties.clear();
			
			try {
				inputStream = new java.io.FileInputStream(file);
			    } 
			catch (FileNotFoundException e) {
				e.printStackTrace(); }
			
			try {
				properties.loadFromXML(inputStream);
			    } 
			catch (InvalidPropertiesFormatException e) {
	 			 e.printStackTrace(); } 
			catch (IOException e) {
				e.printStackTrace(); }		
		}
		String servidor_bd = properties.getProperty("CSF_servidor_bd");
		String usuario = properties.getProperty("CSF_usuario");
		String clave = properties.getProperty("CSF_clave");
		String puerto = properties.getProperty("CSF_puerto");	
		String servidorwas = properties.getProperty("CSF_servidorwas");
		
		setLog(strCallUUID + " IBCC-CSF :::: Servidor Base de Datos: " + servidor_bd);
		setLog(strCallUUID + " IBCC-CSF :::: Usuario: " + usuario);
		setLog(strCallUUID + " IBCC-CSF :::: Clave: " + clave);
		setLog(strCallUUID + " IBCC-CSF :::: Puerto: " + puerto);
		setLog(strCallUUID + " IBCC-CSF :::: Servidor WAS: " + servidorwas);
		///-- FIN - Leer Archivo de Configuracion
		
		// INICIO - Desencriptar CLAVE BD
			try{
				Encripta encripta = new Encripta();
				String claveDesencriptada = encripta.decrypt(clave);
				String usuarioDesencriptado = encripta.decrypt(usuario);
				clave = claveDesencriptada;
				usuario = usuarioDesencriptado;				
			}catch (Exception e) {
				setLog(strCallUUID + " IBCC-CSF :::: Error al desencriptar Clave BD: " + e);
			}	
		// FIN - Desencriptar CLAVE BD
		
	    JSONObject result = new JSONObject();    
	    
	    
	    result.put("servidor_bd", servidor_bd);
	    result.put("usuario", usuario);
	    result.put("clave", clave);
	    result.put("puerto", puerto);
	    result.put("servidorwas", servidorwas);
	    return result;    
    
};

public String consultarIp() throws UnknownHostException {
	InetAddress address = InetAddress.getLocalHost();
	//String sHostName = address.getHostName();
	 byte[] bIPAddress = address.getAddress();
	 String sIPAddress = "";
		 for (int x=0; x< bIPAddress.length; x++) {
			 if (x > 0) {
				 sIPAddress += ".";
			 }
		 sIPAddress += bIPAddress[x] & 255;
		 }
	return "//"+sIPAddress;
}

%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.InvalidPropertiesFormatException"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>