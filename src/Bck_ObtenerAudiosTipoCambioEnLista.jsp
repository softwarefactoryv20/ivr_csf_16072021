<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>



<%@page import="middleware_support.support.MiddlewareExchangeRate"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ObtenerAudiosTipoCambioEnLista.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vMonedaCuenta=additionalParams.get("monedaCargo");
	String vMonedaPago=additionalParams.get("monedaAbono");
	String vTipoCambioCompra=additionalParams.get("tipoCambioCompra");
	String vTipoCambioVenta=additionalParams.get("tipoCambioVenta");
	
	//Output
	String listaAudiosString="";
	String numAudios="";
	
	
	JSONObject result = new JSONObject();
  
	setLog(strCallUUID + " IBCC-CSF :::: ================ BACKEND OBTENER AUDIOS TIPO CAMBIO EN LISTA ================================");
	
	String tipoCambio="";
	
	// CUENTA EN SOLES PAGAR UN SERVICIO EN DOLARES     compra
    if(vMonedaCuenta.equals("0000")  && vMonedaPago.equals("0001")){
	
		tipoCambio=vTipoCambioCompra;
	
	// CUENTA EN DOLARES PAGAR UN SERVICIO EN  SOLES    venta
	}else if (vMonedaCuenta.equals("0001")  && vMonedaPago.equals("0000")){
	
		tipoCambio=vTipoCambioVenta;
	}
	
	MiddlewareExchangeRate refMiddlewareExchangeRate = new MiddlewareExchangeRate(url_audio, tipoCambio, extension);
	listaAudiosString=refMiddlewareExchangeRate.getListaAudiosString();
	numAudios=Integer.toString(refMiddlewareExchangeRate.getListaAudios().size());
	
	
	setLog(strCallUUID + " IBCC-CSF :::: Lista de audios String : " +listaAudiosString);
	setLog(strCallUUID + " IBCC-CSF :::: Tamaño lista de audios  : " +numAudios);
	
	
	result.put("numAudios", numAudios);
	result.put("listaAudiosString", listaAudiosString);
	return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>