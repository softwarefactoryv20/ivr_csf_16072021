<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
        
    String ServidorBus = additionalParams.get("ServidorBus");
    String PuertoBus = additionalParams.get("PuertoBus");  
    String numeroDocumento = additionalParams.get("numeroDocumento");
    String token = additionalParams.get("token");  
	
    String tempString = "";
    String exist = "";
	String typeRestriction = "";
	String descriptionRestriction = "";
	
    JSONObject result = new JSONObject();
    String strCallUUID = state.getString("CallUUID");
    try{
		String url = "https://"+ServidorBus+":"+PuertoBus+"/gscsf-customer/blackListDocument/v1";
		//String url = "https://10.237.117.95:8143/gssbp-customer/swagger-ui.html#!/customer45special45treatment45rest45controller/getCustomerSpecialTreatmentUsingPOST";
		//String url = "https://10.237.117.95:8143/gssbp-customer-fidelity/customer/indicator/cardValue/v1";
		String Body = "{\"country\": \"589\",\"documentNumber\": \""+numeroDocumento+"\",\"documentType\": \"1\"}";
		
	    setLog(strCallUUID + " IBCC-SBP :::: BCK_WS_FRAUDE_DNI" );	
	    
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to URL = " + url);
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Body = " + Body);
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Token = " + token);
		
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();						
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		//con.setRequestProperty("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJjb21wYW55Q29kZSI6IjEiLCJmaXJtYVVzdWFyaW8iOiIiLCJnbG9iYWxJZCI6IlM3ODM0OTQ5Iiwibm9tYnJlIjoiUEXDkUEgQ09CRcORQVMgWU9KQU5ZIE1BUklTT0wiLCJjb2RpZ29CVCI6Ik84NDMyNSIsInBlcmZpbCI6IkFHRU5URSIsImNvZGlnb0VtcHJlc2EiOiIiLCJjZHIiOiIwMDEwMyIsInB1ZXN0byI6IkFTRVNPUiBERSBFWFBFUklFTkNJQSBBTCBDTElFTlRFIiwiY29kaWdvRW1wbGVhZG8iOiI4NDMyNSIsImNvZGlnb1VzdWFyaW8iOiI4NDMyNSIsImNvcnJlbyI6IllPSkFOWS5QRU5BQFNDSS5DT00uUEUiLCJleHAiOjE2MTQ4MjgxOTEsImlhdCI6MTYxNDgyNDU5MSwiY29kaWdvVmVuZGVkb3IiOiIifQ.ZQqM-xa1w3vWmdMlNhoZ3bWkLS2l6RlYw1d1Ck1s9B47pzBsYO4aVnAkDMyQKmuTzffhrzIE03zNLTc5N3JBTw");
		con.setRequestProperty("Authorization", token);
		byte[] input = Body.getBytes();
    	
    	OutputStream os = con.getOutputStream();
    	os.write(input,0,input.length);
    	
    	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
    	
   		//print result
		tempString = response.toString();
		
		try{
			JSONObject jsonobj = new JSONObject(tempString);
			exist = jsonobj.getString("exist");			
			typeRestriction = jsonobj.getString("typeRestriction");
			descriptionRestriction = jsonobj.getString("descriptionRestriction");			
		}catch(JSONException Error){
			setLog(strCallUUID + " IBCC-SBP :::: Response Error Json = " + Error);
		}
    }catch(Exception Error){
		setLog(strCallUUID + " IBCC-SBP :::: Response Error = " + Error);
	}
		
		setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + tempString);	
		setLog(strCallUUID + " IBCC-SBP :::: Response exist = " + exist);	
		setLog(strCallUUID + " IBCC-SBP :::: Response typeRestriction = " + typeRestriction);
		setLog(strCallUUID + " IBCC-SBP :::: Response descriptionRestriction = " + descriptionRestriction);
	
		result.put("exist", exist);
		result.put("typeRestriction", typeRestriction);
		result.put("descriptionRestriction", descriptionRestriction);
	

    return result;

};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>

<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.cert.X509Certificate"%>
<%@page import="javax.net.ssl.HostnameVerifier"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="javax.net.ssl.SSLSession"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="javax.net.ssl.X509TrustManager"%>

<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>

