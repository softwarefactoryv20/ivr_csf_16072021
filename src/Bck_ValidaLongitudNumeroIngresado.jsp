<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ValidaLongitudNumeroIngresado.jsp");
	String vNumIngreso = "0";
	String numero = additionalParams.get("vIngresaNumCuenta");   
	setLog(strCallUUID + " IBCC-CSF :::: Sub_Page35_FundsTransfer_11.callflow");
	setLog(strCallUUID + " IBCC-CSF :::: Numero de la cuenta ingresada u opciòn marcada: " + numero);
    
    JSONObject result = new JSONObject();
    
    /*setLog(strCallUUID + " IBCC-CSF :::: Longitud : " + numero.length());
    if(numero.length() == 1){
    	vNumIngreso = "1";
    } else if(numero.length() == 7 || numero.length() == 10 || numero.length() == 14){
    	vNumIngreso = "2";
    }*/
    
    if(numero.length() == 1){
    	vNumIngreso = "1";
    } else if(numero.length() <= 14){
    	vNumIngreso = "2";
    }
    
    setLog(strCallUUID + " IBCC-CSF :::: variable vNumIngreso : " + vNumIngreso);
    
    result.put("vNumIngreso", vNumIngreso);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>