<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>

<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>


<%@page import="java.util.Iterator"%>
<%@page import="com.google.gson.Gson"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page import="IvrTransaction.modelo.RelacionTarjetas"%>
<%@page import="IvrTransaction.Controller.GeneralController"%>


<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB55.jsp");
	//Input
	String vInstitucion=additionalParams.get("codigoInst") ;  
	String vDocumento=additionalParams.get("vDoc");
	String vDniCe=additionalParams.get("vDniCe");
	
	//Output
	String numTarjetas="";
	String listaTarjetasString="";
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	
	
	JSONObject result = new JSONObject();
    
    IvrTransaction.Controller.GeneralController General= new IvrTransaction.Controller.GeneralController();
	IvrTransaction.modelo.RelacionTarjetas generalModel = General.QIvrRelacionTarjetas(vInstitucion, vDocumento, vDniCe);
	
	setLog(strCallUUID + " IBCC-CSF :::: ================= INVOCACION - IB55 : LISTADO DE TARJETAS ================= ");
	
	vCodErrorRpta=General.getCodRetorno();
	
	if (vCodErrorRpta == 0) {  
		
		vCodRpta=generalModel.getERROR();
		
		if (vCodRpta.equals("0000")) {
			
			numTarjetas = generalModel.getNumTarjetas();
			
			setLog(strCallUUID + " IBCC-CSF :::: N° Registros : " + generalModel.getNumTarjetas());
			
			setLog(strCallUUID + " IBCC-CSF :::: Lista Tarjetas");
			
			List<String> listaTarjetas=new ArrayList();
			
			for(Iterator<String> item= generalModel.getTarjetas().iterator();item.hasNext();){
				
				String tarjeta = item.next();
				listaTarjetas.add(tarjeta);
				setLog(strCallUUID + " IBCC-CSF :::: " + tarjeta);
						 
			}
			
			Gson gson = new Gson();
			listaTarjetasString=gson.toJson(listaTarjetas);
			setLog(strCallUUID + " IBCC-CSF :::: Num Tarjetas = " + numTarjetas);
			setLog(strCallUUID + " IBCC-CSF :::: Lista de Tarjetas = " + listaTarjetasString);
					
			
		}
	}
			
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	result.put("vCodRpta", vCodRpta);
    result.put("vCodErrorRpta", vCodErrorRpta);
    result.put("numTarjetas", numTarjetas);
    result.put("listaTarjetasString", listaTarjetasString);
    return result;
        
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>