<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>

<%@page import="java.io.IOException"%>
<%@page import="ConexionSBJAVA.UseClass"%>
<%@page import="IvrTransaction.Logger.LogTransSCBIvr"%>
<%@page import="IvrTransaction.modelo.TipoCambio"%>
<%@page import="IvrTransaction.Controller.TipoCambioController"%>

<%!

public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB05.jsp");
	String servidorwas = additionalParams.get("servidorwas");
	String codigoInst = additionalParams.get("codigoInst");
	String vCodRpta = "", vCodErrorRpta = "", numero = "";
	String vCompraDolarEntero = "";
	String vCompraDolarDecimal_1 = "";
	String vCompraDolarDecimal_2 = "";
	String vCompraDolarDecimal_3 = "";
	String vCompraDolarDecimal_4 = "";
	String vCDolarDecimal_1 = "silencio";
	String vCDolarDecimal_2 = "silencio";
	String vCDolarDecimal_3 = "silencio";
	String vCDolarDecimal_4 = "silencio";
	
	String vVentaDolarEntero = "";
	String vVentaDolarDecimal_1 = "";
	String vVentaDolarDecimal_2 = "";
	String vVentaDolarDecimal_3 = "";
	String vVentaDolarDecimal_4 = "";
	String vVDolarDecimal_1 = "silencio";
	String vVDolarDecimal_2 = "silencio";
	String vVDolarDecimal_3 = "silencio";
	String vVDolarDecimal_4 = "silencio";
	
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF/";
	
    JSONObject result = new JSONObject();

	setLog(strCallUUID + " IBCC-CSF :::: OPCION 4 - Tipo de Cambio");
	
	try
	{
    	IvrTransaction.Controller.TipoCambioController Cambio = new IvrTransaction.Controller.TipoCambioController();
    	setLog(strCallUUID + " IBCC-CSF :::: Linea 1... Paso OK");
		IvrTransaction.modelo.TipoCambio TipoCmb = Cambio.QIvrTipoCambio(codigoInst,"0001");
		setLog(strCallUUID + " IBCC-CSF :::: Linea 2... Paso OK");
		setLog(strCallUUID + " IBCC-CSF :::: EL codigo de retorno es = " + Cambio.getCodRetorno());
		if (Cambio.getCodRetorno() == 0) {
			setLog(strCallUUID + " IBCC-CSF :::: Linea 3... Paso OK");
			setLog(strCallUUID + " IBCC-CSF :::: El Codigo de Transaccion es = " + TipoCmb.getERROR());			
			if (TipoCmb.getERROR().equals("0000")) {
				setLog(strCallUUID + " IBCC-CSF :::: Linea 5... Paso OK");
				
				String Venta = TipoCmb.getTipoCambioVenta().toString().replace('.', '-');
				setLog(strCallUUID + " IBCC-CSF :::: Venta = " + Venta);
				
				String Compra = TipoCmb.getTipoCambioCompra().toString().replace('.', '-');
				setLog(strCallUUID + " IBCC-CSF :::: Compra = " + Compra);
				
				String[] ventas = Venta.split("-");
				String ventaEntero = ventas[0];
				setLog(strCallUUID + " IBCC-CSF :::: ventaEntero = " + ventaEntero);
				String ventaDecimal = ventas[1];
				setLog(strCallUUID + " IBCC-CSF :::: ventaDecimal = " + ventaDecimal);
				
				ventaEntero = suprimirCerosAdelante(ventaEntero);
				ventaDecimal = suprimirCerosAtras(ventaDecimal);
				
				int longVentaDecimal = ventaDecimal.length();
				if(longVentaDecimal == 1){
					vVDolarDecimal_1 = ventaDecimal.substring(0,1);
				}else if(longVentaDecimal == 2){
					vVDolarDecimal_1 = ventaDecimal.substring(0,1);
					vVDolarDecimal_2 = ventaDecimal.substring(1,2);
				}else if(longVentaDecimal == 3){
					vVDolarDecimal_1 = ventaDecimal.substring(0,1);
					vVDolarDecimal_2 = ventaDecimal.substring(1,2);
					vVDolarDecimal_3 = ventaDecimal.substring(2,3);
				}else if(longVentaDecimal == 4){
					vVDolarDecimal_1 = ventaDecimal.substring(0,1);
					vVDolarDecimal_2 = ventaDecimal.substring(1,2);
					vVDolarDecimal_3 = ventaDecimal.substring(2,3);
					vVDolarDecimal_4 = ventaDecimal.substring(3,4);
				}
				
				String[] compras = Compra.split("-");
				String compraEntero = compras[0];
				setLog(strCallUUID + " IBCC-CSF :::: compraEntero = " + compraEntero);
				String compraDecimal = compras[1];	
				setLog(strCallUUID + " IBCC-CSF :::: compraDecimal = " + compraDecimal);
				
				compraEntero = suprimirCerosAdelante(compraEntero);
				compraDecimal = suprimirCerosAtras(compraDecimal);
				
				int longCompraDecimal = compraDecimal.length();
				if(longCompraDecimal == 1){
					vCDolarDecimal_1 = compraDecimal.substring(0,1);
				}else if(longCompraDecimal == 2){
					vCDolarDecimal_1 = compraDecimal.substring(0,1);
					vCDolarDecimal_2 = compraDecimal.substring(1,2);
				}else if(longCompraDecimal == 3){
					vCDolarDecimal_1 = compraDecimal.substring(0,1);
					vCDolarDecimal_2 = compraDecimal.substring(1,2);
					vCDolarDecimal_3 = compraDecimal.substring(2,3);
				}else if(longCompraDecimal == 4){
					vCDolarDecimal_1 = compraDecimal.substring(0,1);
					vCDolarDecimal_2 = compraDecimal.substring(1,2);
					vCDolarDecimal_3 = compraDecimal.substring(2,3);
					vCDolarDecimal_4 = compraDecimal.substring(3,4);
				}
				
				vCompraDolarEntero = url_audio + "Numero/" + compraEntero + ".wav";
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar entero = " + vCompraDolarEntero);
				
				vCompraDolarDecimal_1 = url_audio + "Numero/" + vCDolarDecimal_1 + ".wav";
				vCompraDolarDecimal_2 = url_audio + "Numero/" + vCDolarDecimal_2 + ".wav";
				vCompraDolarDecimal_3 = url_audio + "Numero/" + vCDolarDecimal_3 + ".wav";
				vCompraDolarDecimal_4 = url_audio + "Numero/" + vCDolarDecimal_4 + ".wav";
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar decimal 1 = " + vCompraDolarDecimal_1);					
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar decimal 2 = " + vCompraDolarDecimal_2);
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar decimal 3 = " + vCompraDolarDecimal_3);
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar decimal 4 = " + vCompraDolarDecimal_4);
				
				vVentaDolarEntero = url_audio + "Numero/" + ventaEntero + ".wav";
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar entero = " + vVentaDolarEntero);
				
				vVentaDolarDecimal_1 = url_audio + "Numero/" + vVDolarDecimal_1 + ".wav";
				vVentaDolarDecimal_2 = url_audio + "Numero/" + vVDolarDecimal_2 + ".wav";
				vVentaDolarDecimal_3 = url_audio + "Numero/" + vVDolarDecimal_3 + ".wav";
				vVentaDolarDecimal_4 = url_audio + "Numero/" + vVDolarDecimal_4 + ".wav";					
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar decimal 1 = " + vVentaDolarDecimal_1);
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar decimal 2 = " + vVentaDolarDecimal_2);
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar decimal 3 = " + vVentaDolarDecimal_3);
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar decimal 4 = " + vVentaDolarDecimal_4);				
			}
		}		
		else
		{
			setLog(strCallUUID + " IBCC-CSF :::: No Existe Conexion con el AS400");
		}
		vCodRpta = TipoCmb.getERROR();
		vCodErrorRpta = Integer.toString(Cambio.getCodRetorno());
		setLog(strCallUUID + " IBCC-CSF :::: vCodRpta es = " + vCodRpta);
		numero = TipoCmb.getTipoCambioVenta().toString().replace('-', '.');
		
	} catch(Exception ex){}
	
    result.put("vCodRpta", vCodRpta);
    result.put("vCodErrorRpta", vCodErrorRpta);
    result.put("vCompraDolarEntero", vCompraDolarEntero);
    result.put("vCompraDolarDecimal_1", vCompraDolarDecimal_1);
    result.put("vCompraDolarDecimal_2", vCompraDolarDecimal_2);
    result.put("vCompraDolarDecimal_3", vCompraDolarDecimal_3);
    result.put("vCompraDolarDecimal_4", vCompraDolarDecimal_4);    
    result.put("vVentaDolarEntero", vVentaDolarEntero);
    result.put("vVentaDolarDecimal_1", vVentaDolarDecimal_1);
    result.put("vVentaDolarDecimal_2", vVentaDolarDecimal_2);
    result.put("vVentaDolarDecimal_3", vVentaDolarDecimal_3);
    result.put("vVentaDolarDecimal_4", vVentaDolarDecimal_4);
    result.put("numero", numero);
    return result;
    
};

public String suprimirCerosAdelante(String entero){
	
	int i=0;
	int p=0;
	int numDigitos=entero.length()-1;
	while(p==0 && i<numDigitos){
		if(entero.charAt(0)=='0'){
			entero=entero.substring(1,entero.length());
		}else{
			p=1;
		}
		i++;
	}	
	return entero;
}

public String suprimirCerosAtras(String decimales){
	
	int p=0;
	int numDigitos=decimales.length()-1;
	while(p==0 && numDigitos>1){
		
		if(decimales.charAt(decimales.length()-1)=='0'){
			
			decimales=decimales.substring(0,decimales.length()-1);
		}else{
			p=1;
		}
	}	
	return decimales;
}

%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>