package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import middleware_support.classes.Saldo;
import middleware_support.tramas.Bean_IB73_IB76_IB77_IB94;
import middleware_support.tramas.Bean_IB73;
import IvrTransaction.Security.SecurityPassword;
import IvrTransaction.RequestM3.ProfileClient;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fIB73_005fIB76_005fIB77_005fIB94_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB73_IB76_IB77_IB94.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//INPUT
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta=additionalParams.get("vNumDoc");
	String vPin=additionalParams.get("varOpcIvr");
	
	String vPinEncriptado = "";
	String vLetras= "";
	String vNumeros= "";
	
	//OUTPUT
	int vCodErrorRptaInteger = 0;
	String client_desc="";
	String city_desc="";
	String ctry_desc="";
	String listaCuentasString="";
	int numCuentas = 0;
	int numTotalCuentas = 0;
	
	String vCodErrorRpta = "";
	String vCodRpta = "0000"; //ERROR IB73
	String vCodRptaIB76;	//ERROR IB76
	String vCodRptaIB77;	//ERROR IB77
	String vCodRptaIB94 = "";	//ERROR IB94
	String vErrorPin = "";
			
	Bean_IB73_IB76_IB77_IB94 refBean_IB73_IB76_IB77_IB94;
	JSONObject result = new JSONObject();
	
	IvrTransaction.Security.SecurityPassword SPOId=new SecurityPassword().EncriptaClave(vPin);
	
	String pinEncriptado = SPOId.getsPasswordTwo();
	vLetras=SPOId.getsLetras();
	vNumeros=SPOId.getsNumeros();
	
	IvrTransaction.RequestM3.ProfileClient ProfileClient=new IvrTransaction.RequestM3.ProfileClient();	
	
	vCodErrorRptaInteger=ProfileClient.LoadProfileClient(vInstitucion, vTarjeta ,pinEncriptado, vLetras, vNumeros);
	vCodErrorRpta = Integer.toString(vCodErrorRptaInteger);
	vCodRpta = ProfileClient.getERROR(); 		//ERROR IB73
	vCodRptaIB76 = ProfileClient.getERROR();	//ERROR IB76
	vCodRptaIB77 = ProfileClient.getERROR();	//ERROR IB77
	vCodRptaIB94 = ProfileClient.getERROR();	//ERROR IB94
	vErrorPin = ProfileClient.getIVRErrorCode();
	
	
	setLog(strCallUUID + " IBCC-CSF :::: ================ BACKEND INVOCACION - IB73/IB76/IB77/IB94 : PERFIL DEL CLIENTE (CUENTAS) ==================");
		
	
	if (vCodErrorRptaInteger==0) {
			
		client_desc=ProfileClient.getValPinSCBCard().getNombreCliente();
		city_desc=ProfileClient.getValPinSCBCard().getCiudad();
		ctry_desc=ProfileClient.getValPinSCBCard().getPais();
		
		refBean_IB73_IB76_IB77_IB94=new Bean_IB73_IB76_IB77_IB94(url_audio, ProfileClient ,extension);
		
		//listaCuentasString=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldosCorrienteAhorrosString();
		//numCuentas=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldosCorrientesAhorros().size();
		listaCuentasString=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldosString();
		numCuentas=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldos().size();
		numTotalCuentas=refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldos().size();
		
		if(vCodRpta.equals("0000")){
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB73------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Nombre del Cliente  " + client_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Ciudad  " + city_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Pais  " + ctry_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de clave " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getTipoClave());
			setLog(strCallUUID + " IBCC-CSF :::: Mas datos  " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Numero Siguiente Pagina " + refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getNroSiguientePagina());
			
			setLog(strCallUUID + " IBCC-CSF :::: DATOS DEL PRODUCTO (14)");
			
			int i=0;
			for(Saldo refSaldo: refBean_IB73_IB76_IB77_IB94.getRefBean_IB73().getListaSaldos()){
				i++;
				setLog(strCallUUID + " IBCC-CSF :::: ---------------"+i+"-----------------"); 
				
				setLog(strCallUUID + " IBCC-CSF :::: Tipo de Cuenta				"+ refSaldo.getTipoCta()); 
				setLog(strCallUUID + " IBCC-CSF :::: Codigo de sucursal			"+ refSaldo.getCodigoSucursal());
				setLog(strCallUUID + " IBCC-CSF :::: Nombre de sucursal			"+ refSaldo.getNombreSucursal());
				setLog(strCallUUID + " IBCC-CSF :::: Moneda BT					"+ refSaldo.getMonedaBT());
				setLog(strCallUUID + " IBCC-CSF :::: Codigo de Producto			"+ refSaldo.getCodigoProducto());
				setLog(strCallUUID + " IBCC-CSF :::: Indicador banco				"+ refSaldo.getIndicadorBanco());
				setLog(strCallUUID + " IBCC-CSF :::: Saldo contable				"+ refSaldo.getSaldoContable().getNumero());
				setLog(strCallUUID + " IBCC-CSF :::: Sig de saldo contable		"+ refSaldo.getSignoSaldoContable());
				setLog(strCallUUID + " IBCC-CSF :::: Saldo disponible         	"+ refSaldo.getSaldoDisponible().getNumero());
				setLog(strCallUUID + " IBCC-CSF :::: Sig de saldo disponible  	"+ refSaldo.getSignoSalDisponible());
				setLog(strCallUUID + " IBCC-CSF :::: Tasa de interes 			"+ refSaldo.getTasaIntereses().getNumero());
				setLog(strCallUUID + " IBCC-CSF :::: Sig de tasa de interes  	"+ refSaldo.getSignoTasaIntereses());
				setLog(strCallUUID + " IBCC-CSF :::: Fecha apertura				"+ refSaldo.getFechaApertura().getFecha());
				setLog(strCallUUID + " IBCC-CSF :::: Descripcion  estad			"+ refSaldo.getDescripcionEstado());
				setLog(strCallUUID + " IBCC-CSF :::: Codigo CCI					"+ refSaldo.getCodigoCCI());
				setLog(strCallUUID + " IBCC-CSF :::: Indicador de afiliacion	 	"+ refSaldo.getIndicadorAfiliacion());

			}
			setLog(strCallUUID + " IBCC-CSF :::: Numero de cuentas [20 o 21] String : "+numCuentas);
			setLog(strCallUUID + " IBCC-CSF :::: Numero de cuentas String : "+numTotalCuentas);
			setLog(strCallUUID + " IBCC-CSF :::: Lista cuentas String : "+listaCuentasString);
		
		}			
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB76 = " + vCodRptaIB76);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB77 = " + vCodRptaIB77);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK IB94 = " + vCodRptaIB94);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Error PIN = " + vErrorPin);

	
	result.put("vCodRpta", vCodRpta);
    result.put("vCodErrorRpta", vCodErrorRpta);
    result.put("vCodRptaIB76", vCodRptaIB76);
    result.put("vCodRptaIB77", vCodRptaIB77);
    result.put("vCodRptaIB94", vCodRptaIB94);
    result.put("vErrorPin", vErrorPin);
    
    result.put("listaCuentas",listaCuentasString);
	result.put("numCuentas", Integer.toString(numCuentas));
	result.put("numTotalCuentas", numTotalCuentas);
	    
    return result;
    
};


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
