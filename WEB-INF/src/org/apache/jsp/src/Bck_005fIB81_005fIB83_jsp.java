package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import middleware_support.tramas.Bean_IB81_IB83_IB85;
import IvrTransaction.RequestM3.ConsultaDeudasVarias;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fIB81_005fIB83_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB81_IB83.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vRuc=additionalParams.get("vRuc");
	String vTipoServicio=additionalParams.get("vTipoServicio"); 
	String vNroServicio=additionalParams.get("vNroServicio");  
	String vOrigen=additionalParams.get("vOrigen");
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta=""; 
	String vCodigoCliente="";
	String vNroDocumento="";
	String vFechaVencimiento="00000000";
	String vFlagPagoParcial="";
	String vMonedaPago="";
	String vMontoPago="";
	
	Bean_IB81_IB83_IB85 refBean_IB81_IB83_IB85;
	JSONObject result = new JSONObject();
    
	IvrTransaction.RequestM3.ConsultaDeudasVarias modelo=new IvrTransaction.RequestM3.ConsultaDeudasVarias();
	
	vCodErrorRpta=modelo.IvrConsultasVarias(vInstitucion, vTarjeta,  vRuc, vTipoServicio, vNroServicio, vOrigen);
	
	setLog(strCallUUID + " IBCC-CSF :::: ============= BACKEND INVOCACION - IB81/IB83/IB85 : CONSULTAS DEUDAS VARIAS =================");
	
	if(vCodErrorRpta==0){
		
		vCodRpta=modelo.getERROR();
		if(vCodRpta.equals("0000")){
			refBean_IB81_IB83_IB85=new Bean_IB81_IB83_IB85(url_audio, modelo, extension);
			vCodigoCliente=refBean_IB81_IB83_IB85.getCODIGOCLIENTE();
			vNroDocumento=refBean_IB81_IB83_IB85.getDOCUMENTO();
			vFechaVencimiento=refBean_IB81_IB83_IB85.getFECVEN().getFecha();	
			vFlagPagoParcial=refBean_IB81_IB83_IB85.getFLAGPAGOPARCIAL();
			vMontoPago=refBean_IB81_IB83_IB85.getIMPORTTOTPAGAR().getNumero();
			String monedaDeuda=refBean_IB81_IB83_IB85.getMONEDADEUDA();
			String moneda=refBean_IB81_IB83_IB85.getMONEDA();
			
			if(monedaDeuda.equals("0000") || monedaDeuda.equals("0001")){
				vMonedaPago = monedaDeuda;
			} else if(moneda.equals("0000") || moneda.equals("0001")){
				vMonedaPago = moneda;
			}
			
			setLog(strCallUUID + " IBCC-CSF :::: Cod cliente : " + refBean_IB81_IB83_IB85.getCODIGOCLIENTE());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Servicio : " + refBean_IB81_IB83_IB85.getTIPOSERVICIO());
			setLog(strCallUUID + " IBCC-CSF :::: Documento : " + refBean_IB81_IB83_IB85.getDOCUMENTO());
			setLog(strCallUUID + " IBCC-CSF :::: Fecha Vencimiento :  " + refBean_IB81_IB83_IB85.getFECVEN().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda : " + refBean_IB81_IB83_IB85.getMONEDA());
			setLog(strCallUUID + " IBCC-CSF :::: Importe a Pagar : " + refBean_IB81_IB83_IB85.getIMPORTTOTPAGAR().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Importe Minimo : " + refBean_IB81_IB83_IB85.getIMPORTMINIMO().getNumero());
			
			setLog(strCallUUID + " IBCC-CSF :::: Estado Cliente: " + refBean_IB81_IB83_IB85.getESTADOCLIENTE());
			setLog(strCallUUID + " IBCC-CSF :::: Flag Cronologico : " + refBean_IB81_IB83_IB85.getFLAGCRONOLOGICO());
			setLog(strCallUUID + " IBCC-CSF :::: Pago vencido" + refBean_IB81_IB83_IB85.getPAGOVENCIDO());
			setLog(strCallUUID + " IBCC-CSF :::: Flag Pago Parcial : "   + refBean_IB81_IB83_IB85.getFLAGPAGOPARCIAL());
			
			setLog(strCallUUID + " IBCC-CSF :::: Fecha emision : "   + refBean_IB81_IB83_IB85.getFECHAEMISION().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Fecha facturacion  : "   + refBean_IB81_IB83_IB85.getFECHAFACTURACION().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre del titular : "   + refBean_IB81_IB83_IB85.getNOMBRETITULAR());

			setLog(strCallUUID + " IBCC-CSF :::: Nro de Servicio : "   + refBean_IB81_IB83_IB85.getNUMEROSERVICIO());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de inscripcion : "   + refBean_IB81_IB83_IB85.getNUMEROINSCRIPCION());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe total : "   + refBean_IB81_IB83_IB85.getSIGNOIMPTOTAL());
			
			setLog(strCallUUID + " IBCC-CSF :::: Importe consumo : " + refBean_IB81_IB83_IB85.getIMPORTCONSUMO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe consumo : " + refBean_IB81_IB83_IB85.getSIGNOIMPCONSUMO());
			setLog(strCallUUID + " IBCC-CSF :::: Importe mora : "   + refBean_IB81_IB83_IB85.getIMPORTMORA().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe mora : "   + refBean_IB81_IB83_IB85.getSIGNOIMPORTEMORA());
			setLog(strCallUUID + " IBCC-CSF :::: Importe Reconexion: "   + refBean_IB81_IB83_IB85.getIMPORTERECONEXION().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe reconexion : "   + refBean_IB81_IB83_IB85.getSIGNOIMPRECONEXION());
			setLog(strCallUUID + " IBCC-CSF :::: Importe IGV : " + refBean_IB81_IB83_IB85.getIMPORTIGV().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe IGV : " + refBean_IB81_IB83_IB85.getSIGNOIMPORTIGV());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda Editada : "  + refBean_IB81_IB83_IB85.getMONEDAEDITADA());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de la deuda : " + refBean_IB81_IB83_IB85.getMONEDADEUDA());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre institucion : " + refBean_IB81_IB83_IB85.getNOMBREINSTITUCION());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Operacion : " + refBean_IB81_IB83_IB85.getNROOPERACION());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Ruc: "   + refBean_IB81_IB83_IB85.getNRORUC());
			
			
		}
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("vCodigoCliente", vCodigoCliente);
	result.put("vNroDocumento", vNroDocumento);
	result.put("vFechaVencimiento", vFechaVencimiento);
	result.put("vFlagPagoParcial", vFlagPagoParcial);
	result.put("vMonedaPago", vMonedaPago);
	result.put("vMontoPago", vMontoPago);
	
	
	
	return result;
    
};


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
