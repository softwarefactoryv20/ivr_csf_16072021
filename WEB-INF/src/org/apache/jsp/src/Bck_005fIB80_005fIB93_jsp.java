package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import middleware_support.tramas.Bean_IB80_IB93;
import middleware_support.classes.Saldo;
import IvrTransaction.RequestM3.Transferencias;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fIB80_005fIB93_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {



public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB80_IB93.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vCuentaCargo= additionalParams.get("codigoProductoCargo");
	String vTipoCuentaCargo=additionalParams.get("tipoCuentaCargo");
	String vMonedaCargo=additionalParams.get("monedaCuentaCargo");
	String vCuentaAbono=additionalParams.get("codigoProducto");
	String vTipoCuentaAbono=additionalParams.get("tipoCuenta");
	String vMonedaAbono=additionalParams.get("monedaCuenta");
	String vMonto=additionalParams.get("vIngresarMonto");
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta=""; 
	String vCodRpta93=""; 
	
	String vCodOperacion="";
	
	String vTipoCambioCargo="";
	String vTipoCambioAbono="";
	String vSaldoDispCargo1="";
	String vSaldoContCargo1="";
	String vImporteConvertido="";
	String vImporteCargo="";
	String vSaldoDispCargo2="";
	String vSaldoContCargo2="";
	String vImporteAbono="";
	String vSaldoDispAbono="";
	String vSaldoContAbono="";
		
	Bean_IB80_IB93 refBean_IB80_IB93;
    
    JSONObject result = new JSONObject();
    
    vMonto=padLeftZeros(15, vMonto);
	IvrTransaction.RequestM3.Transferencias Transferencias=new IvrTransaction.RequestM3.Transferencias();
	Transferencias.TranferenciasFull(vInstitucion,vTarjeta, vCuentaCargo, vTipoCuentaCargo, vMonedaCargo, vCuentaAbono, vTipoCuentaAbono, vMonedaAbono, vMonto);
	vCodErrorRpta=Transferencias.getCodRetorno();
	
	setLog(strCallUUID + " IBCC-CSF :::: ============== BACKEND INVOCACION - IB80/IB93 : TRANSFERENCIAS ================");
    
	if (vCodErrorRpta==0) {
		
		
	 	vCodRpta = Transferencias.getTranfCtasPT().ERROR;
	 	vCodRpta93 = Transferencias.getActSaldosCtas().ERROR;
	 	
		refBean_IB80_IB93=new Bean_IB80_IB93(url_audio, Transferencias, extension);
			
		//if(vCodRpta.equals("0000") && vCodRpta93.equals("0000")){
			if(vCodRpta.equals("0000")){
			
			 vCodOperacion=refBean_IB80_IB93.getRefBean_IB80().getNumeroOperacion().getSerie();
			 vCuentaCargo=refBean_IB80_IB93.getRefBean_IB80().getCodProductCargo().getSerie();
			 vCuentaAbono=refBean_IB80_IB93.getRefBean_IB80().getCodProductAbono().getSerie();
			
			 vTipoCambioCargo=refBean_IB80_IB93.getRefBean_IB80().getTipoCambioCargo();
			 vTipoCambioAbono=refBean_IB80_IB93.getRefBean_IB80().getTipoCambioAbono();
			 vSaldoDispCargo1=refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaCargo1().getNumero();
			 vSaldoContCargo1=refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaCargo1().getNumero();
			 vImporteConvertido=refBean_IB80_IB93.getRefBean_IB80().getImporteConvertidoCargo().getNumero();
			 vImporteCargo=refBean_IB80_IB93.getRefBean_IB80().getImporteCtaCargo().getNumero();
			 vSaldoDispCargo2=refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaCargo2().getNumero();
			 vSaldoContCargo2=refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaCargo2().getNumero();
			 vImporteAbono=refBean_IB80_IB93.getRefBean_IB80().getImporteCtaAbono().getNumero();
			 vSaldoDispAbono=refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaAbono().getNumero();
			 vSaldoContAbono=refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaAbono().getNumero();
	
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB80------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Clave BT " + refBean_IB80_IB93.getRefBean_IB80().getClaveBT());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disponible Cta. Cargo 1  : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaCargo1().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Disponible Cta. Cargo 1 : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoDispCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Contable Cuenta Cargo 1 : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaCargo1().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Contable  Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoContCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Tipo de Cambio Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getIndTipoCambioCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo Cambio Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getTipoCambioCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Importe Convertido Cargo : " + vImporteConvertido);
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe Convertido Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSigImporteConvertCargo());
			setLog(strCallUUID + " IBCC-CSF :::: N° Operacion : " + vCodOperacion);
			setLog(strCallUUID + " IBCC-CSF :::: Sim Moneda Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSimMonedaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Cuenta Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getIndCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo Cuenta Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getTipoCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Cod. Producto Cargo : " + vCuentaCargo);
			setLog(strCallUUID + " IBCC-CSF :::: Importe Cuenta Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getImporteCtaCargo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe Cuenta Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSigImporteCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disponible Cuenta Cargo 2 : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaCargo2().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Disponible Cuent Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getSigDispCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Contable Cuenta Cargo 2 : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaCargo2().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo contables : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoContCtaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Tipo de Cambio Abono : " + refBean_IB80_IB93.getRefBean_IB80().getIndTipoCambioAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Cambio Abono : " + refBean_IB80_IB93.getRefBean_IB80().getTipoCambioAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Sim Moneda Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSimMonedaAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getIndCtaAbono() );
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getTipCtaAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Cod Producto Abono : " + vCuentaAbono);
			setLog(strCallUUID + " IBCC-CSF :::: Importe Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getImporteCtaAbono().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSigImporteAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disp Cuenta : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoDispCtaAbono().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoCtaAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Contable Cuenta Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSaldoContCtaAbono().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Contable Abono : " + refBean_IB80_IB93.getRefBean_IB80().getSigSaldoContAbono());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre beneficiario : " + refBean_IB80_IB93.getRefBean_IB80().getNombreBeneficiario());
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Moneda Cargo : " + refBean_IB80_IB93.getRefBean_IB80().getCodigoMonedaCargo());
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Moneda Abono : " + refBean_IB80_IB93.getRefBean_IB80().getCodigoMonedaAbono());
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB93------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Mas datos : " + refBean_IB80_IB93.getRefBean_IB93().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Sgte. Pagina : " + refBean_IB80_IB93.getRefBean_IB93().getNroSiguentePagina());
			setLog(strCallUUID + " IBCC-CSF :::: Saldos");
			
			setLog(strCallUUID + " IBCC-CSF :::: N°  TipoCta  CodSucursal  NombreSucursal                    Mon.    Cod.Prod.     Ind.Banco SaldoContable     Signo     SaldoDisp    Signo      Tasa            Signo    Fecha         Desc.         Cod CCI            Ind-Afil");
			
			int i=1;
			
			for(Saldo refSaldo: refBean_IB80_IB93.getRefBean_IB93().getListaSaldosCTAS()){
					
				setLog(strCallUUID + " IBCC-CSF :::: " + i+"    "+
						String.format("%1$-5s",refSaldo.getTipoCta())+"      "+
						String.format("%1$-5s",refSaldo.getCodigoSucursal())+"      "+
						String.format("%1$-30s",refSaldo.getNombreSucursal())+"    "+
						String.format("%1$-5s",refSaldo.getMonedaBT())+"    "+
						String.format("%1$-11s",refSaldo.getCodigoProducto())+"    "+
						String.format("%1$-3s",refSaldo.getIndicadorBanco())+"    "+
						String.format("%1$-15s",refSaldo.getSaldoContable().getNumero())+"    "+
						String.format("%1$-3s",refSaldo.getSignoSaldoContable())+"    "+
						String.format("%1$-15s",refSaldo.getSaldoDisponible().getNumero())+"    "+
						String.format("%1$-3s",refSaldo.getSignoSalDisponible())+"    "+
						String.format("%1$-15s",refSaldo.getTasaIntereses().getNumero())+"    "+
						String.format("%1$-3s",refSaldo.getSignoTasaIntereses())+"   "+
						String.format("%1$-10s",refSaldo.getFechaApertura().getFecha())+"    "+
						String.format("%1$-10s",refSaldo.getDescripcionEstado())+"    "+
						String.format("%1$-3s",refSaldo.getCodigoCCI())+"    "+
						String.format("%1$-3s",refSaldo.getIndicadorAfiliacion())+"");
				i++;
			}	
			
			setLog(strCallUUID + " IBCC-CSF ::::  Cod operacion  : "+vCodOperacion);
			setLog(strCallUUID + " IBCC-CSF ::::  Cuenta Cargo  : "+vCuentaCargo);
			setLog(strCallUUID + " IBCC-CSF ::::  Cuenta Abono  : "+vCuentaAbono);
			
			setLog(strCallUUID + " IBCC-CSF ::::  Tipo Cambio Cargo  : "+vTipoCambioCargo);
			setLog(strCallUUID + " IBCC-CSF ::::  Tipo Cambio Abono  : "+vTipoCambioAbono);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Disp Cargo 1  : "+vSaldoDispCargo1);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Cont Cargo 1  : "+vSaldoContCargo1);
			setLog(strCallUUID + " IBCC-CSF ::::  Importe Convertido  : "+vImporteConvertido);
			setLog(strCallUUID + " IBCC-CSF ::::  Importe Cargo  : "+vImporteCargo);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Disp Cargo 2  : "+vSaldoDispCargo2);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Cont Cargo 2  : "+vSaldoContCargo2);
			setLog(strCallUUID + " IBCC-CSF ::::  Importe Abono  : "+vImporteAbono);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Disp Abono  : "+vSaldoDispAbono);
			setLog(strCallUUID + " IBCC-CSF ::::  Saldo Cont Abono  : "+vSaldoContAbono);			
		}
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion IB80= " + vCodRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion IB93 = " + vCodRpta93);	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("vCodRpta93", vCodRpta93);
	result.put("vCodOperacion",vCodOperacion );
	result.put("vTipoCambioCargo",vTipoCambioCargo );
	result.put("vTipoCambioAbono",vTipoCambioAbono );
	result.put("vSaldoDispCargo1",vSaldoDispCargo1 );
	result.put("vSaldoContCargo1",vSaldoContCargo1 );
	result.put("vImporteConvertido",vImporteConvertido );
	result.put("vImporteCargo",vImporteCargo );
	result.put("vSaldoDispCargo2",vSaldoDispCargo2 );	
    result.put("vSaldoContCargo2",vSaldoContCargo2 );
    result.put("vImporteAbono",vImporteAbono );
	result.put("vSaldoDispAbono",vSaldoDispAbono );
	result.put("vSaldoContAbono",vSaldoContAbono );
    
    return result;
    
};

public String padLeftZeros(int size,String cadena){
		
		if(cadena.length()<size){
			cadena=String.format("%0"+(size-cadena.length())+"d%s",0,cadena);
		}
		return cadena;
	}


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
