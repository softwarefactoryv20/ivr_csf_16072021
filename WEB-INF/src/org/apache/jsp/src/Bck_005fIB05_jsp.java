package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import ConexionSBJAVA.UseClass;
import IvrTransaction.Logger.LogTransSCBIvr;
import IvrTransaction.modelo.TipoCambio;
import IvrTransaction.Controller.TipoCambioController;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fIB05_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {



public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB05.jsp");
	String servidorwas = additionalParams.get("servidorwas");
	String codigoInst = additionalParams.get("codigoInst");
	String vCodRpta = "", vCodErrorRpta = "", numero = "";
	String vCompraDolarEntero = "";
	String vCompraDolarDecimal_1 = "";
	String vCompraDolarDecimal_2 = "";
	String vCompraDolarDecimal_3 = "";
	String vCompraDolarDecimal_4 = "";
	String vCDolarDecimal_1 = "silencio";
	String vCDolarDecimal_2 = "silencio";
	String vCDolarDecimal_3 = "silencio";
	String vCDolarDecimal_4 = "silencio";
	
	String vVentaDolarEntero = "";
	String vVentaDolarDecimal_1 = "";
	String vVentaDolarDecimal_2 = "";
	String vVentaDolarDecimal_3 = "";
	String vVentaDolarDecimal_4 = "";
	String vVDolarDecimal_1 = "silencio";
	String vVDolarDecimal_2 = "silencio";
	String vVDolarDecimal_3 = "silencio";
	String vVDolarDecimal_4 = "silencio";
	
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF/";
	
    JSONObject result = new JSONObject();

	setLog(strCallUUID + " IBCC-CSF :::: OPCION 4 - Tipo de Cambio");
	
	try
	{
    	IvrTransaction.Controller.TipoCambioController Cambio = new IvrTransaction.Controller.TipoCambioController();
    	setLog(strCallUUID + " IBCC-CSF :::: Linea 1... Paso OK");
		IvrTransaction.modelo.TipoCambio TipoCmb = Cambio.QIvrTipoCambio(codigoInst,"0001");
		setLog(strCallUUID + " IBCC-CSF :::: Linea 2... Paso OK");
		setLog(strCallUUID + " IBCC-CSF :::: EL codigo de retorno es = " + Cambio.getCodRetorno());
		if (Cambio.getCodRetorno() == 0) {
			setLog(strCallUUID + " IBCC-CSF :::: Linea 3... Paso OK");
			setLog(strCallUUID + " IBCC-CSF :::: El Codigo de Transaccion es = " + TipoCmb.getERROR());			
			if (TipoCmb.getERROR().equals("0000")) {
				setLog(strCallUUID + " IBCC-CSF :::: Linea 5... Paso OK");
				
				String Venta = TipoCmb.getTipoCambioVenta().toString().replace('.', '-');
				setLog(strCallUUID + " IBCC-CSF :::: Venta = " + Venta);
				
				String Compra = TipoCmb.getTipoCambioCompra().toString().replace('.', '-');
				setLog(strCallUUID + " IBCC-CSF :::: Compra = " + Compra);
				
				String[] ventas = Venta.split("-");
				String ventaEntero = ventas[0];
				setLog(strCallUUID + " IBCC-CSF :::: ventaEntero = " + ventaEntero);
				String ventaDecimal = ventas[1];
				setLog(strCallUUID + " IBCC-CSF :::: ventaDecimal = " + ventaDecimal);
				
				ventaEntero = suprimirCerosAdelante(ventaEntero);
				ventaDecimal = suprimirCerosAtras(ventaDecimal);
				
				int longVentaDecimal = ventaDecimal.length();
				if(longVentaDecimal == 1){
					vVDolarDecimal_1 = ventaDecimal.substring(0,1);
				}else if(longVentaDecimal == 2){
					vVDolarDecimal_1 = ventaDecimal.substring(0,1);
					vVDolarDecimal_2 = ventaDecimal.substring(1,2);
				}else if(longVentaDecimal == 3){
					vVDolarDecimal_1 = ventaDecimal.substring(0,1);
					vVDolarDecimal_2 = ventaDecimal.substring(1,2);
					vVDolarDecimal_3 = ventaDecimal.substring(2,3);
				}else if(longVentaDecimal == 4){
					vVDolarDecimal_1 = ventaDecimal.substring(0,1);
					vVDolarDecimal_2 = ventaDecimal.substring(1,2);
					vVDolarDecimal_3 = ventaDecimal.substring(2,3);
					vVDolarDecimal_4 = ventaDecimal.substring(3,4);
				}
				
				String[] compras = Compra.split("-");
				String compraEntero = compras[0];
				setLog(strCallUUID + " IBCC-CSF :::: compraEntero = " + compraEntero);
				String compraDecimal = compras[1];	
				setLog(strCallUUID + " IBCC-CSF :::: compraDecimal = " + compraDecimal);
				
				compraEntero = suprimirCerosAdelante(compraEntero);
				compraDecimal = suprimirCerosAtras(compraDecimal);
				
				int longCompraDecimal = compraDecimal.length();
				if(longCompraDecimal == 1){
					vCDolarDecimal_1 = compraDecimal.substring(0,1);
				}else if(longCompraDecimal == 2){
					vCDolarDecimal_1 = compraDecimal.substring(0,1);
					vCDolarDecimal_2 = compraDecimal.substring(1,2);
				}else if(longCompraDecimal == 3){
					vCDolarDecimal_1 = compraDecimal.substring(0,1);
					vCDolarDecimal_2 = compraDecimal.substring(1,2);
					vCDolarDecimal_3 = compraDecimal.substring(2,3);
				}else if(longCompraDecimal == 4){
					vCDolarDecimal_1 = compraDecimal.substring(0,1);
					vCDolarDecimal_2 = compraDecimal.substring(1,2);
					vCDolarDecimal_3 = compraDecimal.substring(2,3);
					vCDolarDecimal_4 = compraDecimal.substring(3,4);
				}
				
				vCompraDolarEntero = url_audio + "Numero/" + compraEntero + ".wav";
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar entero = " + vCompraDolarEntero);
				
				vCompraDolarDecimal_1 = url_audio + "Numero/" + vCDolarDecimal_1 + ".wav";
				vCompraDolarDecimal_2 = url_audio + "Numero/" + vCDolarDecimal_2 + ".wav";
				vCompraDolarDecimal_3 = url_audio + "Numero/" + vCDolarDecimal_3 + ".wav";
				vCompraDolarDecimal_4 = url_audio + "Numero/" + vCDolarDecimal_4 + ".wav";
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar decimal 1 = " + vCompraDolarDecimal_1);					
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar decimal 2 = " + vCompraDolarDecimal_2);
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar decimal 3 = " + vCompraDolarDecimal_3);
				setLog(strCallUUID + " IBCC-CSF :::: Compra dolar decimal 4 = " + vCompraDolarDecimal_4);
				
				vVentaDolarEntero = url_audio + "Numero/" + ventaEntero + ".wav";
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar entero = " + vVentaDolarEntero);
				
				vVentaDolarDecimal_1 = url_audio + "Numero/" + vVDolarDecimal_1 + ".wav";
				vVentaDolarDecimal_2 = url_audio + "Numero/" + vVDolarDecimal_2 + ".wav";
				vVentaDolarDecimal_3 = url_audio + "Numero/" + vVDolarDecimal_3 + ".wav";
				vVentaDolarDecimal_4 = url_audio + "Numero/" + vVDolarDecimal_4 + ".wav";					
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar decimal 1 = " + vVentaDolarDecimal_1);
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar decimal 2 = " + vVentaDolarDecimal_2);
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar decimal 3 = " + vVentaDolarDecimal_3);
				setLog(strCallUUID + " IBCC-CSF :::: Venta dolar decimal 4 = " + vVentaDolarDecimal_4);				
			}
		}		
		else
		{
			setLog(strCallUUID + " IBCC-CSF :::: No Existe Conexion con el AS400");
		}
		vCodRpta = TipoCmb.getERROR();
		vCodErrorRpta = Integer.toString(Cambio.getCodRetorno());
		setLog(strCallUUID + " IBCC-CSF :::: vCodRpta es = " + vCodRpta);
		numero = TipoCmb.getTipoCambioVenta().toString().replace('-', '.');
		
	} catch(Exception ex){}
	
    result.put("vCodRpta", vCodRpta);
    result.put("vCodErrorRpta", vCodErrorRpta);
    result.put("vCompraDolarEntero", vCompraDolarEntero);
    result.put("vCompraDolarDecimal_1", vCompraDolarDecimal_1);
    result.put("vCompraDolarDecimal_2", vCompraDolarDecimal_2);
    result.put("vCompraDolarDecimal_3", vCompraDolarDecimal_3);
    result.put("vCompraDolarDecimal_4", vCompraDolarDecimal_4);    
    result.put("vVentaDolarEntero", vVentaDolarEntero);
    result.put("vVentaDolarDecimal_1", vVentaDolarDecimal_1);
    result.put("vVentaDolarDecimal_2", vVentaDolarDecimal_2);
    result.put("vVentaDolarDecimal_3", vVentaDolarDecimal_3);
    result.put("vVentaDolarDecimal_4", vVentaDolarDecimal_4);
    result.put("numero", numero);
    return result;
    
};

public String suprimirCerosAdelante(String entero){
	
	int i=0;
	int p=0;
	int numDigitos=entero.length()-1;
	while(p==0 && i<numDigitos){
		if(entero.charAt(0)=='0'){
			entero=entero.substring(1,entero.length());
		}else{
			p=1;
		}
		i++;
	}	
	return entero;
}

public String suprimirCerosAtras(String decimales){
	
	int p=0;
	int numDigitos=decimales.length()-1;
	while(p==0 && numDigitos>1){
		
		if(decimales.charAt(decimales.length()-1)=='0'){
			
			decimales=decimales.substring(0,decimales.length()-1);
		}else{
			p=1;
		}
	}	
	return decimales;
}



	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
