package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.Calendar;
import org.apache.log4j.Logger;
import java.text.Normalizer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class http_005fRequest_005fSet_005fEDU_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	Logger log = Logger.getLogger("http_Request_Set_EDU.jsp");
	setLog(strCallUUID + " IBCC-CSF :::: Primera linea de LOG http_Request_Set_EDU.jsp");
	
	String USER_AGENT = "Mozilla/5.0";
	String v_Start_Time = additionalParams.get("v_Start_Time");
	String vedu = additionalParams.get("eduid");	
	String app_prd = additionalParams.get("app_prd");					//productType		app_prd
	String ani_num = additionalParams.get("nc_ANI");					//ANI				ani_num
	String ani_pre = additionalParams.get("nc_ANI");					//preANI			ani_pre
	String client_desc = additionalParams.get("client_desc");			//client			client_desc
	String city_desc = additionalParams.get("city_desc");				//city				city_desc	LIMA
	String ctry_desc = additionalParams.get("ctry_desc");				//country			ctry_desc	PERU
	String id_num = additionalParams.get("id_num");						//CardId			id_num
	String dnis_num = additionalParams.get("nc_DNIS");					//DNIS				dnis_num
	String ivr_num = additionalParams.get("ivr_num");					//lastIvrOption		ivr_num
	String levelIdentification = "";									//levelIdentification	
	String tvr_trx = additionalParams.get("tvr_trx");					//TransferToIvr		tvr_trx
	String cif_key = additionalParams.get("cif_key");					//cif_key			cif_key		DNI
	String aut_type = additionalParams.get("aut_type");
	
	setLog(strCallUUID + " IBCC-CSF :::: v_Start_Time = " + v_Start_Time);
	setLog(strCallUUID + " IBCC-CSF :::: vedu = " + vedu);	
	setLog(strCallUUID + " IBCC-CSF :::: app_prd = " + app_prd);					
	setLog(strCallUUID + " IBCC-CSF :::: ani_num = " + ani_num);					
	setLog(strCallUUID + " IBCC-CSF :::: ani_pre = " + ani_pre);					
	setLog(strCallUUID + " IBCC-CSF :::: client_desc = " + client_desc);			
	setLog(strCallUUID + " IBCC-CSF :::: city_desc = " + city_desc);				
	setLog(strCallUUID + " IBCC-CSF :::: ctry_desc = " + ctry_desc);
	setLog(strCallUUID + " IBCC-CSF :::: id_num = " + id_num);						
	setLog(strCallUUID + " IBCC-CSF :::: dnis_num = " + dnis_num);					
	setLog(strCallUUID + " IBCC-CSF :::: ivr_num = " + ivr_num);					
	setLog(strCallUUID + " IBCC-CSF :::: tvr_trx = " + tvr_trx);					
	setLog(strCallUUID + " IBCC-CSF :::: cif_key = " + cif_key);
	setLog(strCallUUID + " IBCC-CSF :::: aut_type = " + aut_type);
	
    JSONObject result = new JSONObject();
    
    /// INICIO - OBTENIENDO LOS SEGUNDOS DESDE QUE EL CLIENTE MARCO LA OPCION 0.
    
    try
    {
    	String f_h[] = v_Start_Time.split(",");
    	java.util.Date fechaMenor = new java.util.Date(Integer.parseInt(f_h[0]), Integer.parseInt(f_h[1]), Integer.parseInt(f_h[2]), Integer.parseInt(f_h[3]), Integer.parseInt(f_h[4]), Integer.parseInt(f_h[5]));
    	Calendar calendario = Calendar.getInstance();
    	int anio, mes, dia, hora, minuto, segundo;
    	anio = calendario.get(Calendar.YEAR);
    	mes = calendario.get(Calendar.MONTH) + 1;
    	dia = calendario.get(Calendar.DAY_OF_MONTH);
    	hora = calendario.get(Calendar.HOUR_OF_DAY);
    	minuto = calendario.get(Calendar.MINUTE);
    	segundo = calendario.get(Calendar.SECOND);
    	setLog(strCallUUID + " IBCC-CSF :::: HORA FIN = " +anio + "/" + mes + "/" + dia + " " + hora + ":" + minuto + ":" + segundo);
    	
    	java.util.Date fechaMayor = new java.util.Date(anio, mes, dia, hora, minuto, segundo);
    	long diferenciaMils = fechaMayor.getTime() - fechaMenor.getTime();
    	long solo_segundos = diferenciaMils / 1000;
    	
    	setLog(strCallUUID + " IBCC-CSF :::: Segundos = " + solo_segundos);
    	
    	/// FIN
    	
    	/// INICIO - ENVIANDO REQUEST SET_EDU
    	String EDUSTR = "http://SPCC-WTS4.per.bns:9171/bootcmp.htm?ctry_code=PERU&client_desc=" + client_desc + "&ani=" + ani_num + "&id_doc=DNI&ctry_desc=PERU&eduid=" + vedu +"&script=httpvox.iccmd&id_num=" + id_num + "&aut_type="+aut_type+"&city_desc=LIMA&ivr_num=" + ivr_num + "&cif_key=" + cif_key + "&app_prd=TB&action=start_script&command=setedu&city_code=LIMA";
    	
		EDUSTR = EDUSTR.replace(" ", "%20");
		EDUSTR = Normalizer.normalize(EDUSTR, Normalizer.Form.NFD);
		EDUSTR = EDUSTR.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		
    	URL obj = new URL(EDUSTR);
    	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    	
    	// optional default is GET
    	con.setRequestMethod("GET");

    	//add request header
    	con.setRequestProperty("User-Agent", USER_AGENT);

    	int responseCode = con.getResponseCode();
    	setLog(strCallUUID + " IBCC-CSF :::: Sending 'GET' request to URL = " + EDUSTR);
    	setLog(strCallUUID + " IBCC-CSF :::: Response Code = " + responseCode);

    	BufferedReader in = new BufferedReader(
    	        new InputStreamReader(con.getInputStream()));
    	String inputLine;
    	StringBuffer response = new StringBuffer();

    	while ((inputLine = in.readLine()) != null) {
    		response.append(inputLine);
    	}
    	in.close();

    	//print result
    	setLog(strCallUUID + " IBCC-CSF :::: Response Code = " + response.toString());	
    }
    catch(Exception ex)
	{
    	setLog(strCallUUID + " IBCC-CSF :::: Response Code = " + ex.toString());
	}    
       
    return result;
    
};


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
