package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import middleware_support.support.MiddlewareSerie;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fObtenerAudiosSerieEnVariables_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {



// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {	
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_ObtenerAudiosSerieEnVariables.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String flagUltimos3Audios = additionalParams.get("flag3UltDig");
	String vSerie= additionalParams.get("numero");
	
	//Output
	String audioLoc_01= url_audio+"/silencio"+extension;
	String audioLoc_02= url_audio+"/silencio"+extension;
	String audioLoc_03= url_audio+"/silencio"+extension;
	String audioLoc_04= url_audio+"/silencio"+extension;
	String audioLoc_05= url_audio+"/silencio"+extension;
	String audioLoc_06= url_audio+"/silencio"+extension;
	String audioLoc_07= url_audio+"/silencio"+extension;
	String audioLoc_08= url_audio+"/silencio"+extension;
	String audioLoc_09= url_audio+"/silencio"+extension;
	String audioLoc_10= url_audio+"/silencio"+extension;
	String audioLoc_11= url_audio+"/silencio"+extension;
	String audioLoc_12= url_audio+"/silencio"+extension;
	String audioLoc_13= url_audio+"/silencio"+extension;
	String audioLoc_14= url_audio+"/silencio"+extension;
	String audioLoc_15= url_audio+"/silencio"+extension;
	String audioLoc_16= url_audio+"/silencio"+extension;
	
	JSONObject result = new JSONObject();
	
	if(flagUltimos3Audios.equals("1")){
		vSerie=vSerie.substring(vSerie.length()-3,vSerie.length());
	}
	
	MiddlewareSerie refMiddlewareSerie=new MiddlewareSerie(url_audio,vSerie,extension);
	List<String> listaAudios;
	listaAudios=refMiddlewareSerie.getListaAudios();
	
	switch(listaAudios.size()){
	
		case 1:	audioLoc_01= listaAudios.get(0);
				break;
		case 2:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				break;
		case 3:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				break;
		case 4:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				break;
		case 5:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				break;
		case 6:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				break;
		case 7:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				break;
		case 8:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				break;
		case 9:	audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				break;
		case 10:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				break;
		case 11:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				break;
		case 12:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				break;
		case 13:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				audioLoc_13= listaAudios.get(12);
				break;
		case 14:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				audioLoc_13= listaAudios.get(12);
				audioLoc_14= listaAudios.get(13);
				break;
		case 15:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				audioLoc_13= listaAudios.get(12);
				audioLoc_14= listaAudios.get(13);
				audioLoc_15= listaAudios.get(14);
				break;
		case 16:audioLoc_01= listaAudios.get(0);
				audioLoc_02= listaAudios.get(1);
				audioLoc_03= listaAudios.get(2);
				audioLoc_04= listaAudios.get(3);
				audioLoc_05= listaAudios.get(4);
				audioLoc_06= listaAudios.get(5);
				audioLoc_07= listaAudios.get(6);
				audioLoc_08= listaAudios.get(7);
				audioLoc_09= listaAudios.get(8);
				audioLoc_10= listaAudios.get(9);
				audioLoc_11= listaAudios.get(10);
				audioLoc_12= listaAudios.get(11);
				audioLoc_13= listaAudios.get(12);
				audioLoc_14= listaAudios.get(13);
				audioLoc_15= listaAudios.get(14);
				audioLoc_16= listaAudios.get(15);
				break;
	}
	setLog(strCallUUID + " IBCC-CSF :::: Lista audios Servicio : "+ refMiddlewareSerie.getSerie());
	
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_01);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_02);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_03);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_04);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_05);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_06);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_07);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_08);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_09);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_10);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_11);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_12);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_13);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_14);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_15);
	setLog(strCallUUID + " IBCC-CSF :::: " + audioLoc_16);
	
	
	
	result.put("audioLoc_01",audioLoc_01);
	result.put("audioLoc_02",audioLoc_02);
	result.put("audioLoc_03",audioLoc_03);
	result.put("audioLoc_04",audioLoc_04);
	result.put("audioLoc_05",audioLoc_05);
	result.put("audioLoc_06",audioLoc_06);
	result.put("audioLoc_07",audioLoc_07);
	result.put("audioLoc_08",audioLoc_08);
	result.put("audioLoc_09",audioLoc_09);
	result.put("audioLoc_10",audioLoc_10);
	result.put("audioLoc_11",audioLoc_11);
	result.put("audioLoc_12",audioLoc_12);
	result.put("audioLoc_13",audioLoc_13);
	result.put("audioLoc_14",audioLoc_14);
	result.put("audioLoc_15",audioLoc_15);
	result.put("audioLoc_16",audioLoc_16);
	
	return result;

};


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
