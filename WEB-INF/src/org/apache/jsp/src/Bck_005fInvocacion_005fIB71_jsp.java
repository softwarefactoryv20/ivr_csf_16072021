package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import middleware_support.tramas.Bean_IB71;
import middleware_support.support.MiddlewareValue;
import middleware_support.support.MiddlewareDate;
import middleware_support.support.MiddlewareNumber;
import middleware_support.classes.Movimiento;
import IvrTransaction.Security.SecurityPassword;
import IvrTransaction.Controller.CredBalanceController;
import IvrTransaction.modelo.CredBalanceInformation;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fInvocacion_005fIB71_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {



// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {	
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_Invocacion_IB71.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vTipoValidacion=additionalParams.get("tipoVal");
	String vPin=additionalParams.get("varOpcIvr");
	String vDni="";
	
	String vPinEncriptado = "";
	String vLetras= "";
	String vNumeros= "";
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String client_desc="";
	String city_desc="";
	String ctry_desc="";
	String vListaAudiosSaldoCredito="";
	String vListaAudiosSaldoLineaDispEfectivo="";
	String vListaAudiosPagoTotal="";
	String vListaAudiosPagoFacturado="";
	String vListaAudiosPagoTotalito="";
	String vListaAudiosPagoMinimo="";
	String vListaAudiosPagoMinimito="";
	String vListaAudiosScotiapuntos="";
	String vMoneda="";

	String numAudiosSaldoCredito="";
	String numAudiosSaldoLineaDispEfectivo="";
	String numAudiosPagoTotal="";
	String numAudiosPagoFacturado="";
	String numAudiosPagoTotalito="";
	String numAudiosPagoMinimo="";
	String numAudiosPagoMinimito="";
	String numAudiosScotiapuntos="";
	
	Bean_IB71 refBean_IB71;
	
	JSONObject result = new JSONObject();
    
	if(vTipoValidacion.equals("P")){
		try{
			IvrTransaction.Security.SecurityPassword SPOId=new SecurityPassword().EncriptaClave(vPin);
			vPinEncriptado = SPOId.getsPasswordTwo();
			vLetras=SPOId.getsLetras();
			vNumeros=SPOId.getsNumeros();
		}catch(StringIndexOutOfBoundsException ex){
			setLog(strCallUUID + " IBCC-CSF :::: PIN invalido");
		}
	}
	
	CredBalanceController Balance = new CredBalanceController();
	CredBalanceInformation BalInfor = Balance.QIvrCredBalanceInformationController(vInstitucion, vTarjeta,vTipoValidacion, vPinEncriptado, vLetras,vNumeros,vDni);
	vCodErrorRpta = Balance.getCodRetorno();
	
	
    setLog(strCallUUID + " IBCC-CSF :::: ================= BACKEND INVOCACION  IB71-CREDIT BALANCE INFORMATION X PIN ================");
	
    setLog(strCallUUID + " IBCC-CSF :::: Tarjeta : "+ vTarjeta);    
	
	if (vCodErrorRpta == 0) {  
	
		vCodRpta=BalInfor.getERROR();
		
		if (vCodRpta.equals("0000")) {
			
			client_desc=BalInfor.getNombreCliente();
			city_desc=BalInfor.getCiudad();
			ctry_desc=BalInfor.getPais();
			
			refBean_IB71=new Bean_IB71(url_audio,vTipoValidacion,BalInfor,extension);
			
			
			vListaAudiosSaldoCredito=refBean_IB71.getSaldoCredito().getListaAudiosString();
			vListaAudiosSaldoLineaDispEfectivo=refBean_IB71.getSaldoLinDispEfectivo().getListaAudiosString();
			vListaAudiosPagoTotal=refBean_IB71.getListaAudiosPagoTotalString(url_audio, extension);
			vListaAudiosPagoFacturado=refBean_IB71.getListaAudiosPagoFacturadoString(url_audio, extension);
			vListaAudiosPagoMinimo=refBean_IB71.getListaAudiosPagoMinimoString(url_audio, extension);
			vListaAudiosPagoTotalito=refBean_IB71.getListaAudiosPagoTotalitoString(url_audio, extension);
			vListaAudiosPagoMinimito=refBean_IB71.getListaAudiosPagoMinimitoString(url_audio, extension);
			vListaAudiosScotiapuntos=refBean_IB71.getScotiaPuntosSaldTotal().getListaAudiosString();
			numAudiosSaldoCredito=Integer.toString(refBean_IB71.getSaldoCredito().getListaAudios().size());
			numAudiosSaldoLineaDispEfectivo=Integer.toString(refBean_IB71.getSaldoLinDispEfectivo().getListaAudios().size());
			numAudiosPagoTotal=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoTotal).size());
			numAudiosPagoFacturado=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoFacturado).size());
			numAudiosPagoMinimo=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoMinimo).size());
			numAudiosPagoTotalito=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoTotalito).size());
			numAudiosPagoMinimito=Integer.toString(MiddlewareValue.getUrlsFromJsonString(vListaAudiosPagoMinimito).size());
			numAudiosScotiapuntos=Integer.toString(refBean_IB71.getScotiaPuntosSaldTotal().getListaAudios().size());
			vMoneda = refBean_IB71.getMoneda();
			
			setLog(strCallUUID + " IBCC-CSF :::: Nombre del Cliente              " + client_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Ciudad 			 	            " + city_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Pais 			 	            " + ctry_desc);
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de tarjeta               " + refBean_IB71.getMoneda());
			setLog(strCallUUID + " IBCC-CSF :::: Indicador de MultiMoneda        " + refBean_IB71.getIndMultiMoneda());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Credito   		  " + refBean_IB71.getSaldoCredito().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Linea Disp. Efectivo   		  " + refBean_IB71.getSaldoLinDispEfectivo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Total Soles               " + refBean_IB71.getPagTotSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Total Dolares               " + refBean_IB71.getPagTotDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Facturado Soles               " + refBean_IB71.getPagFactSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Facturado Dolares             " + refBean_IB71.getPagFactDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Minimo Soles               " + refBean_IB71.getPagMinSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Minimo Dolares             " + refBean_IB71.getPagMinDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Minimito Soles             " + refBean_IB71.getPagMinimitoSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Minimito Dolares             " + refBean_IB71.getPagMinimitoDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Scotia puntos vSaldo x tarj.   	" + refBean_IB71.getScotiaPuntosSaldxTarjeta().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Scotia puntos vSaldo Total    	" + refBean_IB71.getScotiaPuntosSaldTotal().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Totalito Soles             " + refBean_IB71.getPagTotalitoSoles().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Pago Totalito Dolares           " + refBean_IB71.getPagTotalitoDolares().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Linea de Credito                " + refBean_IB71.getLineaCredito().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Linea disp. Efectivo            " + refBean_IB71.getLineaDispEfectivo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Fecha Ultimo vPago				" + refBean_IB71.getFecUltimaPago().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Ultimo vPago realizado mda Nac.  " + refBean_IB71.getUltPagRealizadoSol().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Ultimo vPago realizado mda Ext.    " + refBean_IB71.getUltPagRealizadoDol().getNumero());
			
			
		}		
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              			
	}

	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("client_desc", client_desc);
	result.put("city_desc", city_desc);
	result.put("ctry_desc", ctry_desc);
	result.put("vListaAudiosSaldoCredito", vListaAudiosSaldoCredito);
	result.put("vListaAudiosSaldoLineaDispEfectivo", vListaAudiosSaldoLineaDispEfectivo);
	result.put("vListaAudiosPagoTotal", vListaAudiosPagoTotal);		
	result.put("vListaAudiosPagoFacturado", vListaAudiosPagoFacturado);
	result.put("vListaAudiosPagoMinimo", vListaAudiosPagoMinimo);
	result.put("vListaAudiosPagoTotalito", vListaAudiosPagoTotalito);		
	result.put("vListaAudiosPagoMinimito", vListaAudiosPagoMinimito);
	result.put("vListaAudiosScotiapuntos", vListaAudiosScotiapuntos);
	result.put("numAudiosSaldoCredito", numAudiosSaldoCredito);
	result.put("numAudiosSaldoLineaDispEfectivo", numAudiosSaldoLineaDispEfectivo);
	result.put("numAudiosPagoTotal", numAudiosPagoTotal);		
	result.put("numAudiosPagoFacturado", numAudiosPagoFacturado);
	result.put("numAudiosPagoMinimo", numAudiosPagoMinimo);
	result.put("numAudiosPagoTotalito", numAudiosPagoTotalito);		
	result.put("numAudiosPagoMinimito", numAudiosPagoMinimito);
	result.put("numAudiosScotiapuntos", numAudiosScotiapuntos);
	result.put("vMoneda", vMoneda);
		
	return result;
	    
    
};




	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
