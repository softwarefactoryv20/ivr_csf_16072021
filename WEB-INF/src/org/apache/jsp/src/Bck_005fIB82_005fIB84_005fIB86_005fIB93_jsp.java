package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import middleware_support.tramas.Bean_IB82_IB84_IB86_IB93;
import middleware_support.classes.Saldo;
import IvrTransaction.RequestM3.PagoRecibos;
import middleware_support.support.MiddlewareDate;
import middleware_support.support.MiddlewareNumber;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fIB82_005fIB84_005fIB86_005fIB93_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {

	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB82_IB84_IB86_IB93.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vOrigen=additionalParams.get("vOrigen");
	String vCodigoProducto=additionalParams.get("codigoProducto");
	String vTipoCuenta=additionalParams.get("tipoCuenta");
	String vMonedaBT= additionalParams.get("tipoMoneda");
	String vRucInstitucion= additionalParams.get("vRuc");
	String vNroServicio= additionalParams.get("nroServicio");
	String vCodigoCliente= additionalParams.get("codigoCliente");
	String vNroDocumento= additionalParams.get("nroDocumento");
	String vTrnPagum=additionalParams.get("trnPagum");
	String vIndPagoDocEnv= additionalParams.get("indPagoDocEnv");
	String vMonedaPagoDocEnv= additionalParams.get("monedaPago");
	String vImportPagoDocEnv= additionalParams.get("importePago");
	vIndPagoDocEnv = "S";
	
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String vCodOperacion="";
	String vSaldoDisponibleCargo="";
	
	Bean_IB82_IB84_IB86_IB93 refBean_IB82_IB84_IB86_IB93;
	JSONObject result = new JSONObject();
	
	vImportPagoDocEnv = padLeftZeros(vImportPagoDocEnv);
	
	IvrTransaction.RequestM3.PagoRecibos modelo=new IvrTransaction.RequestM3.PagoRecibos();
	modelo.PagarRecibo(vInstitucion, vTarjeta, vOrigen, vCodigoProducto , vTipoCuenta, vMonedaBT, vRucInstitucion, vNroServicio, vCodigoCliente,vNroDocumento,  vTrnPagum, vIndPagoDocEnv, vMonedaPagoDocEnv, vImportPagoDocEnv);
	vCodErrorRpta=modelo.getCodRetorno();
	
	setLog(strCallUUID + " IBCC-CSF :::: =========== BACKEND INVOCACION - IB82/IB84/IB86/IB93 : PAGO DE RECIBOS ================");
    
	if (vCodErrorRpta==0) {
		
		vCodRpta=modelo.getERROR();
		setLog(strCallUUID + " IBCC-CSF :::: vCodRpta = " + vCodRpta);
		if(vCodRpta.equals("0000")){
			//refBean_IB82_IB84_IB86_IB93=new Bean_IB82_IB84_IB86_IB93(url_audio, modelo, extension);
			//vCodOperacion=refBean_IB82_IB84_IB86_IB93.getRelacionBT();
			//vSaldoDisponibleCargo=refBean_IB82_IB84_IB86_IB93.getSaldDispCtaCargo().getNumero();
			
			vCodOperacion = modelo.getRelacionBT();
			vSaldoDisponibleCargo = modelo.getSaldDispCtaCargo();
						
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB82/IB84/IB86------------");
		
			setLog(strCallUUID + " IBCC-CSF :::: Codigo de Operacion: " + vCodOperacion);
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disponible Cuenta Cargp: " + vSaldoDisponibleCargo);
			
			/*setLog(strCallUUID + " IBCC-CSF :::: Fecha primera cuota : " + refBean_IB82_IB84_IB86_IB93.getFecPrimeraCuota().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Relacion BT : " + refBean_IB82_IB84_IB86_IB93.getRelacionBT());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo disponible de cta cargo : " + refBean_IB82_IB84_IB86_IB93.getSaldDispCtaCargo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de saldo disponible : " + refBean_IB82_IB84_IB86_IB93.getSignoSaldDisp());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo contable de cta cargo : " + refBean_IB82_IB84_IB86_IB93.getSaldContCtaCargo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo saldo contable : " + refBean_IB82_IB84_IB86_IB93.getSignoSaldCont());

			setLog(strCallUUID + " IBCC-CSF :::: Indicador de tipo de cambio : " + refBean_IB82_IB84_IB86_IB93.getIndTipoCambio());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de cambio : " + refBean_IB82_IB84_IB86_IB93.getTipoCambio());
			setLog(strCallUUID + " IBCC-CSF :::: Importe convertido : " + refBean_IB82_IB84_IB86_IB93.getImportConvert().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Sig. importe convertido : " + refBean_IB82_IB84_IB86_IB93.getSignoImportConvert());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de operacion : " + refBean_IB82_IB84_IB86_IB93.getNumOperacion());
			setLog(strCallUUID + " IBCC-CSF :::: Codigo de cliente : " + refBean_IB82_IB84_IB86_IB93.getCodigoCliente());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre de institucion : " + refBean_IB82_IB84_IB86_IB93.getNombreInstitucion());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda editada : " + refBean_IB82_IB84_IB86_IB93.getMonedaEditada());
			
			setLog(strCallUUID + " IBCC-CSF :::: Importe total: " + refBean_IB82_IB84_IB86_IB93.getImportTotal().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe total : " + refBean_IB82_IB84_IB86_IB93.getSignoImportTotal());
			setLog(strCallUUID + " IBCC-CSF :::: Numero de documento : " + refBean_IB82_IB84_IB86_IB93.getNumDocumento());
			setLog(strCallUUID + " IBCC-CSF :::: Fecha de vencimiento : " + refBean_IB82_IB84_IB86_IB93.getFecVencimiento().getFecha());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 01 : " + refBean_IB82_IB84_IB86_IB93.getConcepto01());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 01 : " + refBean_IB82_IB84_IB86_IB93.getImporte01().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 01 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport01());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 02 : " + refBean_IB82_IB84_IB86_IB93.getConcepto02());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 02 : " + refBean_IB82_IB84_IB86_IB93.getImporte02().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 02 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport02());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 03 : " + refBean_IB82_IB84_IB86_IB93.getConcepto03());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 03 : " + refBean_IB82_IB84_IB86_IB93.getImporte03().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 03 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport03());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 04 : " + refBean_IB82_IB84_IB86_IB93.getConcepto04());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 04 : " + refBean_IB82_IB84_IB86_IB93.getImporte04().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 04 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport04());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 05 : " + refBean_IB82_IB84_IB86_IB93.getConcepto05());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 05 : " + refBean_IB82_IB84_IB86_IB93.getImporte05().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 05 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport05());
			
			setLog(strCallUUID + " IBCC-CSF :::: Concepto 06 : " + refBean_IB82_IB84_IB86_IB93.getConcepto06());
			setLog(strCallUUID + " IBCC-CSF :::: Importe 06 : " + refBean_IB82_IB84_IB86_IB93.getImporte06().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de importe 06 : " + refBean_IB82_IB84_IB86_IB93.getSignoImport06());
			
			setLog(strCallUUID + " IBCC-CSF :::: Mora : " + refBean_IB82_IB84_IB86_IB93.getMora().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo de mora : " + refBean_IB82_IB84_IB86_IB93.getSignoMora());
			
			setLog(strCallUUID + " IBCC-CSF :::: Nombre de la empresa : " + refBean_IB82_IB84_IB86_IB93.getNombrEmpresa());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de RUC : " + refBean_IB82_IB84_IB86_IB93.getNroRuc());
			setLog(strCallUUID + " IBCC-CSF :::: Nombre del abonado : " + refBean_IB82_IB84_IB86_IB93.getNombreAbonado());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de servicio : " + refBean_IB82_IB84_IB86_IB93.getNroServicio());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de inscripcion : " + refBean_IB82_IB84_IB86_IB93.getNroInscripcion());
			setLog(strCallUUID + " IBCC-CSF :::: Nro de recibo : " + refBean_IB82_IB84_IB86_IB93.getNroRecibo());
				
			setLog(strCallUUID + " IBCC-CSF :::: Fecha de emision : " + refBean_IB82_IB84_IB86_IB93.getFecEmision().getFecha());
			setLog(strCallUUID + " IBCC-CSF :::: Importe total a pagar : " + refBean_IB82_IB84_IB86_IB93.getImportTotalPagar().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe total : " + refBean_IB82_IB84_IB86_IB93.getSignoImportTotal());
			
			setLog(strCallUUID + " IBCC-CSF :::: Importe de consumo : " + refBean_IB82_IB84_IB86_IB93.getImportConsumo().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe de consumo : " + refBean_IB82_IB84_IB86_IB93.getSignoImportConsumo());
			setLog(strCallUUID + " IBCC-CSF :::: Importe de mora : " + refBean_IB82_IB84_IB86_IB93.getImportMora().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe de mora : " + refBean_IB82_IB84_IB86_IB93.getSignoImpMora());
			setLog(strCallUUID + " IBCC-CSF :::: Importe de reconexion : " + refBean_IB82_IB84_IB86_IB93.getImportReconex().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe de reconexion : " + refBean_IB82_IB84_IB86_IB93.getSignoImportReconexion());
			
			setLog(strCallUUID + " IBCC-CSF :::: Importe IGV : " + refBean_IB82_IB84_IB86_IB93.getImportIGV().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo importe IGV : " + refBean_IB82_IB84_IB86_IB93.getSignoImpIGV());
			
			
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB93------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Mas datos : " + refBean_IB82_IB84_IB86_IB93.getRefBean_IB93().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Sgte. Pagina : " + refBean_IB82_IB84_IB86_IB93.getRefBean_IB93().getNroSiguentePagina());
			setLog(strCallUUID + " IBCC-CSF :::: Saldos");
			
			setLog(strCallUUID + " IBCC-CSF :::: N°  TipoCta  CodSucursal  NombreSucursal Mon.  Cod.Prod. Ind.Banco SaldoContable Signo SaldoDisp Signo Tasa       Signo Fecha        Desc.        Cod CCI Ind-Afil");
			
			int i=1;
			
			for(Saldo refSaldo: refBean_IB82_IB84_IB86_IB93.getRefBean_IB93().getListaSaldosCTAS()){
					
				setLog(strCallUUID + " IBCC-CSF :::: " + i+"    "+
						refSaldo.getTipoCta()+"         "+
						refSaldo.getCodigoSucursal()+"         "+
						refSaldo.getNombreSucursal()+"       "+
						refSaldo.getMonedaBT()+"    "+
						refSaldo.getCodigoProducto()+"    "+
						refSaldo.getIndicadorBanco()+"    "+
						refSaldo.getSaldoContable().getNumero()+"    "+
						refSaldo.getSignoSaldoContable()+"    "+
						refSaldo.getSaldoDisponible().getNumero()+"    "+
						refSaldo.getSignoSalDisponible()+"    "+
						refSaldo.getTasaIntereses().getNumero()+"    "+
						refSaldo.getSignoTasaIntereses()+"   "+
						refSaldo.getFechaApertura().getFecha()+"    "+
						refSaldo.getDescripcionEstado()+"    "+
						refSaldo.getCodigoCCI()+"    "+
						refSaldo.getIndicadorAfiliacion()+"");
				i++;
			}	*/
			
		
		}
		
		
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("codOperacion", vCodOperacion);
	result.put("saldoDispCargo", vSaldoDisponibleCargo);
	
    return result;
    
};

public String padLeftZeros(String cadena){
	
	if(cadena.length()<15){
		cadena=String.format("%0"+(15-cadena.length())+"d%s",0,cadena);
	}
	return cadena;
}



	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
