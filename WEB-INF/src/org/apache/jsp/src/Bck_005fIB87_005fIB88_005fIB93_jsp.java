package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import middleware_support.tramas.Bean_IB87_IB88_IB93;
import middleware_support.classes.Saldo;
import IvrTransaction.RequestM3.PagoTarjetaCredito;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fIB87_005fIB88_005fIB93_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {



public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB87_IB88_IB93.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vCodProducto=additionalParams.get("codigoProducto");
	String vTipoCuenta=additionalParams.get("tipoCuenta");
	String vMonedaBT=additionalParams.get("monedaCuenta");
	String vTipoPago=additionalParams.get("tipoPago");
	String vMonedaPago=additionalParams.get("monedaPago");
	String vImportePagoCta=additionalParams.get("importePago");
	String vNroTarjeta=additionalParams.get("nroServicio");
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String codOperacion="";
	String saldodisponibleCargo="";
	
	Bean_IB87_IB88_IB93 refBean_IB87_IB88_IB93;
	JSONObject result = new JSONObject();
	
	vImportePagoCta=vImportePagoCta.replace("+", "");
	
	IvrTransaction.RequestM3.PagoTarjetaCredito modelo=new IvrTransaction.RequestM3.PagoTarjetaCredito();
	modelo.IvrPagoTarjetaCredito(vInstitucion, vTarjeta, vCodProducto, vTipoCuenta, vMonedaBT, vTipoPago, vMonedaPago, vImportePagoCta, vNroTarjeta);
	vCodErrorRpta=modelo.getCodRetorno();
		
	setLog(strCallUUID + " IBCC-CSF :::: ----------------- INVOCACION - IB87/IB88/IB93 : PAGO TARJETA DE CREDITO -------------------");
	   
	if (vCodErrorRpta==0) {
		
		
		vCodRpta=modelo.getERROR();
		
		if(vCodRpta.equals("0000")){
			refBean_IB87_IB88_IB93=new Bean_IB87_IB88_IB93(url_audio, modelo, extension);
			codOperacion=refBean_IB87_IB88_IB93.getRELACIONBT();
			saldodisponibleCargo=refBean_IB87_IB88_IB93.getSALDISCTACARGO().getNumero();
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB87/IB88------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Relacion BT : " + refBean_IB87_IB88_IB93.getRELACIONBT());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Disp Cta Cargo :  " + refBean_IB87_IB88_IB93.getSALDISCTACARGO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Disp Cta Cargo : " + refBean_IB87_IB88_IB93.getSIGSALDODISP());
			setLog(strCallUUID + " IBCC-CSF :::: Saldo Contable Cta Cargo : " + refBean_IB87_IB88_IB93.getSALCONTCTACARGO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Saldo Contable Cta Cargo : " + refBean_IB87_IB88_IB93.getSIGSALDOCONT());
			setLog(strCallUUID + " IBCC-CSF :::: Ind. Tipo de Cambio : " + refBean_IB87_IB88_IB93.getINDTIPCAMBIO());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Cambio : " + refBean_IB87_IB88_IB93.getTIPOCAMBIO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Importe a Convertir : " + refBean_IB87_IB88_IB93.getIMPORTECONVERT().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe a Convertir : " + refBean_IB87_IB88_IB93.getSIGIMPORTCONV());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de Pago : " + refBean_IB87_IB88_IB93.getMONEDAPAGO());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de Tarjeta : " + refBean_IB87_IB88_IB93.getMONEDATARJETA());
			setLog(strCallUUID + " IBCC-CSF :::: Tipo de Tarjeta : " + refBean_IB87_IB88_IB93.getTIPOTARJETA());
			setLog(strCallUUID + " IBCC-CSF :::: Importe Pago Cuenta : " + refBean_IB87_IB88_IB93.getIMPORTEPAGCTA().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe Pago Cuenta : " + refBean_IB87_IB88_IB93.getSIGPAGOCTA());
			setLog(strCallUUID + " IBCC-CSF :::: Importe de Cargo : " + refBean_IB87_IB88_IB93.getIMPORTECARGO().getNumero());
			setLog(strCallUUID + " IBCC-CSF :::: Moneda de Cargo : " + refBean_IB87_IB88_IB93.getMONEDACARGO());
			setLog(strCallUUID + " IBCC-CSF :::: Signo Importe de Cargo : " + refBean_IB87_IB88_IB93.getSIGIMPORTCARGADO());
			setLog(strCallUUID + " IBCC-CSF :::: Procesadora : " + refBean_IB87_IB88_IB93.getPROCESADORA());
					
			setLog(strCallUUID + " IBCC-CSF :::: ---------IB93------------");
			
			setLog(strCallUUID + " IBCC-CSF :::: Mas datos : " + refBean_IB87_IB88_IB93.getRefBean_IB93().getMasDatos());
			setLog(strCallUUID + " IBCC-CSF :::: Nro Sgte. Pagina : " + refBean_IB87_IB88_IB93.getRefBean_IB93().getNroSiguentePagina());
			setLog(strCallUUID + " IBCC-CSF :::: Saldos");
			
			setLog(strCallUUID + " IBCC-CSF :::: N°  TipoCta  CodSucursal  NombreSucursal Mon.  Cod.Prod. Ind.Banco SaldoContable Signo SaldoDisp Signo Tasa       Signo Fecha        Desc.        Cod CCI Ind-Afil");
			
			int i=1;
			
			for(Saldo refSaldo: refBean_IB87_IB88_IB93.getRefBean_IB93().getListaSaldosCTAS()){
					
				setLog(strCallUUID + " IBCC-CSF :::: " + i+"    "+
						refSaldo.getTipoCta()+"         "+
						refSaldo.getCodigoSucursal()+"         "+
						refSaldo.getNombreSucursal()+"       "+
						refSaldo.getMonedaBT()+"    "+
						refSaldo.getCodigoProducto()+"    "+
						refSaldo.getIndicadorBanco()+"    "+
						refSaldo.getSaldoContable().getNumero()+"    "+
						refSaldo.getSignoSaldoContable()+"    "+
						refSaldo.getSaldoDisponible().getNumero()+"    "+
						refSaldo.getSignoSalDisponible()+"    "+
						refSaldo.getTasaIntereses().getNumero()+"    "+
						refSaldo.getSignoTasaIntereses()+"   "+
						refSaldo.getFechaApertura().getFecha()+"    "+
						refSaldo.getDescripcionEstado()+"    "+
						refSaldo.getCodigoCCI()+"    "+
						refSaldo.getIndicadorAfiliacion()+"");
				i++;
			}	
			
			
			
		}
		
		
	}
	
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("codOperacion", codOperacion);
	result.put("saldoDispCargo", saldodisponibleCargo);
	
	return result;
		
    
};


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
