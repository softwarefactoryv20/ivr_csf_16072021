package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import middleware_support.tramas.Bean_IB92;
import IvrTransaction.Controller.GeneralController;
import IvrTransaction.modelo.SaldosTC;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fIB92_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");;
	//sesion =  strCallUUID + ", ";
    Logger log = Logger.getLogger("Bck_IB92.jsp");
	// TODO Auto-generated method stub
			String extension=".wav";
			String servidorwas = additionalParams.get("servidorwas");
			String url = "http://" + servidorwas + ":8080";
			String url_audio = url + "/APP_IVR_PeruCSF/AudiosCSF";
			
			//Input
			String vInstitucion=additionalParams.get("codigoInst");
			String vTarjeta= additionalParams.get("vNumDoc");
			
			//Output
			int vCodErrorRpta = 0;
			String vCodRpta="0000"; 
			String client_desc="";
			String indMultimoneda="";
			String moneda="";
			String deudaTotalFacturadaSoles="";
			String pagoMesSoles="";
			String pagoMinimoSoles="";
			String deudaTotalFacturadaDolares="";
			String pagoMesDolares="";
			String pagoMinimoDolares="";
			
			Bean_IB92 refBean_IB92;
			
			
			JSONObject result = new JSONObject();		    
			
			IvrTransaction.Controller.GeneralController General= new IvrTransaction.Controller.GeneralController();
			IvrTransaction.modelo.SaldosTC generalModel = General.QIvrSaldosTC(vInstitucion, vTarjeta);
			vCodErrorRpta = General.getCodRetorno();
			
		    if (vCodErrorRpta == 0) {  
			
				vCodRpta=generalModel.getERROR();
				if (vCodRpta.equals("0000")) {
					
					client_desc=generalModel.getNombreCliente();
					refBean_IB92=new Bean_IB92(url_audio,generalModel,extension);
					indMultimoneda=refBean_IB92.getIndMultiMoneda();
					moneda=refBean_IB92.getMonedaTarjeta();
					deudaTotalFacturadaSoles=refBean_IB92.getPagoTotalSoles().getNumero();
					pagoMesSoles=refBean_IB92.getPagoMinimitoSoles().getNumero();
					pagoMinimoSoles=refBean_IB92.getPagoMinimoSoles().getNumero();
					deudaTotalFacturadaDolares=refBean_IB92.getPagoTotalDolares().getNumero();
					pagoMesDolares=refBean_IB92.getPagoMinimitoDolares().getNumero();
					pagoMinimoDolares=refBean_IB92.getPagoMinimoDolares().getNumero();
			
					
					
					setLog(strCallUUID + " IBCC-CSF :::: ========= INVOCACION - IB92 : SALDOS TARJETA DE CREDITO ========= ");
					
					setLog(strCallUUID + " IBCC-CSF :::: Cod. Producto: " + refBean_IB92.getNroTarjetaCredito());
					setLog(strCallUUID + " IBCC-CSF :::: Cliente:  " + client_desc);
				
					setLog(strCallUUID + " IBCC-CSF :::: Moneda: " + refBean_IB92.getMonedaTarjeta());
					setLog(strCallUUID + " IBCC-CSF :::: Ind. Multimoneda: " + refBean_IB92.getIndMultiMoneda());
					setLog(strCallUUID + " IBCC-CSF :::: Minimo Soles: " + refBean_IB92.getPagoMinimoSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Total Soles: " + refBean_IB92.getPagoTotalSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Fact. Soles: " + refBean_IB92.getPagoFactSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Minimito Soles: " + refBean_IB92.getPagoMinimitoSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Totalito Soles: " + refBean_IB92.getPagoTotalitoSoles().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Minimo Dolares: " + refBean_IB92.getPagoMinimoDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Total Dolares: " + refBean_IB92.getPagoTotalDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Fact. Dolares: " + refBean_IB92.getPagoFactDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Minimito Dolares: " + refBean_IB92.getPagoMinimitoDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Totalito Dolares: " + refBean_IB92.getPagoTotalitoDolares().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Linea Credito: " + refBean_IB92.getLineaCredito().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Linea Disp. Efectivo: " + refBean_IB92.getLineaDispEfectivo().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Saldo Credito: " + refBean_IB92.getSaldoCredito().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Saldo Linea Disp. Efectivo: " + refBean_IB92.getSaldoLineaDispEfectivo().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Ultimo Pago Mon. Local: " + refBean_IB92.getUltPagoMonLocal().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Ultimo Pago Mon. Ext. : " + refBean_IB92.getUltPagoMonExt().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Fecha Ultimo Pago: " + refBean_IB92.getFecUltPago().getFecha());
					setLog(strCallUUID + " IBCC-CSF :::: Scotia Ptos. Saldo Tarjeta: " + refBean_IB92.getScotiaPtosSalxTarjeta().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Sctoia Ptos. Saldo Total:  " + refBean_IB92.getScotiaPtosSalTotal().getNumero());
					setLog(strCallUUID + " IBCC-CSF :::: Procesadora: " + refBean_IB92.getProcesadora());
					
					setLog(strCallUUID + " IBCC-CSF :::: ------------Output--------------");
						
					setLog(strCallUUID + " IBCC-CSF :::: Indicador Multimoneda :"+indMultimoneda);
					setLog(strCallUUID + " IBCC-CSF :::: Moneda :"+moneda);
					setLog(strCallUUID + " IBCC-CSF :::: Deuda Total Facturada Soles : "+deudaTotalFacturadaSoles);
					setLog(strCallUUID + " IBCC-CSF :::: Pago Mes Soles: "+pagoMesSoles);
					setLog(strCallUUID + " IBCC-CSF :::: Pago Minimo Soles: "+pagoMinimoSoles);
					setLog(strCallUUID + " IBCC-CSF :::: Deuda Total Facturada Dolares : "+deudaTotalFacturadaDolares);
					setLog(strCallUUID + " IBCC-CSF :::: Pago Mes Dolares: "+pagoMesDolares);
					setLog(strCallUUID + " IBCC-CSF :::: Pago Minimo Dolares: "+pagoMinimoDolares);
					
				}		
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  			
			}
		    
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
			setLog(strCallUUID + " IBCC-CSF :::: Codigo Retorno Transaccion OK = " + vCodRpta);
			
			
			result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
			result.put("vCodRpta", vCodRpta);
			result.put("multimoneda", indMultimoneda);
			result.put("monedaServicio", moneda);
			result.put("pagoDeudaTotalSoles", deudaTotalFacturadaSoles);
			result.put("pagoDeudaMesSoles", pagoMesSoles);
			result.put("pagoDeudaMinimoSoles", pagoMinimoSoles);
			result.put("pagoDeudaTotalDolares", deudaTotalFacturadaDolares);
			result.put("pagoDeudaMesDolares", pagoMesDolares);
			result.put("pagoDeudaMinimoDolares", pagoMinimoDolares);
			
		    return result;
    
};


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;
		//String rutaAppWs = rutaLog;
		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				//rutaAppWs += "/"+ namelog + sdf.format(new Date()) + ".txt";
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				/*
				if(tipo == 5)
				{
					f = new File(rutaLog + "/LOGS/WSLog");
					if (!f.exists()) {
						f.mkdirs();
					}
					bw = new BufferedWriter(new FileWriter(rutaAppWs,true));
					bw.write(sb.toString());
					bw.flush();
					bw.close();
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*
	
	public void setTrama(int tipo, String objetivo) {
		String rutaApp = rutaLog;
		String namelog = "";
		String dateformat = "yyyyMMdd";
		String timeformat = "HH:mm:ss";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try {
			if(!objetivo.equals("")){
				switch(tipo){
				case 0:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSconsultaAutogestion";
					break;
				case 1:
					rutaApp += "/WSTramas104Prepago";
					namelog = "WSCuentaCorriente";
					break;
					
					
				}
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				sdf2 = new SimpleDateFormat(timeformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sdf.format(new Date()) + ";"+ sdf2.format(new Date())+ ";" + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String readFile(String path) {
		StringBuilder texto = new StringBuilder();
		try {
			File f = new File(path);
			String salto = System.getProperty("line.separator");
			if (f.exists()) {
				String linea = "";
				BufferedReader br = new BufferedReader(new FileReader(f));
				while ((linea = br.readLine()) != null) {
					texto.append(linea + salto);
				}
				br.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return texto.toString();
	}

	public String getPropertiesValue(String file, String key) {
		String valor = "";
		String path = rutaLog + "/" + file + ".properties";
		try {
			File archivo = new File(path);
			if(archivo.exists()) {
				Properties pro = new Properties();
				pro.load(new FileInputStream(archivo));
				valor = pro.getProperty(key);
			}
			else {
				System.out.println("No existe el archivo");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}



	public String getDate(String formato){
		SimpleDateFormat sdf = new SimpleDateFormat(formato);
		return sdf.format(new Date());
	}

	public String getContigenciaValue(String key){
		String valor = "";
		Document doc = null;
		String path = rutaLog;
		path = path.substring(0, path.lastIndexOf("\\") + 1) + "CONTINGENCIA/Contingencia.xml";
		File file = new File(path);
		if(file.exists()){
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				if(doc == null){
					doc = db.parse(file);
					valor = doc.getElementsByTagName(key).item(0).getTextContent().toString();
				}
				
			}catch(Exception e){
				System.out.println("Error getContigenciaValues en " + e.getMessage());
			}
		}
		return valor;
	}


	public long getSecondsElapsed(String startTime){
		long seconds = 0;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			long lStartTime = sdf.parse(startTime).getTime();
			long lEndTime = sdf.parse(sdf.format(new Date())).getTime();
			seconds = (lEndTime - lStartTime) / 1000;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return seconds;
	}*/

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(2);
    _jspx_dependants.add("/src/../include/log/LogConfig.jspf");
    _jspx_dependants.add("/src/../include/backend.jspf");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
